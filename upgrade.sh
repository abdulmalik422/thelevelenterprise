#!/bin/bash
 
echo "Running Setup:Upgrade..";
php -dmemory_limit=2G bin/magento setup:upgrade; 

#php bin/magento setup:di:compile; 

echo "Start Content Deploy For adminhtml..";
php -dmemory_limit=2G bin/magento setup:static-content:deploy en_US -a adminhtml -t Magento/backend;

echo "Start Content deploy for Theme";
php -dmemory_limit=2G bin/magento setup:static-content:deploy en_US -a frontend -t Ktpl/thelevele;


echo "Start Cache Flush..";
php bin/magento cache:clean; 
php bin/magento cache:flush;

echo "Finished Processing..";

#php bin/magento indexer:reindex; 
