<?php
/**
* @copyright Copyright (c) 2016 www.magebuzz.com
*/

namespace Magebuzz\Socialshare\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Socialshare extends \Magento\Framework\View\Element\Template
{
    public $_coreRegistry;
    public $_scopeConfig;
    public $_product;
    public $_priceHelper;
    public $_storeManager;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PricingHelper $PricingHelper,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        array $data = array()
    ) {
        $this->_scopeConfig  = $scopeConfig;
        $this->_priceHelper  = $PricingHelper;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
        $this->_product      = $this->_coreRegistry->registry('current_product');
        $this->_storeManager = $storeManager;
    }

    public function getShareDescription()
    {

        $_product = $this->_product;

        $productName = $_product->getName();
        $productPrice = strip_tags($this->_priceHelper->currency(round($_product->getPrice(), 2)));
        $limit = $this->_scopeConfig->getValue('socialshare/whatsapp/description_limit', ScopeInterface::SCOPE_STORE);
        $productShortdesc = substr(strip_tags($_product->getDescription()), 0, $limit);
        $productsData = $this->_scopeConfig->getValue('socialshare/whatsapp/share_desc', ScopeInterface::SCOPE_STORE);
        $productsData = nl2br($productsData);
        $productsData = str_replace(array("[product-title]", "[product-price]", "[product-description]"), array($productName, $productPrice, $productShortdesc), $productsData);
        $productsData = str_replace(array("<br>", "<br/>", "<br />"), array("%0a", "%0a", "%0a"), $productsData);

        return $productsData;
    }

    public function getFacebookShare(){
        $siteUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK);
        return empty($this->_product) ? $siteUrl : $this->_product->getProductUrl();
    }

    public function getTwitterButton()
    {
        $siteUrl  = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK);
        $shareUrl = empty($this->_product) ? $siteUrl : $this->_product->getProductUrl();
        return "<div class='twitter_button iconSocial social-button'>
        <i class='fa fa-twitter'></i>
  <a href='https://twitter.com/share' class='iconSocial tw-icon twitter-share-button' data-url='" . $shareUrl . "' ><i class='fa fa-twitter'></i></a>
  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></div>";
    }

    public function getPinItButton()
    {
        $siteUrl      = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK);
        $count_button = $this->_scopeConfig->getValue('socialshare/pinitsharing/display_pinit_count', ScopeInterface::SCOPE_STORE);
        $count_button = ($count_button == 1) ? 'beside' : 'none';
        $shareUrl     = empty($this->_product) ? $siteUrl : $this->_product->getProductUrl();
        $siteName     = empty($this->_product) ? $siteUrl : $this->_product->getName();

        return '<div class="pinit_button social-button iconSocial pin-icon">
        <i class="fa fa-pinterest"></i>
  <a href="//www.pinterest.com/pin/create/button/?url=' . urlencode($shareUrl) . '&description=' . urlencode($siteName) . '" data-pin-do="buttonPin" data-pin-color="red" data-pin-config="' . $count_button . '"  data-pin-height="20"></a>
  </div>
  <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>';
    }

    public function getGooglePlusButton()
    {
        $count_button = $this->_scopeConfig->getValue('socialshare/googleplus/display_google_count', ScopeInterface::SCOPE_STORE);
        $count_button = ($count_button == 1) ? 'bubble' : 'none';
        return '<div class="google_button social-button iconSocial goplus-icon">
        <i class="fa fa-google-plus"></i>
  <div class="g-plusone" data-size="medium"  data-annotation="' . $count_button . '"></div>
  </div>
  <script src="https://apis.google.com/js/platform.js" async defer></script>';
    }

    public function getProduct()
    {
        $siteUrl  = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK);
        $shareUrl = empty($this->_product) ? $siteUrl : $this->_product->getProductUrl();
        return $shareUrl;
    }
}
