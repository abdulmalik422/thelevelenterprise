/**
* Copyright 2018 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

var config = {
    map: {
        '*': {
            awPopupManagerFieldset: 'Aheadworks_Popup/js/managerFieldset',
            awPopupMagnific: 'Aheadworks_Popup/js/jquery.magnific-popup',
            awPopupManager: 'Aheadworks_Popup/js/popupManager'
        }
    }
};