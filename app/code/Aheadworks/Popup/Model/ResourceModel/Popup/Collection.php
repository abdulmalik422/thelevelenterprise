<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Popup\Model\ResourceModel\Popup;

/**
 * Class Collection
 * @package Aheadworks\Popup\Model\ResourceModel\Popup
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Id field name
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Store manager
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Catalog config factory
     * @var \Magento\Catalog\Model\ResourceModel\ConfigFactory
     */
    protected $_catalogConfFactory;

    /**
     * Catalog attribute factory
     * @var \Magento\Catalog\Model\Entity\AttributeFactory
     */
    protected $_catalogAttrFactory;

    /**
     * Datetime
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;

    /**
     * Constructor
     *
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ResourceModel\ConfigFactory $catalogConfFactory
     * @param \Magento\Catalog\Model\Entity\AttributeFactory $catalogAttrFactory
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\ConfigFactory $catalogConfFactory,
        \Magento\Catalog\Model\Entity\AttributeFactory $catalogAttrFactory,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->_storeManager = $storeManager;
        $this->_catalogConfFactory = $catalogConfFactory;
        $this->_catalogAttrFactory = $catalogAttrFactory;
        $this->_dateTime = $dateTime;
    }

    /**
     * Constructor
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Magento\Framework\DataObject::class, \Aheadworks\Popup\Model\ResourceModel\Popup::class);
    }

    /**
     * Init collection select
     *
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        if (!$this->getFlag('ctr_joined')) {
            $this->getSelect()
                ->joinLeft(
                    ['ctr_t' => new \Zend_Db_Expr(
                        "(Select " .
                        "id as ctr_id, " .
                        "ROUND(click_count/IF(view_count > 0, view_count, 1) * 100, 0) as ctr ".
                        "FROM {$this->getTable('aw_popup_block')})"
                    )
                    ],
                    "main_table.id = ctr_t.ctr_id",
                    ['ctr']
                );
            $this->addFilterToMap('ctr', 'ctr_t.ctr');
            $this->setFlag('ctr_joined', true);
        }

        return $this;
    }

    /**
     * Add customer group filter
     *
     * @param mixed $customerGroups
     * @return $this
     */
    public function addCustomerGroupFilter($customerGroup)
    {
        $this->addFieldToFilter('customer_groups', ['finset' => $customerGroup]);
        return $this;
    }

    /**
     * Add position filter
     *
     * @param int $position
     * @return $this
     */
    public function addPositionFilter($position)
    {
        $this->addFieldToFilter('position', ['eq' => $position]);
        return $this;
    }

    /**
     * Add page type filter
     *
     * @param int $page
     * @return $this
     */
    public function addPageTypeFilter($page)
    {
        $this->addFieldToFilter('page_type', ['finset' => $page]);
        return $this;
    }

    /**
     * Add store filter
     *
     * @param int $storeId
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        $this
            ->getSelect()
            ->where("FIND_IN_SET(0, store_ids) OR FIND_IN_SET({$storeId}, store_ids)");
        return $this;
    }

    /**
     * Add status enabled filter
     *
     * @return $this
     */
    public function addStatusEnabledFilter()
    {
        $this->addFieldToFilter('status', ['eq' => 1]);
        return $this;
    }

    /**
     * Add excluded ids filter
     *
     * @param array $popupIds
     * @return $this
     */
    public function addExcludedIdsFilter(array $popupIds)
    {
        $this->addFieldToFilter('id', ['nin' => $popupIds]);
        return $this;
    }

    /**
     * Add pages viewed filter
     *
     * @param int $viewedCount
     * @return $this
     */
    public function addPageViewedFilter($viewedCount)
    {
        $eventPageViewedType = \Aheadworks\Popup\Model\Source\Event::VIEWED_PAGES;
        $this
            ->getSelect()
            ->where(
                "(main_table.event <> '" . $eventPageViewedType . "' OR (main_table.event = '" . $eventPageViewedType .
                "' AND main_table.event_value <= " . $viewedCount . "))"
            );
        return $this;
    }
}
