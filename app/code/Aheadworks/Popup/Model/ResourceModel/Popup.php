<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Popup\Model\ResourceModel;

/**
 * Class Popup
 * @package Aheadworks\Popup\Model\ResourceModel
 */
class Popup extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('aw_popup_block', 'id');
    }

    /**
     * Before save
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($object->hasData('customer_groups') && is_array($object->getData('customer_groups'))) {
            $object->setData('customer_groups', implode(',', $object->getData('customer_groups')));
        }

        if ($object->hasData('store_ids') && is_array($object->getData('store_ids'))) {
            $object->setData('store_ids', implode(',', $object->getData('store_ids')));
        }

        if ($object->hasData('page_type') && is_array($object->getData('page_type'))) {
            $object->setData('page_type', implode(',', $object->getData('page_type')));
        }

        if (is_array($object->getData('popup_conditions'))) {
            $object->setData('product_condition', serialize($object->getData('popup_conditions')));
        }

        return $this;
    }

    /**
     * After load
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($object->getId()) {
            $object->setData('customer_groups', explode(',', $object->getData('customer_groups')));
            $object->setData('store_ids', explode(',', $object->getData('store_ids')));

            $conditions = unserialize($object->getData('product_condition'));
            $object->setData('conditions', $conditions);
            if ($conditions) {
                $object->getRuleModel()->getConditions()->loadArray($conditions, 'popup');
            }
        }
        return parent::_afterLoad($object);
    }

    /**
     * Add CTR to model
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Framework\DB\Select
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $field = $this->getConnection()->quoteIdentifier(sprintf('%s.%s', $this->getMainTable(), $field));
        $select = $this->getConnection()->select()->from($this->getMainTable())->where($field . '=?', $value);
        $mainTable = $this->getMainTable();
        $select->columns(
            [
                'ctr' =>
                "CONCAT(ROUND(" . $mainTable . ".click_count/IF(" . $mainTable . ".view_count > 0, "
                . $mainTable . ".view_count, 1) * 100, 0),'%')"
            ]
        );
        return $select;
    }
}
