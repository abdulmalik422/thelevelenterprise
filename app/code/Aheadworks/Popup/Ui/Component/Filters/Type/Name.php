<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Popup\Ui\Component\Filters\Type;

/**
 * Class Name
 * @package Aheadworks\Popup\Ui\Component\Filters\Type
 */
class Name extends \Magento\Ui\Component\Filters\Type\Input
{
    /**
     * Apply filter for name column
     *
     * @return void
     */
    protected function applyFilter()
    {
        if (isset($this->filterData[$this->getName()])) {
            $value = $this->filterData[$this->getName()];

            if (!empty($value)) {
                $filter = $this->filterBuilder->setConditionType('like')
                    ->setField($this->getName())
                    ->setValue(sprintf('%s%%', $value))
                    ->create()
                ;
                $this->getContext()->getDataProvider()->addFilter($filter);
            }
        }
    }
}
