<?php
namespace Aalogics\Opencart\Setup;

use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig       = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $this->addAttribute($eavSetup, "phone_number", "Phone Number");

        $setup->endSetup();
    }

    public function addAttribute(EavSetup $eavSetup, $name, $label, $type = 'text')
    {
        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            $name,
            [
                'type'                  => 'varchar',
                'label'                 => $label,
                'input'                 => $type,
                'required'              => false,
                'default'               => '',
                'sort_order'            => 100,
                'position'              => 100,
                'visible'               => true,
                'system'                => 0,
                'is_used_in_grid'       => true,
                'is_visible_in_grid'    => true,
                'is_filterable_in_grid' => true,
                'is_searchable_in_grid' => true,
            ]
        );
        $attr = $this->eavConfig->getAttribute('customer', $name);
        $attr->setData(
            'used_in_forms',
            ['adminhtml_customer', 'customer_account_create', 'customer_account_edit']
        );

        $attr->save();
    }

    public function removeAttribute(EavSetup $eavSetup, $name)
    {
        $eavSetup->removeAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            $name);
    }
}
