<?php 

namespace Aalogics\Opencart\Setup;

use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
 
class Uninstall implements UninstallInterface
{
	private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig       = $eavConfig;
    }

    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
 		
 		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $this->removeAttribute($eavSetup,"phone_number");
 
        $setup->endSetup();
    }
    public function removeAttribute(EavSetup $eavSetup, $name)
    {
        $eavSetup->removeAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            $name);
    }
}