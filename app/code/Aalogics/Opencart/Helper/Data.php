<?php

namespace Aalogics\Opencart\Helper;

use Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Area;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $jsonHelper;
	protected $_transportBuilder;
    protected $scopeConfig;
    protected $_storeManager;
    protected $mathRandom;
    
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        Context $context,
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Framework\Math\Random $mathRandom
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->scopeConfig = $context->getScopeConfig();
        $this->_storeManager = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->mathRandom = $mathRandom;
    }

    public function jsonResponse($response = '')
    {
        return $this->jsonHelper->jsonEncode($response);
    }

    public function createUniqueHash($prefix = '')
    {
        return md5($prefix . uniqid(microtime() . $this->mathRandom->getRandomNumber(), true));
    }

    public function parseOutput($response = '')
    {
        $responseOutput['result'] = [
           'status' => [
               'code' => 200,
               'message' => "OK",
               'error' => 0,
               'error_messages' => []
           ]
       ];
       if(!empty($response)){
           $responseOutput['result']['data'] = $response;
       }
        return $responseOutput;
    }

    public function parseError($msg = ''){
        $errorOutput = [
            'status' => [
                'code' => 400,
                'message' => "Bad Request",
                'error' => 1,
                'error_messages' => [$msg],
            ]
        ];
        return $this->jsonResponse($errorOutput);
    }

    public function getStorename()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_sales/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getStoreEmail()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_sales/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function sendCustomEmail($dataParams){
        $identity = array('email' => $this->getStoreEmail(), 'name' => $this->getStorename());
        $storeid = $this->_storeManager->getStore()->getStoreId();
        $tomail = $dataParams['customer_email'];
        $templateId = 'customer_new_password_template';
        if ($templateId && $identity) {
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier($templateId)
                ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => $storeid])
                ->setTemplateVars($dataParams)
                ->setFrom($identity)
                ->addTo($tomail)
                ->getTransport();
            $transport->sendMessage();
        }
    }
    

}
