<?php
namespace Aalogics\Opencart\Model;


use Magento\Quote\Model\QuoteIdMaskFactory;
use Psr\Log\LoggerInterface as PsrLogger;
use Magento\Framework\Exception\LocalizedException;

class GuestCartItemRepository extends \Magento\Quote\Model\GuestCart\GuestCartItemRepository implements \Aalogics\Opencart\Api\GuestCartItemRepositoryInterface
{
    protected $_productloader;
    protected $_helper;
    protected $logger;
    protected $catModel;
    protected $stockRegistry;
    protected $cartItemInterface;
    protected $guestCartRepositoryInterface;
    protected $quoteRepository;
    protected $quote;
    protected $productOptionExtensionFactory;
    protected $productOptionFactory;
    protected $itemOptionValueFactory;
    protected $productRepository;
    protected $jsonHelper;

    public function __construct(
        \Magento\Quote\Api\CartItemRepositoryInterface $repository,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Aalogics\Opencart\Helper\Data $helper,
        \Magento\Catalog\Model\Category $catModel,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        PsrLogger $logger, 
        \Magento\Quote\Api\Data\CartItemInterface $cartItemInterface,
        \Magento\Quote\Api\GuestCartRepositoryInterface $guestCartRepositoryInterface,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Api\Data\ProductOptionExtensionFactory $productOptionExtensionFactory,  
        \Magento\Quote\Model\Quote\ProductOptionFactory $productOptionFactory,
        \Magento\ConfigurableProduct\Model\Quote\Item\ConfigurableItemOptionValueFactory $itemOptionValueFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Json\Helper\Data $jsonHelper   
    ) {
        
        $this->_productloader = $_productloader;
        $this->_helper = $helper;
        $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->catModel = $catModel;
        $this->stockRegistry = $stockRegistry;
        $this->cartItemInterface = $cartItemInterface;
        $this->guestCartRepositoryInterface = $guestCartRepositoryInterface;
        $this->quoteRepository = $quoteRepository;
        $this->quote = $quote;
        $this->productOptionExtensionFactory = $productOptionExtensionFactory;
        $this->productOptionFactory = $productOptionFactory;
        $this->itemOptionValueFactory = $itemOptionValueFactory;
        $this->productRepository = $productRepository;

        parent::__construct($repository,$quoteIdMaskFactory);
    }

    public function updateCart($guest_token,$cart){
        $itemIds = [];
        try{
            if(is_array($cart) && !empty($cart)){
                $cartItemInterface = $this->cartItemInterface;
                foreach ($cart as $cartItems) {

                    if(isset($cartItems['product_id']) && isset($cartItems['quantity'])){
                        $product = $this->productRepository->getById($cartItems['product_id']);
                        $cartItemInterface->setQuoteId($guest_token);
                        $cartItemInterface->setSku($product->getSku());
                        $cartItemInterface->setQty($cartItems['quantity']);
                       
                        /***** Set Product Option *****/ 
                        // if(is_array($cartItems['option']) && !empty($cartItems['option']) ){
                        //     $configurableOptions = [];
                        //     foreach ($cartItems['option'] as $optionId => $optionValue) {
                        //         $option = $this->itemOptionValueFactory->create();
                        //         $option->setOptionId($optionId);
                        //         $option->setOptionValue($optionValue);
                        //         $configurableOptions[] = $option;
                        //     }
                        //     $productOption = $this->productOptionFactory->create();
                        //     $extensibleAttribute = $this->productOptionExtensionFactory->create();

                        //     $extensibleAttribute->setConfigurableItemOptions($configurableOptions);
                        //     $productOption->setExtensionAttributes($extensibleAttribute);
                        //     $cartItemInterface->setProductOption($productOption);
                        // }

                        /****** Save Cart Items *****/ 
                        $itemsData = $this->save($cartItemInterface);
                        $itemIds[] = $itemsData->getItemId();   
            
                    }else{
                        throw new LocalizedException(__('Please Provide Valid Data'));
                    }
                }

                /********* Update Data ***********/
                foreach ($itemIds as $itemId) {
                    $cartItemInterface = $this->cartItemInterface;
                    $cartItemInterface->setQuoteId($guest_token);
                    $cartItemInterface->setItemId($itemId);
                    $this->save($cartItemInterface);
                }
            }
            return $this->_helper->parseOutput();
        }catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            return $this->_helper->parseError($e->getMessage());
        }
    }


    /**
     * {@inheritdoc}
     */
    public function getCart(
        $guest_token        
    ){   
        try {
            $items = $this->getList($guest_token);
            $arr = [];                       
            foreach($items as $value)
            { 
                $itemData = $value->getData();
                
                $optionValuesArray = [];
                if(!empty($value->getOptions())){
                    foreach ($value->getOptions() as $option) {
                        $optionValues = $this->jsonHelper->jsonDecode($option->getValue());
                        if(isset($optionValues['super_attribute'])){
                            foreach ($optionValues['super_attribute'] as $optionKey => $optionValue) {
                                $optionValuesArray[] = [
                                    'option_id' => $optionKey,
                                    'option_value_id' => $optionValue
                                ];
                            }
                        }
                    }
                }
                
                $product = $itemData['product'];
                $productType = $product->getTypeId();
                
                $productData = $product->getData(); 
                $stockObject = $this->stockRegistry->getStockItem($itemData['product_id']);
                $min =  $stockObject->getMinSaleQty();
                $max =  $stockObject->getMaxSaleQty();                    
                $category = $this->catModel->load($product['category_ids']['0'])->getData();
                $img = [];
                
                if(!empty($product['media_gallery']['images'])){
                    foreach($product['media_gallery']['images'] as $imageData){
                        $img[]= [
                            'product_image_id' =>isset($imageData['value_id'])? $imageData['value_id'] :'',
                            'product_id' =>isset($itemData['product_id'])? $itemData['product_id'] :'', 
                            'image' =>isset($imageData['file'])? $imageData['file'] :'',
                            'sort_order'=>isset($imageData['position'])? $imageData['position'] :'',
                        ];
                    }
                }

                $arr[] = [
                    'key' => isset($itemData['quote_id']) ? $itemData['quote_id']: '',
                    'product_id' => isset($itemData['product_id']) ? $itemData['product_id']: '',
                    'sku' => isset($itemData['sku']) ? $itemData['sku']: '',
                    'name' => isset($itemData['name']) ? $itemData['name']: '',         
                    'model'=> isset($itemData['sku']) ? $itemData['sku']: '',
                    'type'=> $productType,
                    'shipping_country' => '',
                    'free_shipping' => isset($itemData['free_shipping']) ? $itemData['free_shipping']: '',
                    'offer_label' => '',
                    'shipping' => '',
                    'image'=> isset($product['small_image']) ? $product['small_image']: '',
                    'has_options'=> isset($product['has_options']) ? $product['has_options']: '',
                    'option'=> !empty($optionValuesArray) ? $optionValuesArray : [],
                    'download'=>'',
                    'quantity' => isset($itemData['qty']) ? $itemData['qty']: '',
                    'minimum'=> $min,
                    'maximum'=> $max,
                    'subtract'=> '',
                    'cost' => isset($productData['cost']) ? $productData['cost']: '',
                    'stock'=> isset($product['quantity_and_stock_status']['is_in_stock']) ?$product['quantity_and_stock_status']['is_in_stock'] : '',
                    'price' => isset($itemData['price']) ? $itemData['price']: '',
                    'base_price' => isset($itemData['base_price']) ? $itemData['base_price']: '',
                    'base_row_total'=>isset($itemData['base_row_total'])? $itemData['base_row_total']: '',
                    'reward'=>'',
                    'points'=>'',
                    'tax_class_id' => isset($itemData['tax_class_id']) ? $itemData['tax_class_id']: '',
                    'weight'=> $product->getData('weight'),
                    'weight_class_id'=> '',
                    'length'=> isset($productData['ts_dimensions_length'])?$productData['ts_dimensions_length']: "",
                    'width'=> isset($productData['ts_dimensions_width'])?$productData['ts_dimensions_width']: "",
                    'height'=> isset($productData['ts_dimensions_height'])?$productData['ts_dimensions_height']:"",
                    'manufacturer_id'=>$product->getManufacturer(),  
                    'category_id' => isset($product['category_ids'])? $product['category_ids'] :'',
                    'category_name' => isset($category['name'])? $category['name'] :'',
                    'manufacturer_name'=> $product->getAttributeText('manufacturer'),   
                    'notes'=>'',   
                    'referer_type'=>'',   
                    'referer_value'=>'',   
                    'images' => $img,
                    
                ];            
            }
               
            $cartArray= $this->_helper->parseOutput($arr);
            return $cartArray;


        }catch (\Exception $e) {

            $this->logger->critical($e->getMessage());
            return $this->_helper->parseError($e->getMessage());
        }
    }



}
