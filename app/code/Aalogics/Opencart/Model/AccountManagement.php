<?php
namespace Aalogics\Opencart\Model;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\ValidationResultsInterfaceFactory;
use Magento\Customer\Helper\View as CustomerViewHelper;
use Magento\Customer\Model\Config\Share as ConfigShare;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Customer\Model\Customer\CredentialsValidator;
use Magento\Customer\Model\Metadata\Validator;
use Magento\Eav\Model\Validator\Attribute\Backend;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObjectFactory as ObjectFactory;
use Magento\Framework\Encryption\EncryptorInterface as Encryptor;
use Magento\Framework\Encryption\Helper\Security;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\ExpiredException;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Math\Random;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\StringUtils as StringHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface as PsrLogger;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Session\SaveHandlerInterface;
use Magento\Customer\Model\ResourceModel\Visitor\CollectionFactory;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Customer\Model\AuthenticationInterface;

class AccountManagement extends \Magento\Customer\Model\AccountManagement implements \Aalogics\Opencart\Api\AccountManagementInterface
{
    const CUSTOMER_PHONE_NUMBER_ATTR = 'phone_number';

    private $customerInterface;

    protected $_tokenModelFactory;
   
    protected $_helper;

    protected $subscriberFactory;

    protected $customerSession;

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager,
        Random $mathRandom,
        Validator $validator,
        ValidationResultsInterfaceFactory $validationResultsDataFactory,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        CustomerMetadataInterface $customerMetadataService,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        PsrLogger $logger,
        Encryptor $encryptor,
        ConfigShare $configShare,
        StringHelper $stringHelper,
        CustomerRepositoryInterface $customerRepositoryInterface,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        DataObjectProcessor $dataProcessor,
        Registry $registry,
        CustomerViewHelper $customerViewHelper,
        DateTime $dateTime,
        \Magento\Customer\Model\Customer $customerModel,
        ObjectFactory $objectFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        CredentialsValidator $credentialsValidator = null,
        DateTimeFactory $dateTimeFactory = null,
        \Magento\Customer\Model\AccountConfirmation $accountConfirmation = null,
        SessionManagerInterface $sessionManager = null,
        SaveHandlerInterface $saveHandler = null,
        CollectionFactory $visitorCollectionFactory = null,
        \Aalogics\Opencart\Helper\Data $helper,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory,
        CustomerInterface $customerInterface,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {

        $this->mathRandom = $mathRandom;
        $this->encryptor = $encryptor;
        $this->storeManager = $storeManager;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->customerFactory = $customerFactory;
        $this->eventManager = $eventManager;
        $this->customerRegistry = $customerRegistry;
        $this->credentialsValidator = $credentialsValidator;

        $this->customerInterface = $customerInterface;
        $this->customerRepository = $customerRepository;
        $this->_tokenModelFactory = $tokenModelFactory;
        $this->_helper = $helper;
        $this->subscriberFactory = $subscriberFactory;
        $this->customerSession = $customerSession;

        parent::__construct($customerFactory,$eventManager,$storeManager,$mathRandom,$validator,$validationResultsDataFactory,$addressRepository,$customerMetadataService,$customerRegistry,$logger,$encryptor,$configShare,$stringHelper,$customerRepositoryInterface,$scopeConfig,$transportBuilder,$dataProcessor,$registry,$customerViewHelper,$dateTime,$customerModel,$objectFactory,$extensibleDataObjectConverter,$credentialsValidator,$dateTimeFactory,$accountConfirmation,$sessionManager,$saveHandler,$visitorCollectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function customerRegister(
        $name,$email,$telephone_extension,$telephone,$newsletter,$password = null,$date_of_birth, $redirectUrl = ''
    ){   
        try {
            $customer = $this->customerInterface;
            if ($password !== null) {
                $this->checkPasswordStrength($password);
                $customerEmail = $customer->getEmail();
                try {
                    $this->credentialsValidator->checkPasswordDifferentFromEmail($customerEmail, $password);
                } catch (InputException $e) {
                    throw new LocalizedException(__('Password cannot be the same as email address.'));
                }
                $hash = $this->createPasswordHash($password);
            } else {
                $hash = null;
            }
            
            /**** Set Customer Details ***/ 
            $this->setCustomerName($name);
            $customer->setEmail($email);
            $customer->setDob($date_of_birth);

            /******* Create Customer Token********/ 
            $customerToken = $this->_helper->createUniqueHash($email);
            
            /**** Set Customer Custom Attributes ***/
            $customer_extra_att = [
                self::CUSTOMER_PHONE_NUMBER_ATTR => $telephone_extension.$telephone
            ];
            foreach ($customer_extra_att as $attr_key => $attr_value) {
                $customer->setCustomAttribute($attr_key, $attr_value);  
            }

            /***** Save Customer Data ******/ 
            $customer = $this->createAccountWithPasswordHash($customer, $hash, $redirectUrl);
            
            $this->customerSession->setCustomerDataAsLoggedIn($customer);
            if($newsletter == 1){
                $this->subscriberFactory->create()->subscribe($customer->getEmail());
            }

            /******** Get Customer Data *********/ 
            $customerModelData = $this->customerModel->load($customer->getId());
            
            $customerData = [
                'customer_id' => $customerModelData->getData('entity_id'),
                'name' => $customerModelData->getData('firstname').' '.$customerModelData->getData('lastname'),
                'email' => $customerModelData->getData('email'),
                'telephone' => $customerModelData->getData('phone_number'),
                'newsletter' => $newsletter,
                'date_of_birth' => $customerModelData->getData('dob')
            ];
            $result = $this->_helper->parseOutput($customerData);
            return $result;

        }catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            return $this->_helper->parseError($e->getMessage());
        }
    }

    private function setCustomerName($name){
        $customerName = explode(' ', $name);
        $this->customerInterface->setFirstname($customerName[0]);
        if(isset($customerName[1])){
            $this->customerInterface->setLastname($customerName[1]);
        }
        return $name;
    }


}
