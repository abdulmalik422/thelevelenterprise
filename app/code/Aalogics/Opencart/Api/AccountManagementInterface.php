<?php
namespace Aalogics\Opencart\Api;

/**
* @api
*/

interface AccountManagementInterface extends \Magento\Customer\Api\AccountManagementInterface
{
    /**
     * Create customer account. Perform necessary business operations like sending email.
     *
     * @param string $name
     * @param string $email
     * @param int $telephone_extension
     * @param int $telephone
     * @param int $newsletter
     * @param string $password
     * @param string $date_of_birth
     * @param string $redirectUrl
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function customerRegister(
       $name,$email,$telephone_extension,$telephone,$newsletter,$password = null,$date_of_birth, $redirectUrl = ''
    );

}