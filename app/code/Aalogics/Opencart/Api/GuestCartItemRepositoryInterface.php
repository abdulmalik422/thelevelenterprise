<?php
namespace Aalogics\Opencart\Api;

/**
* @api
*/

interface GuestCartItemRepositoryInterface extends \Magento\Quote\Api\GuestCartItemRepositoryInterface
{
    /**
     * Create customer account. Perform necessary business operations like sending email.
     *
     * @param string $guest_token
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCart($guest_token);

     /**
     * Add/update the specified cart item.
     *
     * @param string $guest_token
     * @param mixed $cart The item.
     * @return \Magento\Quote\Api\Data\CartItemInterface Item.
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
     * @throws \Magento\Framework\Exception\CouldNotSaveException The specified item could not be saved to the cart.
     * @throws \Magento\Framework\Exception\InputException The specified item or cart is not valid.
     */
    public function updateCart($guest_token,$cart);

}