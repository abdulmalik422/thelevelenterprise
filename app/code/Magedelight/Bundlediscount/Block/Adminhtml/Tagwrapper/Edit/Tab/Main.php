<?php
namespace Magedelight\Bundlediscount\Block\Adminhtml\Tagwrapper\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $_objectManager,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magedelight\Bundlediscount\Helper\Option $optionData,
        \Magedelight\Bundlediscount\Model\ResourceModel\Tagcategories\CollectionFactory $tagCatagoryCollection,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_objectManager = $_objectManager;
        $this->_statusOption = $optionData;
        $this->_systemStore = $systemStore;
        $this->_tagCatagoryCollection = $tagCatagoryCollection;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create('Magedelight\Bundlediscount\Model\Tagwrapper');
        if($id)
        {
            $model->load($id);
        }
        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('tagwrapper_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Tagwrapper Details'), 'class' => 'fieldset-wide']
        );
//        $path ='giftwrapper/images/';
//        $image = '';
//        if ($model->getImage() != '') {
//            $image = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$path.$model->getImage();
//        }
        //Get gift categories collection using factory method
       // $fieldset->addType('image', 'Magedelight\Bundlediscount\Block\Adminhtml\Tagwrapper\Helper\Image');
        $categoriesCollection = $this->_tagCatagoryCollection->create()->addFieldToFilter('is_active', 1);
        
        //set default value for category drop down
        $default=array('value'=>'','label'=>'Choose Category');
        $i=0;
        $categories[$i]=$default;

        foreach($categoriesCollection as $key=>$value){
            $i++;
            $categories[$i]=array('value'=>$value->getEntityId(),'label'=>$value->getName());
        }
        
        if (!empty($model->getId())) {
            $fieldset->addField('id', 'hidden', ['name' => 'id', 'value' => $model->getId()]);
        }

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Tag Name'),
                'title' => __('Tag Name'),
                'required' => true,
                'value' => $model->getName()
            ]
        );

        
        $fieldset->addField(
            'category',
            'select',
            [
                'name' => 'category',
                'label' => __('Category'),
                'title' => __('Category'),
                'required' => true,
                'value' => $model->getCategory(),
                'values' => $categories
            ]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'required' => true,
                'name' => 'is_active',
                'class' => 'required-entry',
                'value' => $model->getIsActive(),
                'values' => $this->_statusOption->getStatusesOptionArray()
            ]
        );

        //$form->setAction($this->getUrl('*/*/save/id/').$this->getRequest()->getParam('id'));
        //$form->setUseContainer(true);
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Tagwrapper Details');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Tagwrapper Details');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return true;//$this->_authorization->isAllowed($resourceId);
    }

}
