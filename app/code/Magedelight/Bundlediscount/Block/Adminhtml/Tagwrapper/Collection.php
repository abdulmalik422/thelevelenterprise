<?php
/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Bundlediscount
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Bundlediscount\Block\Adminhtml\Tagwrapper;

use Magedelight\Bundlediscount\Model\ResourceModel\Tagwrapper\CollectionFactory as Tagwrappers;

class Collection extends \Magento\Backend\Block\Template
{
	/**
     * @var \Magedelight\Bundlediscount\Model\ResourceModel\giftwrapperFactory
     */
	protected $_tagwrappersFactory;

  /**
     * @param Magento\Catalog\Block\Product\Context
     * @param Magedelight\Bundlediscount\Model\ResourceModel\Tagwrapper\CollectionFactory
     * @param array $data
     */
	public function __construct(
      \Magento\Backend\Block\Template\Context $context,
      \Magento\Directory\Model\Currency $currency,
      Tagwrappers $tagwrapperFactory,
      array $data = []
   	) {
      parent::__construct($context, $data);
      $this->_currency = $currency;
      $this->_tagwrapperFactory = $tagwrapperFactory;
    }

   	/**
   	* check if Giftwrapper needs to be displayed at product view page and extension is enabled.
   	* @return true/false
   	*/

   	public function isEnabled()
   	{
   		$displayTagWrapper = false;
   		$gwextEnabled = $this->_scopeConfig->getValue('bundlediscount/others/enable_frontend', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		  if($gwextEnabled){
  			$displayTagWrapper = true;
  		}

  		return $displayTagWrapper;
   	}

   	/**
     * Retrieve giftwrapper object and create collection
     *
     * @return giftwrappercollection
     */
   	public function getTagwrapperCollection()
  	{
      if($this->isEnabled()){
        $categoryId = $this->getRequest()->getParam('category');
        $gwcollection = $this->_tagwrapperFactory->create();
        $gwcollection->addFieldToFilter('category', $categoryId);
        $gwcollection->addFieldToFilter('is_active', 1);
       
        
        return $gwcollection;
      }
    }

    /**
     * Retrieve Media Image path
     *
     * @return Media Image Path
     */
    // public function getMediaImagePath()
    // {
    //   $imagePath = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'giftwrapper/images/';

    //   return $imagePath;
    // }

    /**
     * Default Giftwrapper Image if missing
     *
     * @return Default Image placeholder
     */
    // public function getDefaultPlaceholder()
    // {
    //   $default = $this->_assetRepo->getUrl("Magedelight_Giftwrapper::images/noGWImage.jpg");

    //   return $default;
    // }

    /**
     * Uploaded Giftwrapper Image uploaded via system config if missing
     *
     * @return Image placeholder
     */
    // public function getPlaceholder()
    // {
    //   $placeholder = $this->_scopeConfig->getValue('giftwrapper_section/general/giftwrapper_placeholder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

    //   return $placeholder;
    // }

    /**
     * Magento\Directory\Model\Currency $currency
     *
     * @return Currency Symbol
     */
    public function getCurrencySymbol()
    {
      $currencySymbol = $this->_currency->getCurrencySymbol();

      return $currencySymbol;
    }
}