<?php
/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Bundlediscount
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/
namespace Magedelight\Bundlediscount\Controller\Adminhtml\Tagwrapper;

use Magento\Framework\App\GiftwrapperTypesInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magedelight\Bundlediscount\Controller\Adminhtml\Tagwrapper
{
    /**
    * @var \Magento\Framework\Image\AdapterFactory
    */
    protected $adapterFactory;

    /**
    * @var \Magento\MediaStorage\Model\File\UploaderFactory
    */
    protected $uploader;

    /**
    * @var \Magento\Framework\Filesystem
    */
    protected $filesystem;

    /**
    * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
    */

    protected $timezoneInterface;

    /**
    * @param \Magento\Backend\App\Action\Context $context
	* @param \Magento\Framework\Image\AdapterFactory $adapterFactory
	* @param \Magento\MediaStorage\Model\File\UploaderFactory $uploader
	* @param \Magento\Framework\Filesystem $filesystem
    */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploader,
        \Magento\Framework\Filesystem $filesystem
    )
    {
        $this->adapterFactory = $adapterFactory;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    /**
     * Save Newsletter Bundlediscount
     *
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->getResponse()->setRedirect($this->getUrl('*/tagwrapper'));
        }
   
        $template = $this->_objectManager->create('Magedelight\Bundlediscount\Model\Tagwrapper');
        $id = $this->getRequest()->getPostValue('id');
        if ($id) {
            $template->load($id);
        }
        if ($data) {
           
            //end block upload image
            try {
                $data = $request->getParams();
                 
                $template->setData('name',
                    $request->getParam('name')
                )->setData('category',
                    $request->getParam('category')
                )->setData('is_active',
                    $request->getParam('is_active')
                 );    
                $template->save();

                $this->messageManager->addSuccess(__('The Tagwrapper has been saved.'));
                $this->_getSession()->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $template->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
                
            } catch (LocalizedException $e) {
                
                $this->messageManager->addError(nl2br($e->getMessage()));
                $this->_getSession()->setData('gridmanager_template_form_data', $this->getRequest()->getParams());
                return $resultRedirect->setPath('*/*/edit', ['id' => $template->getGridpart2templateId(), '_current' => true]);
            } catch (\Exception $e) {
                
                $this->messageManager->addException($e, __('Something went wrong while saving this template.'));
                $this->_getSession()->setData('gridmanager_template_form_data', $this->getRequest()->getParams());
                return $resultRedirect->setPath('*/*/edit', ['id' => $template->getGridpart2templateId(), '_current' => true]);
            }
            
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
