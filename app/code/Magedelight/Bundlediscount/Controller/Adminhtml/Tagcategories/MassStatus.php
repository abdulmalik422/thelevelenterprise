<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magedelight\Bundlediscount\Controller\Adminhtml\Tagcategories;

use Magento\Backend\App\Action;
use Magedelight\Bundlediscount\Controller\Adminhtml\Tagcategories;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magedelight\Bundlediscount\Model\ResourceModel\Tagcategories\CollectionFactory;

class MassStatus extends \Magedelight\Bundlediscount\Controller\Adminhtml\Tagcategories
{
    /**
     * MassActions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var Redirect
     */
    protected $redirect;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Action\Context $context
     * @param Builder $productBuilder
     * @param \Magedelight\Bundlediscount\Model\Indexer\Product\Price\Processor $productPriceIndexerProcessor
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        $this->filter = $filter;
        $this->_redirect = $resultRedirectFactory;
        $this->collectionFactory = $collectionFactory;
        
        parent::__construct($context);
    }

    /**
     * Update product(s) status action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {   
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $GiftcategoryIds = $collection->getAllIds();
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $status = (int) $this->getRequest()->getParam('status');

        try {
            $model = $this->_objectManager->create('Magedelight\Bundlediscount\Model\Tagcategories');

          

            foreach($GiftcategoryIds as $id){
                $model->load($id);
                $model->setIsActive($status);
                $model->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->_getSession()->addException($e, __('Something went wrong while updating the Tagcategory(ies) status.'));
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    }
}
