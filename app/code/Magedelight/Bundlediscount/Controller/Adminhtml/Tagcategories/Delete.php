<?php
/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Bundlediscount
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/
namespace Magedelight\Bundlediscount\Controller\Adminhtml\Tagcategories;


class Delete extends \Magedelight\Bundlediscount\Controller\Adminhtml\Tagcategories
{
    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                // init model and delete
                $model = $this->_objectManager->create('Magedelight\Bundlediscount\Model\Tagcategories');
                $gwModel = $this->_objectManager->create('Magedelight\Bundlediscount\Model\Tagwrapper');
                $tagwrappersCollection = $gwModel->getCollection()->addFieldToFilter('category',$id);
                foreach($tagwrappersCollection as $tagwrapper){
                    $gwModel->load($tagwrapper->getId());
                    $gwModel->setCategory('deleted');
                    $gwModel->save();
                }
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('The Tag category has been deleted.'));
                // go to grid
               
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a Tag category to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
