<?php
/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Bundlediscount
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/
namespace Magedelight\Bundlediscount\Controller\Adminhtml\Tagcategories;

use Magento\Framework\App\GiftwrapperTypesInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;

class Save extends \Magedelight\Bundlediscount\Controller\Adminhtml\Tagcategories
{
    /**
     * Save Bundlediscount
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->getResponse()->setRedirect($this->getUrl('*/tagcategories'));
        }
   
        if ($data) {
            $template = $this->_objectManager->create('Magedelight\Bundlediscount\Model\Tagcategories');
            $id = $this->getRequest()->getParam('entity_id');
            if ($id) {
                $template->load($id);
            }
            
            try {
                $data = $request->getParams();
                 
                $template->setData('name',
                    $request->getParam('name')
                )->setData('is_active',
                    $request->getParam('is_active')
                );

               

                $template->save();

                $this->messageManager->addSuccess(__('The Tag cagegory has been saved.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $template->getEntityId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');

                
            } catch (LocalizedException $e) {
                
                $this->messageManager->addError(nl2br($e->getMessage()));
                $this->_getSession()->setData('gridmanager_template_form_data', $this->getRequest()->getParams());
                return $resultRedirect->setPath('*/*/edit', ['id' => $template->getEntityId(), '_current' => true]);
            } catch (\Exception $e) {
                
                $this->messageManager->addException($e, __('Something went wrong while saving this template.'));
                $this->_getSession()->setData('gridmanager_template_form_data', $this->getRequest()->getParams());
                return $resultRedirect->setPath('*/*/edit', ['id' => $template->getEntityId(), '_current' => true]);
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
