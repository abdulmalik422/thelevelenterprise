<?php
 
namespace Magedelight\Bundlediscount\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Custom extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;
    
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }


 
    public function execute()
    {   
       $priceHelper = $this->_objectManager->create('Magento\Framework\Pricing\Helper\Data'); 
       $result = $this->resultJsonFactory->create();
       $bundle = $this->getRequest()->getParam('bundle_id');
       $product = $this->getRequest()->getParam('product_option_id');
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       $bundleModel = $objectManager->create('\Magedelight\Bundlediscount\Model\Bundlediscount')->calculateDiscountAmountByProductId($bundle,$product);
      return $result->setData(['total_amount' => $priceHelper->currency($bundleModel['total_amount'],true,false),'discount_amount' => $priceHelper->currency($bundleModel['discount_amount'],true,false),'final_amount' => $priceHelper->currency($bundleModel['final_amount'],true,false),'discount_label' => $priceHelper->currency($bundleModel['discount_label'],true,false)]);
    }
}