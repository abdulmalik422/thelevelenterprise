<?php
/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Megamenu
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Megamenu\Model\Menu\Source;

use Magento\Framework\App\Request\Http;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class MenuList
 *
 * @package Magedelight\Megamenu\Model\Menu\Source
 */
class MenuList implements \Magento\Framework\Option\ArrayInterface
{
     /**
     * @var \Magedelight\Megamenu\Model\Menu
     */
    protected $megamenuMenu;
    
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;
    
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * Constructor
     *
     * @param \Magedelight\Megamenu\Model\Menu $megamenuMenu
     */
    public function __construct(\Magedelight\Megamenu\Model\Menu $megamenuMenu,Http $request,StoreManagerInterface $storeManager)
    {
        $this->megamenuMenu = $megamenuMenu;
        $this->request = $request;
        $this->_storeManager = $storeManager;
    }
    
    /**
     * @param \Magedelight\Megamenu\Model\Menu $megamenuMenu
     * @return string
     */
    public function getStores()
    {
        
        $store = $this->request->getParam('store');
        $website = $this->request->getParam('website');
        $current = $this->request->getParam('section');
        
        
       
        $allStores = array();
        if(isset($store) AND !empty($store)){
            $allStores[] = $store;
        }
        elseif(isset($website) AND !empty($website)){
            $website = $this->_storeManager->getWebsite($website);
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                     $allStores[] = $store->getStoreId();
                }
            }
        }
        else{
            $allStores[] = 0;
        }
        return $allStores;
    }
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $storeId = $this->getStores();
        
        if(in_array(0, $storeId)){
            $menus = $this->megamenuMenu->getCollection()
            ->addFieldToFilter('is_active',1)
            ->addStoreFilter(0);
        }
        else{
            $menusTemp = $this->megamenuMenu->getCollection()
            ->addFieldToFilter('is_active',1);
            foreach ($menusTemp as $singleMenu){
                $menuAvailable = true;
                foreach($storeId as $singleStore){
                    $menu = $this->megamenuMenu->load($singleMenu->getMenuId());
                    if(!in_array($singleStore, $menu->getStoreId())){
                        $menuAvailable = false;
                        break;
                    }
                }
                if($menuAvailable){
                    $menus[] = $singleMenu;
                }
            }
            
        }
        
        $sourceArray = array();
        $count = 0;
        if(isset($menus) AND !empty($menus)){
            foreach($menus as $menu){
                $sourceArray[$count]['value'] = $menu->getMenuId();
                $sourceArray[$count]['label'] = $menu->getMenuName();
                $count++;
            }
        }
        
        return $sourceArray;
        //return [['value' => 1, 'label' => __('Yes')], ['value' => 0, 'label' => __('No')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('No'), 1 => __('Yes')];
    }
}
