<?php

/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>
 *
 * @category Magedelight
 * @package Magedelight_Megamenu
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Megamenu\Block\Adminhtml\Menu\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\App\Request\Http;
use Magento\Cms\Model\BlockFactory;
use Magedelight\Megamenu\Model\MenuFactory;
use Magedelight\Megamenu\Model\MenuItemsFactory;
use Magedelight\Megamenu\Helper\Data;
use Magedelight\Megamenu\Model\Source\AnimationType;

/**
 * Class MenuItems
 *
 * @package Magedelight\Megamenu\Block\Adminhtml\Menu\Edit
 */
class MenuItems extends \Magento\Backend\Block\Template {

    /**
     * @var string
     */
    protected $_template = 'Magedelight_Megamenu::menu/menuitems.phtml';

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $_blockFactory;

    /**
     * @var \Magedelight\Megamenu\Model\MenuFactory
     */
    protected $_menuFactory;

    /**
     * @var \Magedelight\Megamenu\Model\MenuItemsFactory
     */
    protected $_menuItemsFactory;

    /**
     * @var \Magedelight\Megamenu\Helper\Data
     */
    public $helper;

    /**
     * @var int
     */
    public static $countDepth = 0;

    /**
     * @var bool
     */
    public static $rootUl = false;

    /**
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $_context;

    /**
     * @var \Magedelight\Megamenu\Model\Source\AnimationType
     */
    public $animationOptions;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     * @param \Magedelight\Megamenu\Model\MenuFactory $menuFactory
     * @param \Magedelight\Megamenu\Model\MenuItemsFactory $menuItemsFactory
     */
    public function __construct(
    Context $context, Http $request, BlockFactory $blockFactory, MenuFactory $menuFactory, MenuItemsFactory $menuItemsFactory, Data $helper, AnimationType $animationOptions
    ) {
        parent::__construct($context);
        $this->_context = $context;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_blockFactory = $blockFactory;
        $this->_menuFactory = $menuFactory;
        $this->_menuItemsFactory = $menuItemsFactory;
        $this->_urlBuilder = $context->getUrlBuilder();
        $this->helper = $helper;
        $this->animationOptions = $animationOptions;
        $this->setFormAction($this->_urlBuilder->getUrl('*/*/save'));
        $this->request = $request;
        $menu_id = $this->request->getParam('menu_id');
        $this->setData('currentMenuId', $menu_id);
        $menu = $this->get_menu_from_menu_id();
        $this->setData('currentMenu', $menu);
    }

    /**
     * Retrieve category by category id
     *
     * @param string
     * @return Magento\Catalog\Model\Category
     */
    public function get_current_menu_id() {
        return $this->getData('currentMenuId');
    }

    /**
     * Retrieve current menu object
     * @return Magedelight\Megamenu\Model\Menu
     */
    public function get_current_menu() {
        return $this->getData('currentMenu');
    }

    /**
     * Retrieve current menu object
     * @return Magedelight\Megamenu\Model\Menu
     */
    public function get_menu_from_menu_id() {
        return $this->_menuFactory->create()->load($this->getData('currentMenuId'));
    }

    /**
     * Show Store Html
     */
    public function getStoreHtml() {
        $storeHtml = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template');
        $storeHtml->setMenuId($this->getData('currentMenuId'));
        $storeHtml->setMenu($this->getData('currentMenu'));
        $storeHtml->setContext($this->_context);
        $storeHtml->setTemplate('Magedelight_Megamenu::menu/stores.phtml');
        return $storeHtml->toHtml();
    }

    /**
     * Show Customer Group Html
     */
    public function getCustomerGroupHtml() {
        $customerHtml = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template');
        $customerHtml->setMenuId($this->getData('currentMenuId'));
        $customerHtml->setMenu($this->getData('currentMenu'));
        $customerHtml->setCustomerGroups($this->helper->getCustomerGroups());
        $customerHtml->setTemplate('Magedelight_Megamenu::menu/customerGroup.phtml');
        return $customerHtml->toHtml();
    }

    /**
     * Retrieve Pages for Selected Store
     * @return array
     */
    public function getStoreSpecificPages() {
        $pages = array();
        $menu = $this->getData('currentMenu');
        $storeId = $menu->getStoreId();

        $pagesModel = $this->_objectManager->create('Magento\Cms\Model\Page')->getCollection()
                ->addFieldToFilter('is_active', 1);
        foreach ($pagesModel as $singlePage) {
            $pageAvailable = true;
            foreach ($storeId as $singleStore) {
                $pageId = $this->_objectManager->get('Magento\Cms\Model\Page')->checkIdentifier($singlePage->getIdentifier(), $singleStore);
                if (!$pageId) {
                    $pageAvailable = false;
                    break;
                }
            }
            if ($pageAvailable) {
                $pages[] = $singlePage;
            }
        }
        return $pages;
    }

    /**
     * Show Pages for side panel
     */
    public function getPages() {
        $pages = $this->getStoreSpecificPages();
        $pageHtml = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template');
        $pageHtml->setPages($this->getStoreSpecificPages());
        $pageHtml->setTemplate('Magedelight_Megamenu::menu/pages.phtml');
        return $pageHtml->toHtml();
    }

    /**
     * Retrieve backend menu tree
     */
    public function getBackendMenuTree($menuId) {
        $menuItems = $this->_menuItemsFactory->create()
                ->getCollection()
                ->addFieldToFilter('menu_id', $menuId)
                ->addFieldToFilter('item_parent_id', 0)
                ->setOrder('sort_order', 'ASC');
        return $this->genereateBackendTree($menuItems);
    }

    /**
     * Generate backend tree for Menu Items
     * @return string
     */
    public function genereateBackendTree($items) {
        if (self::$rootUl) {
            $itemOutput = "<ol class='dd-list'>";
        } else {
            $itemOutput = '<ol class="dd-list mainroot" data-parentId="0">';
            self::$rootUl = true;
        }

        foreach ($items as $item) {
            $currentItem = $this->_menuItemsFactory->create()->load($item->getItemId());
            $itemName = $this->getItemName($currentItem->getItemId(), $currentItem->getObjectId(), $currentItem->getItemType());
            $itemText = $this->getItemText($currentItem);
            $categoryDisplay = $currentItem->getCategoryDisplay();
            /* if((int)$currentItem->getCategoryDisplay() === (int) 1){
              $categoryDisplay = $currentItem->getCategoryDisplay();
              } */
            $itemColumns = '';
            if ($currentItem->getItemColumns()) {
                $itemColumns = $this->getSavedItemsColumns($currentItem);
            }
            
            $itemOutput .= '<li class="dd-item col-m-12" data-id="' . $currentItem->getItemId() . '" data-name="' . $itemName . '" data-type="' . $currentItem->getItemType() . '" data-objectid="' . $currentItem->getObjectId() . '" data-link="' . $currentItem->getItemLink() . '" font-icon="' . $currentItem->getItemFontIcon() . '" animation-field="' . $currentItem->getAnimationOption() . '" item-class="' . $currentItem->getItemClass() . '" data-cat="' . $categoryDisplay . '"><button class="cf removebtn btn right" href="#" type="button">Remove </button><a class="right collapse linktoggle">Collapse</a><a class="right expand linktoggle">Expand</a><div class="dd-handle">' . $itemName . "<span class='right'>(" . $this->helper->getMenuName($currentItem->getItemType()) . ")</span>" . '</div><div class="item-information col-m-12">' . $itemText . $itemColumns . '<div class="cf"></div></div>';
            if ($this->hasItemChildren($item->getItemId())) {
                $childrenItems = $this->_menuItemsFactory->create()
                        ->getCollection()
                        ->addFieldToFilter('item_parent_id', $item->getItemId())
                        ->setOrder('sort_order', 'ASC');
                $itemOutput .= $this->genereateBackendTree($childrenItems);
            }
            $itemOutput .= '</li>';
        }
        return $itemOutput . '</ol>';
    }

    /**
     * Items Columns which are saved
     * @return string
     */
    public function getSavedItemsColumns($currentItem) {
        $itemColumns = json_decode($currentItem->getItemColumns());
        $selectOption = '';
        if (count($itemColumns)) {
            $itemColumnsCount = count($itemColumns);
            $selectOption = '<div class="marginTop20 custColumnsBlock col-m-12"><div class="col-m-4"><h4>Menu Columns </h4>' . $this->columnsSelect($itemColumnsCount) . '</div><div class="col-m-4"><h4>Animation Fields </h4>' . $this->animationSelect($currentItem) . '</div><div class="col-m-12"><div class="menuColumnBlockWrapper">';
            foreach ($itemColumns as $itemColumn) {
                $selectOption .= '<div class="menuColumnBlock column' . $itemColumnsCount . '">';
                $selectOption .= trim(preg_replace('/\s\s+/', ' ', $this->getStaticBlocks($itemColumn->type, $itemColumn->value)));
                $selected = '';
                if ($itemColumn->showtitle == '1') {
                    $selected = 'checked';
                }
                $selectOption .= ' <p>Show Title <input ' . $selected . ' type="checkbox" name="showtitle" class="showtitle"></p>';
                $selectOption .= '</div>';
            }
            $selectOption .= '</div></div></div>';
        }
        return $selectOption;
    }

    /**
     * Check Item Children
     * @return bool
     */
    public function hasItemChildren($itemId) {
        $menuItems = $this->_menuItemsFactory->create()
                ->getCollection()
                ->addFieldToFilter('item_parent_id', $itemId)
                ->Count();
        if ($menuItems) {
            return true;
        }
        return false;
    }

    /**
     * Retrieve Item Label, Url and Preceding Label Content
     * @return string
     */
    public function getItemText($currentItem) {

        $randLabel = rand(1, 100000);
        $randNumber = rand(1, 100000);
        $name = $currentItem->getItemName();
        $class = $currentItem->getItemClass();
        $url = $currentItem->getItemLink();
        $fonticon = $currentItem->getItemFontIcon();

        $category_display = $currentItem->getCategoryDisplay();
        $subcat_display = false;
        $category_checkbox = "";
        $categoryColumns = [];
        if ((int) $category_display === (int) 1) {
            $category_checkbox = "checked";
        }
        if ($currentItem->getCategoryColumns()) {
            $categoryColumns = json_decode($currentItem->getCategoryColumns());
        }

        if ($currentItem->getItemType() == 'link') {
            return '<div class="col-m-3"><h4>Label</h4><input class="input-text admin__control-text required-entry linkclass linktypelabel" type="text" name="  ' . $randLabel . '" value="' . $name . '"></div><div class="col-m-3"><h4>Url</h4><input class="input-text admin__control-text required-entry validate-url linkclass linktypeurl" type="text"  name="custom_link_url' . $randNumber . '" value="' . $url . '"></div><div class="col-m-3"><h4>Class</h4><input class="input-text admin__control-text linkclass linktypeclass" type="text" name="item_class' . $randLabel . '" value="' . $class . '"></div><div class="col-m-3"><h4>Preceding Label Content</h4><input class="input-text admin__control-text linktypefont linkclass" type="text" name="fonticon' . $randLabel . '" value="' . $fonticon . '" ><div class="admin__field-note"><span>This Content will be added before Menu Label.</span></div></div>';
        } elseif ($currentItem->getItemType() == 'megamenu') {
            return '<div class="col-m-4"><h4>Label</h4><input class="input-text admin__control-text required-entry linkclass linktypelabel" type="text" name="mcustom_link_text' . $randLabel . '" value="' . $name . '"></div><div class="col-m-4"><h4>Class</h4><input class="input-text admin__control-text linkclass linktypeclass" type="text" name="item_class' . $randLabel . '" value="' . $class . '"></div><div class="col-m-4"><h4>Preceding Label Content</h4><input class="input-text admin__control-text linktypefont linkclass" type="text" name="fonticon' . $randLabel . '" value="' . $fonticon . '" ><div class="admin__field-note"><span>This Content will be added before Menu Label.</span></div></div>';
        } else {
            $categoryHtml = '';

            $categoryHtml .= '<div class="col-m-4"><h4>Class</h4><input class="input-text admin__control-text linkclass linktypeclass" type="text" name="item_class' . $randLabel . '" value="' . $class . '"></div><div class="col-m-4"><h4>Preceding Label Content</h4><input class="input-text admin__control-text linktypefont linkclass" type="text" name="fonticon' . $randLabel . '" value="' . $fonticon . '" ><div class="admin__field-note"><span>This Content will be added before Menu Label.</span></div></div>';
            
            if ($this->getData('currentMenu')->getMenuType() === '2') {
                if ($currentItem->getItemType() === 'category') {
                    $categoryHtml .= '<div class="col-m-4"><h4>Animation Fields </h4>' . $this->animationSelect($currentItem) . '</div><div class="cf"></div>';
                }

                if ($currentItem->getItemType() == 'category') {
                    $categoryHtml .= '<div class="col-m-12 menuColumnBlockWrapper" style="margin:10px 0;"><input class="admin__control-checkbox checkbox category_checkbox" type="checkbox" name="subcat' . $randLabel . '"' . $category_checkbox . '><label style="line-height:16px;">Display all subcategories</label></div><div class="cf"></div>';
                }
                $blockname = '';
                if (count($categoryColumns)) {
                    foreach ($categoryColumns as $categoryColumn) {
                        $hiddenClass = 'hidden';
                        $selected = '';
                        if ($categoryColumn->enable) {
                            $hiddenClass = '';
                            if ($categoryColumn->showtitle === '1') {
                                $selected = 'checked';
                            }
                            $blockname = $categoryColumn->value;
                        }

                        $categoryHtml .= '<div class="col-m-3"><h4>' . ucfirst($categoryColumn->type) . ' Block</h4>' . $this->yesNoDropdown($categoryColumn->enable, $categoryColumn->type, $randLabel) . '<div class="header_staticblock_select categorylink_category_select ' . $hiddenClass . '" style="margin-top:10px;"><h4 style="margin-top:0;">Select Static Block</h4>' . $this->getStaticBlocks('block', $blockname, true) . '<p>Show Title <input ' . $selected . ' type="checkbox" name="showtitle" class="showtitle"></p></div></div>';
                    }
                }
            }
            return $categoryHtml;
        }
    }

    /**
     * Retrieve animations to select
     * @return string
     */
    public function animationSelect($currentItem = null) {
        $animationOption = '';
        if ($currentItem) {
            $animationOption = $currentItem->getAnimationOption();
        }
        $options = $this->animationOptions->toOptionArray();
        $selectedHtml = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template');
        $selectedHtml->setAnimationOption($animationOption);
        $selectedHtml->setOptions($options);
        $selectedHtml->setTemplate('Magedelight_Megamenu::menu/animationFields.phtml');
        $contents = $selectedHtml->toHtml();
        return trim(preg_replace('/\s\s+/', ' ', $contents));
    }

    /**
     * Retrieve columns number to select
     * @return string
     */
    public function columnsSelect($selected = '') {
        $selectedOptions = array();
        for ($i = 1; $i <= 5; $i++) {
            $selectedOptions[$i] = '';
            if ($selected == $i) {
                $selectedOptions[$i] = 'selected';
            }
        }
        $selectedHtml = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template');
        $selectedHtml->setSelectedOptions($selectedOptions);
        $selectedHtml->setTemplate('Magedelight_Megamenu::menu/columnSelect.phtml');
        $contents = $selectedHtml->toHtml();
        return trim(preg_replace('/\s\s+/', ' ', $contents));
    }

    /**
     * Retrieve menu items name
     * @return string
     */
    public function getItemName($itemId, $objectId, $type) {
        $name = 1;
        if ($type == 'category') {
            $category = $this->_objectManager->create('Magento\Catalog\Model\Category')->load($objectId);
            $name = $category->getName();
        } else if ($type == 'pages') {
            $pages = $this->_objectManager->create('Magento\Cms\Model\Page')->load($objectId);
            $name = $pages->getTitle();
        } else {
            $link = $this->_menuItemsFactory->create()->load($itemId);
            $name = $link->getItemName();
        }
        return $name;
    }

    /**
     * Retrieve static blocks and normal menus according to selected stores
     * @return string
     */
    public function getStaticBlocks($selectedGroup = '', $selectedValue = '', $onlyStaticBlock = false) {
        $blockSelected = '';
        $menuSelected = '';
        $categorySelected = '';
        $menus = '';
        if (!empty($selectedGroup)) {
            if ($selectedGroup == 'block') {
                $blockSelected = 'selected';
            } else if ($selectedGroup == 'menu') {
                $menuSelected = 'selected';
            } else if ($selectedGroup == 'category') {
                $categorySelected = 'selected';
            }
        }
        $menu = $this->getData('currentMenu');
        $storeId = $menu->getStoreId();

        if (in_array(0, $storeId)) {
            $blocks = $this->_blockFactory->create()->getCollection()
                    ->addFieldToFilter('is_active', 1)
                    ->addStoreFilter(0);


            $menus = $this->_menuFactory->create()->getCollection()
                    ->addFieldToFilter('is_active', 1)
                    ->addFieldToFilter('menu_type', 1)
                    ->addStoreFilter(0);
        } else {
            $blocksTemp = $this->_blockFactory->create()->getCollection()
                    ->addFieldToFilter('is_active', 1);
            foreach ($blocksTemp as $singleBlock) {
                $blockAvailable = true;
                foreach ($storeId as $singleStore) {
                    $block = $this->_blockFactory->create();
                    $block->setStoreId($singleStore)->load($singleBlock->getBlockId());
                    if (!$block->getBlockId()) {
                        $blockAvailable = false;
                        break;
                    }
                }
                if ($blockAvailable) {
                    $blocks[] = $singleBlock;
                }
            }

            $menusTemp = $this->_menuFactory->create()->getCollection()
                    ->addFieldToFilter('is_active', 1)
                    ->addFieldToFilter('menu_type', 1);
            foreach ($menusTemp as $singleMenu) {
                $menuAvailable = true;
                foreach ($storeId as $singleStore) {
                    $menu = $this->_menuFactory->create()->load($singleMenu->getMenuId());
                    if (!in_array($singleStore, $menu->getStoreId())) {
                        $menuAvailable = false;
                        break;
                    }
                }
                if ($menuAvailable) {
                    $menus[] = $singleMenu;
                }
            }
        }

        $blockHtml = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template');
        $blockHtml->setBlocks($blocks);
        $blockHtml->setSelectedValue($selectedValue);
        $blockHtml->setBlockSelected($blockSelected);
        $blockHtml->setMenuSelected($menuSelected);
        $blockHtml->setCategorySelected($categorySelected);
        $blockHtml->setSelectedGroup($selectedGroup);
        $blockHtml->setMenus($menus);
        $blockHtml->setOnlyStaticBlock($onlyStaticBlock);
        $blockHtml->setCategoriesData($this->getCategoriesDropdown());
        $blockHtml->setTemplate('Magedelight_Megamenu::menu/staticBlocks.phtml');
        $contents = $blockHtml->toHtml();

        return $contents;
    }

    public function getCategoriesDropdown() {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $categoryCollection = $objectManager->get('\Magento\Catalog\Model\Category');
        $categoriesArray = $categoryCollection
                ->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSort('path', 'asc')
                ->addFieldToFilter('is_active', array('eq' => '1'))
                ->load()
                ->toArray();


        foreach ($categoriesArray as $categoryId => $category) {
            if (isset($category['name'])) {
                $categories[] = array(
                    'label' => $category['name'],
                    'level' => $category['level'],
                    'value' => $categoryId
                );
            }
        }
        return $categories;
    }

    /**
     * Retrieve yes/no select dropdown
     * @return string
     */
    public function yesNoDropdown($selected = '', $name, $randLabel) {
        /* $selectedOptions = array();
          for($i=1;$i<=5;$i++){
          $selectedOptions[$i] = '';
          if($selected == $i){
          $selectedOptions[$i] = 'selected';
          }
          } */
        $selectedHtml = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template');
        $selectedHtml->setSelectedOptions($selected);
        $selectedHtml->setName($name);
        $selectedHtml->setRandLabel($randLabel);
        $selectedHtml->setTemplate('Magedelight_Megamenu::menu/yesNo.phtml');
        $contents = $selectedHtml->toHtml();
        return trim(preg_replace('/\s\s+/', ' ', $contents));
    }
    
    public function getMenuLabels(){
        return json_encode($this->helper->menuTypes());
    }
           

}
