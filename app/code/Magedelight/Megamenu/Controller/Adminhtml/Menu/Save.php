<?php
/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Megamenu
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Megamenu\Controller\Adminhtml\Menu;

use Magento\Backend\App\Action;
use Magedelight\Megamenu\Model\Menu;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 *
 * @package Magedelight\Megamenu\Controller\Adminhtml\Menu
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magedelight_Megamenu::save';

    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Action\Context $context,
        PostDataProcessor $dataProcessor,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataProcessor = $dataProcessor;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->dataProcessor->filter($data);
            
           
            if (isset($data['is_active']) && !empty($data['is_active'])){
                $data['is_active'] = 1;
            }
            else{
                $data['is_active'] = 0;
            }
            
            if (empty($data['menu_id'])) {
                $data['menu_id'] = null;
            }

            /** @var \Magedelight\Megamenu\Model\Menu $model */
            $model = $this->_objectManager->create('Magedelight\Megamenu\Model\Menu');

            $id = $this->getRequest()->getParam('menu_id');
            if ($id) {
                $model->load($id);
            }
            
            if($data['customer_groups']){
                $data['customer_groups'] = implode(',' , $data['customer_groups']);
            }
            
            $model->setData($data);
            $this->_eventManager->dispatch(
                'megamenu_menu_prepare_save',
                ['menu' => $model, 'request' => $this->getRequest()]
            );

            if (!$this->dataProcessor->validateRequireEntry($data)) {
                return $resultRedirect->setPath('*/*/edit', ['menu_id' => $model->getMenuId(), '_current' => true]);
            }

            try {
                $form = $model->save();
                
                $menuId = $form->getMenuId();

                $deleteItems = $this->_objectManager->create('Magedelight\Megamenu\Model\MenuItems')->deleteItems($menuId);
              
                //print_r($data['totalMenus']);die;
                for($i=1;$i<=$data['totalMenus'];$i++){
                    
                    $itemsData = array();
                    $itemsData['item_name'] = $data['item_name'][$i];
                    $itemsData['item_type'] = $data['item_type'][$i];
                    $itemsData['sort_order'] = $data['sort_order'][$i];
                    $itemsData['item_parent_id'] = $data['item_parent_id'][$i];
                    $itemsData['menu_id'] = $menuId;
                    $itemsData['object_id'] = $data['object_id'][$i];
                    $itemsData['item_link'] = $data['item_link'][$i];
                    $itemsData['item_font_icon'] = $data['item_font_icon'][$i];
                    $itemsData['item_class'] = $data['item_class'][$i];
                    $itemsData['animation_option'] = $data['animation_option'][$i];

                    if($data['item_all_cat'][$i]){
                        $itemsData['category_display'] = $data['item_all_cat'][$i];
                    }


                    if(!empty($data['item_columns'][$i])){
                        $itemsData['item_columns'] = json_encode($data['item_columns'][$i]);
                    }
                    
                    if(!empty($data['category_columns'][$i])){
                        $itemsData['category_columns'] = json_encode($data['category_columns'][$i]);
                    }

                    $currentItem = $this->_objectManager->create('Magedelight\Megamenu\Model\MenuItems')->setData($itemsData)->save();
                    $itemId = $currentItem->getItemId();
                    foreach($data['item_parent_id'] as $key => $val)
                    {
                        if ($val == $i) $data['item_parent_id'][$key] = $itemId;
                    }
                }
                
                //echo "<pre>";print_r($itemsData);die;
                $this->messageManager->addSuccess(__('You saved the menu.'));
                $this->dataPersistor->clear('megamenu_menu');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['menu_id' => $model->getMenuId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, $e->getMessage());
            }

            $this->dataPersistor->set('megamenu_menu', $data);
            return $resultRedirect->setPath('*/*/edit', ['menu_id' => $this->getRequest()->getParam('menu_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
