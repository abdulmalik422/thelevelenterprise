<?php
/**
* Magedelight
* Copyright (C) 2018 Magedelight <info@magedelight.com>
*
* NOTICE OF LICENSE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
*
* @category Magedelight
* @package Magedelight_Payfort
* @copyright Copyright (c) 2018 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/
namespace Magedelight\Payfort\Controller\MerchantPage;

use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magedelight\Payfort\Gateway\Config\Config;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Math\Random;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Magento\Vault\Model\PaymentTokenFactory;

/**
 * Class TokenRequest
 * @package Magedelight\Payfort\Controller\MerchantPage
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class NewTokenResponse extends \Magento\Framework\App\Action\Action
{
    const TOKENSUCCESS = 18000;

    /**
     * @var Config
     */
    private $payfortConfig;
     
    /**
     * @var Magento\Framework\Encryption\EncryptorInterface
     */
    private $encryptor;
    
    /**
     *
     * @var type 
     */
    private $urlBuilder;

    /**
     * @var Magento\Framework\Data\FormFactory
     */
    private $formFactory;
    
    /**
     * @var Random
     */
    private $random;
    
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;
    
    /**
     * @var JsonFactory
     */
    private $paymentTokenFactory;
    
     /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    public function __construct(
        Context $context,
        Config $config,
        EncryptorInterface $encryptor,
        FormFactory $formFactory,
        Random $random,
        StoreManagerInterface $storeManager,
        JsonFactory $resultJsonFactory,
        PaymentTokenFactory $paymentTokenFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Serialize\Serializer\Json $serializer
    ) {
         parent::__construct($context);
         $this->payfortConfig = $config;
         $this->urlBuilder = $context->getUrl();;
         $this->encryptor = $encryptor;
         $this->formFactory = $formFactory;
         $this->random = $random;
         $this->storeManager = $storeManager;
         $this->resultJsonFactory = $resultJsonFactory;
         $this->paymentTokenFactory = $paymentTokenFactory;
         $this->customerSession = $customerSession;
         $this->serializer = $serializer;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $request = $this->getRequest();
        $response = $request->getParams();
        if($response['response_code']==self::TOKENSUCCESS)
        {
            try{
                $expiry_date = $response['expiry_date'];
                $exp_yr = substr($expiry_date, 0, 2);
                $exp_mt = substr($expiry_date, 2, 4);
                $expirationDate = $exp_mt . $exp_yr;
                $expDate = new \DateTime(
                    $exp_yr
                    . '-'
                    . $exp_mt
                    . '-'
                    . '01'
                    . ' '
                    . '00:00:00',
                    new \DateTimeZone('UTC')
                );
                $expDate->add(new \DateInterval('P1M'));
                $expDateFull =  $expDate->format('Y-m-d 00:00:00');
                $last4cc = substr($response['card_number'], -4);
                $vaultCard = [];
                $vaultCard['gateway_token'] = $response['token_name'];
                $vaultCard['customer_id'] = $this->customerSession->getCustomer()->getId();
                $vaultCard['is_active'] = true;
                $vaultCard['is_visible'] = true;
                $vaultCard['payment_method_code'] = 'md_payfort';
                $vaultCard['type'] = 'card';
                $vaultCard['expires_at'] = $expDateFull;
                $cardholder = $response['card_holder_name'];
                $nameArray = explode(" ", $cardholder);
                $firstname = $nameArray[0];
                $lastname  = $nameArray[1];
                $vaultCard['details'] = $this->convertDetailsToJSON([
                    'type' => $this->getCreditCardType($response['card_bin']),
                    'maskedCC' => $last4cc,
                    'expirationDate' => $expirationDate,
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'firstuse'    => 1
                ]);
                $vaultCard['public_hash'] = $this->generatePublicHash($vaultCard);
                $CardExits =  $this->paymentTokenFactory->create()->getCollection()->addFieldToFilter('public_hash', array("eq" => $vaultCard['public_hash']));
                if (count($CardExits->getData()) == 0) {
                     $this->paymentTokenFactory->create()->setData($vaultCard)->save();
                     $this->messageManager->addSuccess(__("Card has been Saved Successfully."));
                     return $this->_redirect('vault/cards/listaction/');
                }
                else
                {
                    $this->messageManager->addError(__("Duplicate Card Entry."));
                    return $this->_redirect('vault/cards/listaction/');
                }
                
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __($e->getMessage()));
                return $this->_redirect('vault/cards/listaction/');
            }
        }
        else
        {
            $this->messageManager->addError($response['response_message']);
            return $this->_redirect('vault/cards/listaction/');
        }
        
    }
    
    private function convertDetailsToJSON($details)
    {
        $json = $this->serializer->serialize($details);
        return $json ? $json : '{}';
    }
    
    function getCreditCardType($str, $format = 'string')
    {
        if (empty($str)) {
            return false;
        }

        $matchingPatterns = [
            'VI' => '/^4[0-9]{0,}$/',
            'MC' => '/^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/',
            'AE' => '/^3[47][0-9]{0,}$/',
        ];

        $ctr = 1;
        foreach ($matchingPatterns as $key=>$pattern) {
            if (preg_match($pattern, $str)) {
                return $format == 'string' ? $key : $ctr;
            }
            $ctr++;
        }
    }
    
    protected function generatePublicHash($vaultCard) {
        $hashKey = $vaultCard['gateway_token'];
        if ($vaultCard['customer_id']) {
            $hashKey = $vaultCard['customer_id'];
        }

        $hashKey .= $vaultCard['payment_method_code']
                . $vaultCard['type']
                . $vaultCard['details'];

        return $this->encryptor->getHash($hashKey);
    }
}
