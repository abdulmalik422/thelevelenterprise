<?php
/**
* Magedelight
* Copyright (C) 2018 Magedelight <info@magedelight.com>
*
* NOTICE OF LICENSE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
*
* @category Magedelight
* @package Magedelight_Payfort
* @copyright Copyright (c) 2018 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/
namespace Magedelight\Payfort\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magedelight\Payfort\Gateway\Config\Config;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Payment\Model\Config as PaymentConfig;

/**
 * Class ConfigProvider
 */
final class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'md_payfort';

    const CC_VAULT_CODE = 'md_payfort_cc_vault';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var PaymentConfig
     */
    private $paymentConfig;
    /**
     * Constructor
     *
     * @param Config $config
     * @param PayPalConfig $payPalConfig No longer used by internal code and not recommended.
     * @param PayfortAdapter $adapter
     * @param ResolverInterface $localeResolver No longer used by internal code and not recommended.
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __construct(
        Config $config,
        PaymentConfig $paymentConfig
    ) {
        $this->config = $config;
        $this->paymentConfig = $paymentConfig;
    }
    public function getCcAvailableCardTypes()
    {
        $types = array_flip($this->config->getCcTypes());
        $mergedArray = [];
        if (is_array($types)) {
            foreach (array_keys($types) as $type) {
                $types[$type] = $this->getCcTypeNameByCode($type);
            }
        }
         //preserve the same credit card order
        $allTypes = $this->getCcTypes();
        if (is_array($allTypes)) {
            foreach ($allTypes as $ccTypeCode => $ccTypeName) {
                if (array_key_exists($ccTypeCode, $types)) {
                    $mergedArray[$ccTypeCode] = $ccTypeName;
                }
            }
        }

        return $mergedArray;
    }
    /**
     * 
     * @return boolean
     */
    public function getCcTypes()
    {
        $ccTypes = $this->paymentConfig->getCcTypes();
        if (is_array($ccTypes)) {
            return $ccTypes;
        } else {
            return false;
        }
    }
    /**
     * 
     * @param type $code
     * @return boolean
     */
    public function getCcTypeNameByCode($code)
    {
        $ccTypes = $this->paymentConfig->getCcTypes();
        if (isset($ccTypes[$code])) {
            return $ccTypes[$code];
        } else {
            return false;
        }
    }
    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'isActive' => $this->config->getIsActive(),
                    'availableCardTypes' => $this->getCcAvailableCardTypes(),
                    'useCvv' => $this->config->isCardVerificationEnabled(),
                    'environment' => $this->config->getIsTestMode(),
                    'ccVaultCode' => self::CC_VAULT_CODE
                ]
            ]
        ];
    }

}
