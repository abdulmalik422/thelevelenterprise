/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            custompriceBox: 'Magedelight_Looknbuy/js/custompricebox',
            priceBundleCustom: 'Magedelight_Looknbuy/js/price-bundle'
        }
    }
};