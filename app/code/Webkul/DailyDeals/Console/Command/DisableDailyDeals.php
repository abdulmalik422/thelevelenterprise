<?php
/**
 * Webkul Software
 *
 * @category Magento
 * @package  Webkul_DailyDeals
 * @author   Webkul
 * @license  https://store.webkul.com/license.html
 */
namespace Webkul\DailyDeals\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DisableDailyDeals
 */
class DisableDailyDeals extends Command
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $_moduleManager;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $_eavAttribute;

    /**
     * @var \Magento\Framework\Module\Status
     */
    protected $_modStatus;

    /**
     * @param \Magento\Eav\Setup\EavSetupFactory        $eavSetupFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\Module\Manager         $moduleManager
     * @param \Magento\Framework\Module\Status          $modStatus
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Eav\Model\Entity\Attribute $entityAttribute,
        \Magento\Framework\Module\Status $modStatus
    ) {
        $this->_resource = $resource;
        $this->_moduleManager = $moduleManager;
        $this->_eavAttribute = $entityAttribute;
        $this->_modStatus = $modStatus;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('dailydeal:disable')
            ->setDescription('Daily Deal Disable Command');
        parent::configure();
    }
    
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->_moduleManager->isEnabled('Webkul_DailyDeals')) {
            $connection = $this->_resource
                ->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
            
            // delete deal product attribute
            $this->_eavAttribute->loadByCode('catalog_product', 'deal_status')->delete();
            $this->_eavAttribute->loadByCode('catalog_product', 'deal_discount_type')->delete();
            $this->_eavAttribute->loadByCode('catalog_product', 'deal_discount_percentage')->delete();
            $this->_eavAttribute->loadByCode('catalog_product', 'deal_value')->delete();
            $this->_eavAttribute->loadByCode('catalog_product', 'deal_from_date')->delete();
            $this->_eavAttribute->loadByCode('catalog_product', 'deal_to_date')->delete();

            // disable daily deals
            $this->_modStatus->setIsEnabled(false, ['Webkul_DailyDeals']);

            // delete entry from setup_module table
            $tableName = $connection->getTableName('setup_module');
            $connection->query("DELETE FROM " . $tableName . " WHERE module = 'Webkul_DailyDeals'");
            $output->writeln('<info>Webkul Daily Deals has been disabled successfully.</info>');
        }
    }
}
