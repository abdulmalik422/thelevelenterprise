<?php

/**
 * Webkul_DailyDeals View On Product Block.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\DailyDeals\Block;

use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigProType;
use Magento\Catalog\Api\ProductRepositoryInterface;

class ViewOnProduct extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Catalog\Block\Product\Context\Registry
     */
    private $coreRegistry;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    private $configProType;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $product;

    /**
     * @var \Webkul\DailyDeals\Helper\Data
     */
    private $helperData;

    /**
     * @param Magento\Catalog\Block\Product\Context   $context
     * @param Webkul\Auction\Helper\Data              $helperData
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Webkul\DailyDeals\Helper\Data $helperData,
        ConfigProType $configProType,
        Product $product,
        ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        $this->coreRegistry = $context->getRegistry();
        $this->helperData = $helperData;
        $this->configProType = $configProType;
        $this->product = $product;
        $this->productRepository = $productRepository;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * @return array Product Deal Detail
     */
    public function getCurrentProductDealDetail()
    {
        $curPro = $this->coreRegistry->registry('current_product');
        $productType = $curPro->getTypeId();
        $assDealDetails = [];
        if ($productType == "configurable") {
            $dataDeal = $this->getConfigAssociateProDeals(true);
        } else {
            $dataDeal = $this->helperData->getProductDealDetail($curPro);
            if ($dataDeal) {
                $dataDeal['entity_id'] = $curPro->getId();
            }
        }
        return $dataDeal;
    }

    /**
     * getChildProductDealDetail
     * @param int $proId
     * @return Magento\Catalog\Model\Product
     */
    public function getChildProductDealDetail($proId)
    {
        $product = $this->productRepository->getById($proId);
        return $this->helperData->getProductDealDetail($product);
    }

    /**
     * getConfigAssociateProDeals
     * @param boolen
     * @return boolen|array
     */

    public function getConfigAssociateProDeals($max = false)
    {
        $configProId = $this->coreRegistry->registry('current_product')->getId();
        $alldeal = [];
        // Added by Ktpl : start
        $assDealDetails = [];
        // Added by Ktpl : End
        $associatedProducts = $this->configProType->getChildrenIds($configProId);
        foreach ($associatedProducts[0] as $assProId) {
            $dealDetail = $this->getChildProductDealDetail($assProId);
            if ($dealDetail && $dealDetail['deal_status'] && isset($dealDetail['diff_timestamp'])) {
                $alldeal[$assProId] = $dealDetail['saved-amount'];
                $dealDetail['entity_id'] = $assProId;
                $dealDetail['pro_type'] = 'configurable';
                $assDealDetails[$assProId] = $dealDetail;
            }
        }
        if (!empty($assDealDetails) && $max) {
            $maxsIndex = array_keys($assDealDetails, max($assDealDetails));
            $dealDetail = isset($assDealDetails[$maxsIndex[0]]) ? $assDealDetails[$maxsIndex[0]] : false;
            return $dealDetail;
        }
        return $assDealDetails;
    }

    /**
     * get current product type
     *
     * @return string
     */
    public function getCurrentProduct()
    {
        return $this->coreRegistry
                                ->registry('current_product');
    }
}
