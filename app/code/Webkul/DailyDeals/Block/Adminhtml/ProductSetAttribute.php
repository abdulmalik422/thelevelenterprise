<?php
/**
 * Webkul_DailyDeals Product Product Attribute Adminhtml Block.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\DailyDeals\Block\Adminhtml;

class ProductSetAttribute extends \Magento\Backend\Block\Template
{
    /**
     * @var string
     */
    public $_template = 'product/setattribute.phtml';

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @param \Magento\Backend\Block\Template\Context      $context
     * @param \Magento\Framework\Registry $coreRegistry     $jsonEncoder
     * @param array                                        $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * getDealsDateTime
     * @return false|string
     */
    public function getDealsDateTime()
    {
        $product = $this->coreRegistry->registry('product');
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $dateFrom = $product->getDealFromDate();
        $dateTo = $product->getDealToDate();
        $dealStatus = $product->getDealStatus();
        $proType = $this->getRequest()->getParam('type');
        $proType = $proType ? $proType : $product->getTypeId();
        return [
            'deal_from_date'=> $dealStatus && $dateFrom ? $this->_localeDate->date($dateFrom)
                ->format('d/m/Y H:i:s') :'',
            'deal_to_date'=> $dealStatus && $dateTo ? $this->_localeDate->date($dateTo)->format('d/m/Y H:i:s'):'',
            'date_format' => $dateFormat,
            'module_enable' => $this->_scopeConfig->getValue('dailydeals/general/enable'),
            'product_type' => $proType
        ];
    }
}
