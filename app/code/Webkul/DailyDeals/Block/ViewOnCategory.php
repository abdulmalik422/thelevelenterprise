<?php

/**
 * Webkul_DailyDeals View On Category Block.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\DailyDeals\Block;

class ViewOnCategory extends \Magento\Catalog\Block\Product\ListProduct
{
}
