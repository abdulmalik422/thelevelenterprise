<?php
namespace Webkul\DailyDeals\Block\Plugin;

/**
 * Webkul DailyDeals ProductListUpdateForDeals plugin.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Catalog\Block\Product\ListProduct;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigProType;
use Magento\Catalog\Model\Product;

class ProductListUpdateForDeals
{
    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    private $configProType;
    
    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $product;

    /**
     * @var \Webkul\DailyDeals\Helper\Data
     */
    private $dailyDealHelper;

    /**
     * @param ConfigProType $configProType
     * @param Product $product
     * @param Webkul\DailyDeals\Helper\Data $dailyDealHelper
     */
    public function __construct(
        ConfigProType $configProType,
        Product $product,
        \Webkul\DailyDeals\Helper\Data $dailyDealHelper
    ) {
        $this->configProType = $configProType;
        $this->product = $product;
        $this->dailyDealHelper = $dailyDealHelper;
    }
 
    /**
     * beforeGetProductPrice // update deal details before price render
     * @param ListProduct                    $list
     * @param \Magento\Catalog\Model\Product $product
     */
    public function beforeGetProductPrice(
        ListProduct $list,
        $product
    ) {
        $dealDetail = $this->dailyDealHelper->getProductDealDetail($product);
    }

    /**
     * aroundGetProductPrice // add clock data html product price
     * @param ListProduct                    $list
     * @param Object                         $proceed
     * @param \Magento\Catalog\Model\Product $product
     */
    public function aroundGetProductPrice(
        ListProduct $list,
        $proceed,
        $product
    ) {
        $dealDetail = $this->getCurrentProductDealDetail($product);
        $dealDetailHtml = "";
        if ($dealDetail && $dealDetail['deal_status'] && isset($dealDetail['diff_timestamp'])) {
            $dealDetailHtml = '<div class="deal wk-daily-deal" data-deal-id="'.$product->getId().'" data-update-url="'
            .$dealDetail['update_url'].'">
            <div class="wk-deal-off-box"><div class="wk-deal-left-border"></div><span class="deal-price-label">'.$dealDetail['discount-percent'].'% '.__('Off')
                                    .'</span></div>
            <span class="price-box ">';
            $saveBox = isset($dealDetail['saved-amount'])? '<span class="wk-save-box "><span class="save-label">'. __('Save On Deal ').'<span class="wk-deal-font-weight-600">'
                                            .$dealDetail['saved-amount'].'</span></span></span><br><br>' : '' ;
            $saveBoxAvl = isset($dealDetail['saved-amount']) ? '' : 'notavilable';
            $dealDetailHtml = $dealDetailHtml.'<div class="wk-deal-timer wk-deal-timer1"><span class="wk-deal-ends-label">'.__('Deal Ends in ').'</span><p class="wk_cat_count_clock wk-deal-font-weight-600'.$saveBoxAvl.'" data-stoptime="'
                                .$dealDetail['stoptime'].'" data-diff-timestamp ="'.$dealDetail['diff_timestamp']
                                .'"></p></div>'.$saveBox.'</div>';
        }
        return $proceed($product).$dealDetailHtml;
    }

    /**
     * @return array Product Deal Detail
     */

    public function getCurrentProductDealDetail($curPro)
    {
        $productType = $curPro->getTypeId();
        $assDealDetails = [];
        if ($productType == "configurable") {
            $dataDeal = $this->getConfigAssociateProDeals($curPro);
        } else {
            $dataDeal = $this->dailyDealHelper->getProductDealDetail($curPro);
            if ($dataDeal) {
                $dataDeal['entity_id'] = $curPro->getId();
            }
        }
        return $dataDeal;
    }

    /**
     * getConfigAssociateProDeals
     * @param Magento\Catalog\Model\Product $curPro
     * @return boolen|array
     */

    public function getConfigAssociateProDeals($curPro)
    {
        $configProId = $curPro->getId();
        $alldeal = [];
        $associatedProducts = $this->configProType->getChildrenIds($configProId);
        foreach ($associatedProducts[0] as $assProId) {
            $dealDetail = $this->getChildProductDealDetail($assProId);
            if ($dealDetail && $dealDetail['deal_status'] && isset($dealDetail['diff_timestamp'])) {
                $alldeal[$assProId] = $dealDetail['saved-amount'];
                $dealDetail['entity_id'] = $assProId;
                $dealDetail['pro_type'] = 'configurable';
                $assDealDetails[$assProId] = $dealDetail;
            }
        }
        if (!empty($assDealDetails)) {
            $maxsIndex = array_keys($assDealDetails, max($assDealDetails));
            $dealDetail = isset($assDealDetails[$maxsIndex[0]]) ? $assDealDetails[$maxsIndex[0]] : false;
            return $dealDetail;
        }
        return false;
    }

    /**
     * getChildProductDealDetail
     * @param int $proId
     * @return Magento\Catalog\Model\Product
     */
    public function getChildProductDealDetail($proId)
    {
        $product = $this->product->load($proId);
        return $this->dailyDealHelper->getProductDealDetail($product);
    }
}
