<?php
/**
 *
 * @category    Webkul
 * @package     Webkul_DailyDeals
 * @author      Webkul
 * @copyright   Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 *
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Webkul_DailyDeals',
    __DIR__
);
