/**
 * Webkul_DailyDeals requirejs config
 * @category    Webkul
 * @package     Webkul_DailyDeals
 * @author      Webkul Software Private Limited
 */


var config = {
    map: {
        '*': {
            viewondeal: 'Webkul_DailyDeals/js/viewoncategory',
            viewondealproduct: 'Webkul_DailyDeals/js/viewonproduct',
            priceBox: 'Webkul_DailyDeals/js/price-box',
        }
    }
};