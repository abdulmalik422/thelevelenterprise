/**
 * Webkul_DailyDeals requirejs config
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

var config = {
    map: {
        '*': {
            setattribute: 'Webkul_DailyDeals/js/setattribute'
        }
    }
};