<?php
namespace Webkul\DailyDeals\Observer;

/**
 * Webkul_DailyDeals Product View Observer.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Event\ObserverInterface;
use Webkul\DailyDeals\Helper\Data as DailyDealsHelperData;

/**
 * Reports Event observer model.
 */
class CatalogControllerProductView implements ObserverInterface
{
     /**
      * @var DailyDealsHelperData
      */
    private $dailyDealsHelperData;

    /**
     * @param DailyDealsHelperData $DailyDealsHelperData
     */

    public function __construct(
        DailyDealsHelperData $dailyDealsHelperData
    ) {
        $this->dailyDealsHelperData = $dailyDealsHelperData;
    }

    /**
     * View Catalog Product View observer.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $dealDetail = $this->dailyDealsHelperData->getProductDealDetail($product);
        return $this;
    }
}
