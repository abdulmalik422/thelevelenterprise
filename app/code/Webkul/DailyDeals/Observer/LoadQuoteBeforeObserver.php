<?php
/**
 * Webkul DailyDeals CatalogProductSaveBefore Observer.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\DailyDeals\Observer;

use Magento\Framework\Event\ObserverInterface;

class LoadQuoteBeforeObserver implements ObserverInterface
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var \Webkul\DailyDeals\Helper\Data
     */
    private $helperData;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * product save event handler.
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeInterface,
     * @param \Magento\Checkout\Model\Cart $cart,
     * @param \Magento\Framework\App\RequestInterface $request,
     * @param \Webkul\DailyDeals\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeInterface,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\DailyDeals\Helper\Data $helperData
    ) {
        $this->productRepository = $productRepository;
        $this->scopeConfig = $scopeInterface;
        $this->cart = $cart;
        $this->helperData = $helperData;
        $this->request = $request;
    }

    /**
     * product save event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $routName = $this->request->getRouteName();
        $actionName = $this->request->getFullActionName();
        $params = $this->request->getParams();
        $updated = false;
        if (!isset($params['key'])) {
            if ($routName != 'adminhtml' && strpos($actionName, 'mpquotesystem') === false) {
                $items = $this->cart->getQuote()->getAllVisibleItems();
                $modEnable = $this->scopeConfig->getValue('mpdailydeals/general/enable');
                foreach ($items as $item) {
                    $product = $this->productRepository->getById($item->getProductId());
                    $dealStatus = $this->helperData->getProductDealDetail($product);
                    $proDealStatus = $product->getDealStatus();
                    $customOptionPrice = $this->getCustomOptionPrice($item->getProduct());
                    if ($dealStatus === false && $modEnable && $proDealStatus) {
                        $item->setPrice($product->getPrice() + $customOptionPrice);
                        $item->setOriginalCustomPrice($product->getPrice() + $customOptionPrice);
                        $item->setCustomPrice($product->getPrice() + $customOptionPrice);
                        $item->getProduct()->setIsSuperMode(true);
                        $updated = true;
                    } elseif ($modEnable && $proDealStatus) {
                        $item->setPrice($product->getSpecialPrice() + $customOptionPrice);
                        $item->setOriginalCustomPrice($product->getSpecialPrice() + $customOptionPrice);
                        $item->setCustomPrice($product->getSpecialPrice() + $customOptionPrice);
                        $item->getProduct()->setIsSuperMode(true);
                        $updated = true;
                    }
                }
                if ($updated) {
                    $this->cart->getQuote()->setTotalsCollectedFlag(false)->collectTotals();
                }
            }
        }
        return $this;
    }
    public function getCustomOptionPrice($product)
    {
        $finalPrice = 0;
        $optionIds = $product->getCustomOption('option_ids');
        if ($optionIds) {
            foreach (explode(',', $optionIds->getValue()) as $optionId) {
                if ($option = $product->getOptionById($optionId)) {
                    $confItemOption = $product->getCustomOption('option_' . $option->getId());

                    $group = $option->groupFactory($option->getType())
                        ->setOption($option)
                        ->setConfigurationItemOption($confItemOption);
                    $finalPrice += $group->getOptionPrice($confItemOption->getValue(), 0);
                }
            }
        }
        return $finalPrice;
    }
}
