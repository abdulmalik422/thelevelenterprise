<?php
/**
 * Webkul DailyDeals CatalogProductSaveBefore Observer.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\DailyDeals\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigProType;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProTypeModel;

class CatalogProductSaveAfter implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $localeDate;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Webkul\DailyDeals\Helper\Data
     */
    private $dailyDealHelper;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    private $configProType;

    /**
     * @var ConfigurableProTypeModel
     */
    protected $_configurableProTypeModel;

    /**
     * @param TimezoneInterface $localeDate,
     * @param ProductRepositoryInterface $productRepository,
     * @param RequestInterface $request,
     * @param ScopeConfigInterface $scopeInterface
     */
    public function __construct(
        TimezoneInterface $localeDate,
        ProductRepositoryInterface $productRepository,
        RequestInterface $request,
        ConfigProType $configProType,
        ScopeConfigInterface $scopeInterface,
        ConfigurableProTypeModel $configurableProTypeModel,
        \Webkul\DailyDeals\Helper\Data $dailyDealHelper
    ) {
        $this->localeDate = $localeDate;
        $this->productRepository = $productRepository;
        $this->request = $request;
        $this->configProType = $configProType;
        $this->scopeConfig = $scopeInterface;
        $this->_configurableProTypeModel = $configurableProTypeModel;
        $this->dailyDealHelper = $dailyDealHelper;
    }

    /**
     * product save event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();
        $productData = $this->request->getParam('product');
        $modEnable = $this->scopeConfig->getValue('dailydeals/general/enable');
        if ($modEnable) {
            $token = false;
            if (isset($productData['deal_to_date_tmp'])) {
                $dealToDate = $productData['deal_to_date_tmp'];
            } else {
                $dealToDate = $productData['deal_to_date'];
            }
            if (isset($productData['deal_from_date_tmp'])) {
                $dealFromDate = $productData['deal_from_date_tmp'];
            } else {
                $dealFromDate = $productData['deal_from_date'];
            }
            $config = $this->_configurableProTypeModel->getParentIdsByChild($product->getEntityId());
            if (isset($config[0])) {
                if ($productData['deal_status']) {
                    $token = true;
                } else {
                    $associatedProducts = $this->configProType->getChildrenIds($config[0]);
                    foreach ($associatedProducts[0] as $assProId) {
                        $associatedProduct = $this->productRepository->getById($assProId);
                        if ($associatedProduct->getDealStatus()) {
                            $dealToDate = $associatedProduct->getDealToDate();
                            $dealFromDate = $associatedProduct->getDealFromDate();
                            $token = true;
                            break;
                        }
                    }
                }
                $configproduct = $this->productRepository->getById($config[0]);
                if ($token) {
                    $configproduct->setDealStatus(1);
                    $configproduct->setDealToDate($dealToDate);
                    $configproduct->setDealFromDate($dealFromDate);
                    $configproduct->setDealValue(1);
                    $configproduct->setDealDiscountType('fixed');
                    $this->productRepository->save($configproduct, true);
                } else {
                    $configproduct->setDealStatus(0);
                    $configproduct->setDealToDate('');
                    $configproduct->setDealFromDate('');
                    $configproduct->setDealValue(0);
                    $configproduct->setDealDiscountType('fixed');
                    $this->productRepository->save($configproduct, true);
                }
            }
        }
        return $this;
    }
}
