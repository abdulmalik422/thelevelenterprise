<?php
/**
 * Webkul DailyDeals CatalogProductSaveBefore Observer.
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\DailyDeals\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class CatalogProductSaveBefore implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $localeDate;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param TimezoneInterface $localeDate,
     * @param ProductRepositoryInterface $productRepository,
     * @param RequestInterface $request,
     * @param ScopeConfigInterface $scopeInterface
     */
    public function __construct(
        TimezoneInterface $localeDate,
        ProductRepositoryInterface $productRepository,
        RequestInterface $request,
        ScopeConfigInterface $scopeInterface
    ) {
        $this->localeDate = $localeDate;
        $this->productRepository = $productRepository;
        $this->request = $request;
        $this->scopeConfig = $scopeInterface;
    }

    /**
     * product save event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();
        // echo "<pre>";
        // print_r(json_encode($product->getData()));
        // die();
        $productData = $this->request->getParam('product');
        $modEnable = $this->scopeConfig->getValue('dailydeals/general/enable');
        if ($product->getDealStatus() && $modEnable) {
            $configTimeZone = $this->localeDate->getConfigTimezone();
            $defaultTimeZone = $this->localeDate->getDefaultTimezone();
            if (isset($productData['deal_to_date_tmp'])) {
                $dealToDate = $productData['deal_to_date_tmp'];
            } else {
                $dealToDate = $productData['deal_to_date'];
            }
            if (isset($productData['deal_from_date_tmp'])) {
                $dealFromDate = $productData['deal_from_date_tmp'];
            } else {
                $dealFromDate = $productData['deal_from_date'];
            }
            $dealToDate = $dealToDate == '' ? $productData['deal_to_date'] : $dealToDate;
            $dealFromDate = $dealFromDate == '' ? $productData['deal_from_date'] : $dealFromDate;
    
            if ($dealToDate != '' && $dealFromDate != '') {
                $product->setDealFromDate($dealFromDate);
                $product->setDealToDate($dealToDate);
            }
        } elseif ($product->getEntityId() && $modEnable) {
            $proDealStatus = $this->productRepository->getById($product->getEntityId())->getDealStatus();
            //To Do for default special price of magneto
            if ($proDealStatus) {
                $product->setSpecialToDate('');
                $product->setSpecialFromDate('');
                $product->setSpecialPrice(null);
                $product->setDealDiscountPercentage('');
            }
        }
        return $this;
    }
}
