<?php
namespace Webkul\DailyDeals\Helper;

/**
 * Webkul_DailyDeals data helper
 * @category  Webkul
 * @package   Webkul_DailyDeals
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Downloadable\Api\LinkRepositoryInterface;
use Magento\Bundle\Api\ProductOptionRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProTypeModel;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $localeDate;

    /**
     * @var PricingHelper
     */
    private $pricingHelper;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var LinkRepositoryInterface
     */
    private $linkRepositoryInterface;

    /**
     * @var ConfigurableProTypeModel
     */
    protected $_configurableProTypeModel;

    /**
     * @param \Magento\Framework\App\Helper\Context $context,
     * @param ProductRepositoryInterface $productRepository,
     * @param TimezoneInterface $localeDate,
     * @param PricingHelper $pricingHelper,
     * @param LinkRepositoryInterface $linkRepositoryInterface
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        ProductRepositoryInterface $productRepository,
        TimezoneInterface $localeDate,
        PricingHelper $pricingHelper,
        LinkRepositoryInterface $linkRepositoryInterface,
        ConfigurableProTypeModel $configurableProTypeModel,
        ProductOptionRepositoryInterface $productOption
    ) {
        $this->localeDate = $localeDate;
        $this->pricingHelper = $pricingHelper;
        $this->productRepository = $productRepository;
        $this->linkRepositoryInterface = $linkRepositoryInterface;
        $this->productOption = $productOption;
        $this->_configurableProTypeModel = $configurableProTypeModel;
        parent::__construct($context);
    }

    /**
     * @param object $product
     * @return array|false []
     */
    public function getProductDealDetail($product)
    {
        $product = $this->productRepository->getById($product->getEntityId());
        $dealStatus = $product->getDealStatus();
        $content = false;
        $modEnable = $this->scopeConfig->getValue('dailydeals/general/enable');
        if ($dealStatus && $modEnable && $product->getDealValue()) {
            $content = ['deal_status' => $dealStatus];
            $today = $this->localeDate->date()->format('Y-m-d H:i:s');
            $dealFromDateTime = $this->localeDate->date(strtotime($product->getDealFromDate()))->format('Y-m-d H:i:s');
            $dealToDateTime = $this->localeDate->date(strtotime($product->getDealToDate()))->format('Y-m-d H:i:s');
            $difference = strtotime($dealToDateTime) - strtotime($today);
            $specialPrice = $product->getSpecialPrice();
            $price = $product->getPrice();
            if ($modEnable && $difference > 0 && $dealFromDateTime < $today) {
                $content['update_url'] = $this->_urlBuilder->getUrl('dailydeals/index/updatedealinfo');
                $content['stoptime'] = $product->getSpecialToDate();
                $content['diff_timestamp'] = $difference;
                $content['discount-percent'] = $product->getDealDiscountPercentage();
                if ($product->getTypeId() != 'bundle') {
                    $content['saved-amount'] = $this->pricingHelper->currency($price - $specialPrice, true, false);
                }
                if ($product->getTypeId() != 'configurable') {
                    $this->setPriceAsDeal($product);
                }
            } elseif ($modEnable && $dealToDateTime < $today) {
                $token = 0;
                
                if ($product->getDealStatus()) {
                    $token = 1;
                }
                if ($token) {
                    $product->setSpecialToDate(date("m/d/Y", strtotime('-1 day')));
                    $product->setSpecialFromDate(date("m/d/Y", strtotime('-2 day')));
                    $product->setDealStatus(0);
                    $this->productRepository->save($product, true);
                    $content = false;
                }
                $content = false;
            }
        }
        return $content;
    }

    /**
     * setPriceAsDeal
     * @param ProductRepositoryInterface $product
     * @return void
     */
    public function setPriceAsDeal($product)
    {
        $proType = $product->getTypeId();
        $dealvalue = $product->getDealValue();
        if ($product->getDealDiscountType() == 'percent') {
            if ($proType != 'bundle') {
                $price = $product->getPrice() * ($dealvalue/100);
            } else {
                $price = $dealvalue;
                //$product->setPrice(null);
            }
            $discount = $dealvalue;
        } else {
            $price = $dealvalue;
            $discount = ($dealvalue/$product->getPrice())*100;
        }

        $token = 0;
        if ($product->getSpecialPrice()) {
            $token = 1;
        }

        $product->setSpecialFromDate(date("m/d/Y", strtotime($product->getDealFromDate())));
        $product->setSpecialToDate(date("m/d/Y", strtotime($product->getDealToDate())));
        $product->setSpecialPrice($price);
        $product->setDealDiscountPercentage(round(100-$discount));
        if ($proType != 'bundle') {
            if (!$token) {
                $this->productRepository->save($product, true);
                $config = $this->_configurableProTypeModel->getParentIdsByChild($product->getEntityId());
                try {
                    if ($this->request->getFullActionName() == 'catalog_product_view' && !isset($config[0])) {
                        $this->response->setRedirect($product->getProductUrl());
                    }
                } catch (\Exception $e) {
                }
                $product->unsetDealStatus();
            }
        } else {
            $optionCollection = $this->productOption->getList($product->getSku());
            $selectionCollection = $product->getTypeInstance()->getSelectionsCollection(
                $product->getTypeInstance()->getOptionsIds($product),
                $product
            );
            foreach ($optionCollection as $option) {
                if ($option->getRequired() && count($option->getSelections()) == 1) {
                    $selections = array_merge($selectionCollection, $option->getSelections());
                } else {
                    $selections = [];
                    break;
                }
            }
            //$options = $optionCollection;//->appendSelections($selectionCollection);
            $extension = $product->getExtensionAttributes();
            $extension->setBundleProductOptions($selections);
            $product->setExtensionAttributes($extension);
            $this->productRepository->save($product, true);
        }
    }
}
