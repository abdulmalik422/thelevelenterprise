<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Quickview
 */


namespace Amasty\Quickview\Plugin\Catalog\Helper\Product;

use Magento\Catalog\Helper\Product\View as MagentoView;
use Magento\Framework\View\Result\Page as ResultPage;
use Magento\Framework\App\RequestInterface;

class View
{
    /**
     * @var RequestInterface
     */
    private $request;

    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    public function beforeInitProductLayout(
        MagentoView $subject,
        ResultPage $resultPage,
        $product,
        $params = null
    ) {
        if ((int)$this->request->getParam('amasty_quickview', 0)) {
            $resultPage->addHandle('amasty_quickview_ajax_view');
        }

        return [$resultPage, $product, $params];
    }
}
