<?php

namespace Ktpl\Blog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface {

	/**
     * {@inheritdoc}
     * @SuppressWarnings(PHPD, ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {

    	$setup->startSetup();

    	$tableName = $setup->getTable('magefan_blog_post');
    	$setup->getConnection()->addColumn($tableName, 'is_home_page', [
            'type' 		=> \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            'length'    => 1,
            'nullable'	=> false,
            'default' 	=> '0',
            'comment' 	=> 'Is Homepage'
        ]);

    	$setup->endSetup();
    }
}