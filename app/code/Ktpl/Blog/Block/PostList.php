<?php

namespace Ktpl\Blog\Block;

class PostList extends \Magento\Framework\View\Element\Template {

	/**
     * @var \Magefan\Blog\Model\ResourceModel\Post\CollectionFactory
     */
    protected $_postCollectionFactory;

    /**
     * @var \Magefan\Blog\Model\Url
     */
    protected $_url;

	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magefan\Blog\Model\ResourceModel\Post\CollectionFactory $postCollectionFactory,
        \Magefan\Blog\Model\Url $url,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_postCollectionFactory = $postCollectionFactory;
        $this->_url = $url;
    }

    public function getHomePostCollection() {
    	$postCollection = $this->_postCollectionFactory->create()
            ->addFieldToSelect(['post_id', 'featured_img', 'title', 'content', 'identifier'])
            ->addActiveFilter()
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addFieldToFilter('is_home_page', 1)
            ->addFieldToFilter('main_table.is_active', 1)
            ->setPageSize(3)
            ->setOrder('creation_time', 'DESC');

        $postCollection->getSelect()
            ->joinLeft(['post_cat' => 'magefan_blog_post_category'], 'post_cat.post_id = main_table.post_id')
            ->joinLeft(['category' => 'magefan_blog_category'], 'category.category_id = post_cat.category_id', "GROUP_CONCAT(category.title SEPARATOR ', ') AS cat_title")
            ->group('post_cat.post_id');
            
        return $postCollection;
    }

    public function getImage($image) {
    	return $this->_url->getMediaUrl($image);
    }

    public function getPostUrl($identifier) {
    	return $this->_url->getUrlPath($identifier, 'post');
    }
}