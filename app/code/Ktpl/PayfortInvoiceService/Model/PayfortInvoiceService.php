<?php

namespace Ktpl\PayfortInvoiceService\Model;

use Magedelight\Payfort\Gateway\Config\Config;
use Magento\Framework\Encryption\EncryptorInterface;

class PayfortInvoiceService extends \Magento\Payment\Model\Method\AbstractMethod
{
    const CODE              = 'ktpl_payfortinvoiceservice';
    const SERVICE_COMMAND   = 'PAYMENT_LINK';
    const LINK_COMMAND      = 'PURCHASE';//'PURCHASE' OR 'AUTHORIZATION'
    const NOTIFICATION_TYPE = 'EMAIL';//'EMAIL' OR 'SMS'
    const PAYMENT_OPTION    = '';//'VISA' OR 'MASTERCARD' OR 'AMEX';
    const TEST_REQUEST_URL  = 'https://sbpaymentservices.PayFort.com/FortAPI/paymentApi';
    const LIVE_REQUEST_URL  = 'https://paymentservices.PayFort.com/FortAPI/paymentApi';

    protected $_code = self::CODE;

    protected $_canAuthorize = true;

    protected $_canUseCheckout = false;
  
    protected $_moduleList;

    protected $_localeDate;

    protected $_storeManager;

    protected $_localeResolver;

    protected $_messageManager;

    protected $_urlBuilder;

    private $encryptor;

    protected $payfortConfig;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\Resolver $localeResolver,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\UrlInterface $urlBuilder,
        EncryptorInterface $encryptor,
        Config $config,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
        $this->_moduleList = $moduleList;
        $this->_localeDate = $localeDate;
        $this->_storeManager = $storeManager;
        $this->_localeResolver = $localeResolver;
        $this->_messageManager = $messageManager;
        $this->_messageManager = $messageManager;
        $this->_urlBuilder = $urlBuilder;
        $this->encryptor = $encryptor;
        $this->payfortConfig = $config;
    }

    /**
     * Authorize a payment.
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     */
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {        
        $storeManager   = $this->_storeManager;
        $messageManager = $this->_messageManager;
        $resolver       = $this->_localeResolver;
        $isTestMode     = $this->payfortConfig->getIsTestMode();
        if($isTestMode){
            $requestUrl = self::TEST_REQUEST_URL;
        } else {
            $requestUrl = self::LIVE_REQUEST_URL;
        }
        
        try {
            $order          = $payment->getOrder();
            $incrementId    = $order->getIncrementId();
            $billingAddress = $order->getBillingAddress();
            
            $current_currency = $storeManager->getStore()->getCurrentCurrency()->getCode();
            $base_currency = $storeManager->getStore()->getBaseCurrency()->getCode();
            $amount_ordered = $payment->getAmountOrdered();
            $amount = $this->convertFortAmount($amount_ordered,$current_currency);
            $language_code = strstr($resolver->getLocale(), '_', true);
            $link_expiry_date = date("Y-m-d\TH:i:sP", strtotime("+2 days"));

            //build array of payment data for API request.
            $service_command    = self::SERVICE_COMMAND;
            $access_code        = $this->encryptor->decrypt($this->payfortConfig->getAccessCode());
            $merchant_identifier= $this->encryptor->decrypt($this->payfortConfig->getMerchantIdentifier());
            $merchant_reference = $incrementId;
            $currency           = $current_currency;
            $language           = $language_code;
            $customer_email     = $billingAddress->getEmail();
            $request_expiry_date= $link_expiry_date;
            $notification_type  = self::NOTIFICATION_TYPE;
            $link_command       = self::LINK_COMMAND;
            $payment_link_id    = $incrementId;
            $payment_option     = self::PAYMENT_OPTION;
            $order_description  = '';
            $customer_name      = $billingAddress->getFirstname().' '.$billingAddress->getLastname();
            $customer_phone     = $billingAddress->getTelephone();
            $return_url         = $this->_urlBuilder->getBaseUrl();

            /*signature parms*/
            $signType           = 'request';
            $ShaRequestPhrase   = $this->encryptor->decrypt($this->payfortConfig->getRequestSha());
            $ShaResponsePhrase  = $this->encryptor->decrypt($this->payfortConfig->getResponseSha());
            $ShaType            = $this->payfortConfig->getShaType();//'sha256';//'SHA-256';

            $request = [
                'service_command'       => $service_command,
                'access_code'           => $access_code,
                'merchant_identifier'   => $merchant_identifier,
                'merchant_reference'    => $merchant_reference,
                'amount'                => $amount,
                'currency'              => $currency,
                'language'              => $language,
                'customer_email'        => $customer_email,
                'request_expiry_date'   => $request_expiry_date,
                'notification_type'     => $notification_type,
                'link_command'          => $link_command,
                'payment_link_id'       => $payment_link_id,
                'customer_name'         => $customer_name,
                'customer_phone'        => $customer_phone,
                'return_url'            => $return_url
            ];

            if(!empty($payment_option))
                $request['payment_option'] = $payment_option;

            if(!empty($order_description))
                $request['order_description'] = $order_description;

            $request['signature'] = $this->signatureString($request, $signType, $ShaRequestPhrase, $ShaResponsePhrase, $ShaType);

            //check if payment has been authorized
            $response = $this->makeAuthRequest($request, $requestUrl);

        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }
        
        if(isset($response['transactionID'])) {
            // Successful auth request.
            // Set the transaction id on the payment so the capture request knows auth has happened.
            $payment->setTransactionId($response['transactionID']);
            $payment->setParentTransactionId($response['transactionID']);
        }

        //processing is not done yet.
        $payment->setIsTransactionClosed(0);

        return $this;
    }

    /**
     * Set the payment action to authorize_and_capture
     *
     * @return string
     */
    public function getConfigPaymentAction()
    {
        return self::ACTION_AUTHORIZE; /*return self::ACTION_AUTHORIZE_CAPTURE;*/
    }

    /**
     * API call for authorization request.
     *
     * @param $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function makeAuthRequest($request, $requestUrl)
    {
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0";
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8'));
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_ENCODING, "compress, gzip");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects     
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); // The number of seconds to wait while trying to connect
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));

        $response = curl_exec($ch);
        curl_close($ch);
        $array_result = json_decode($response, true);

        if (!$response || empty($array_result)) {
            return false;
        }

        if(isset($array_result['amount']) && isset($array_result['currency']))
        {
            $amn = $array_result['amount'];
            $currency = $array_result['currency'];
            $amount = $this->castAmountFromFort($amn,$currency);
            $array_result['amount'] = $amount;
        }

        if($array_result['status'] == '00'){
            throw new \Magento\Framework\Exception\LocalizedException(__('There was a problem with the Payfort payment response : %1', $array_result['response_message']));

            return false;
        }

        return $array_result;
    }

    public function signatureString($request, $signType, $ShaRequestPhrase, $ShaResponsePhrase, $ShaType)
    {
        $ShaString = '';
        ksort($request);
        foreach ($request as $k => $v) {
            $ShaString .= "$k=$v";
        }

        if ($signType == 'request') 
            $ShaString = $ShaRequestPhrase . $ShaString . $ShaRequestPhrase;
        else 
            $ShaString = $ShaResponsePhrase . $ShaString . $ShaResponsePhrase;

        $signature = hash($ShaType, $ShaString);
        return $signature;
    }

    /**
     * Convert Amount with dicemal points
     * @param decimal $amount
     * @param string  $currencyCode
     * @return decimal
     */
    public function convertFortAmount($amount, $currencyCode)
    {
        $new_amount = 0;
        $total = $amount;
        $decimalPoints = $this->getCurrencyDecimalPoints($currencyCode);
        $new_amount    = round($total, $decimalPoints) * (pow(10, $decimalPoints));
        return $new_amount;
    }

    public function castAmountFromFort($amount, $currencyCode)
    {
        $decimalPoints = $this->getCurrencyDecimalPoints($currencyCode);
        $new_amount    = round($amount, $decimalPoints) / (pow(10, $decimalPoints));
        return $new_amount;
    }
    /**
     * 
     * @param string $currency
     * @param integer 
     */
    public function getCurrencyDecimalPoints($currency)
    {
        $decimalPoint  = 2;
        $arrCurrencies = array(
            'JOD' => 3,
            'KWD' => 3,
            'OMR' => 3,
            'TND' => 3,
            'BHD' => 3,
            'LYD' => 3,
            'IQD' => 3,
        );
        if (isset($arrCurrencies[$currency])) {
            $decimalPoint = $arrCurrencies[$currency];
        }
        return $decimalPoint;
    }
}