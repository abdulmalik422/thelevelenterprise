<?php

namespace Ktpl\PaymentFee\Block\Adminhtml\Sales\Order\Creditmemo;

class Totals extends \Magento\Framework\View\Element\Template {

    protected $_helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context, 
        \Ktpl\PaymentFee\Helper\Data $helper, 
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_helper = $helper;
    }

    /**
     * Get data (totals) source model
     *
     * @return \Magento\Framework\DataObject
     */
    public function getSource() {
        return $this->getParentBlock()->getSource();
    }

    public function getCreditmemo() {
        return $this->getParentBlock()->getCreditmemo();
    }

    /**
     * Initialize payment fee totals
     *
     * @return $this
     */
    public function initTotals() {
        $this->getParentBlock();
        $this->getCreditmemo();
        $this->getSource();
        
        if (!$this->getParentBlock()->getOrder()->getFeeAmount() || $this->getParentBlock()->getOrder()->getFeeAmount() == 0) {
            return $this;
        }

        $fee = new \Magento\Framework\DataObject([
            'code' => 'fee',
            'strong' => false,
            'value' => $this->getParentBlock()->getOrder()->getFeeAmount(),
            'label' => !empty($this->getParentBlock()->getOrder()->getFeeLabel()) ? $this->getParentBlock()->getOrder()->getFeeLabel() : $this->_helper->getTitle($this->getSource()),
        ]);
        
        $this->getParentBlock()->addTotalBefore($fee, 'grand_total');
        

        return $this;
    }

}
