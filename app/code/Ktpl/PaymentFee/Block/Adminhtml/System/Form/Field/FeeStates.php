<?php

namespace Ktpl\PaymentFee\Block\Adminhtml\System\Form\Field;

class FeeStates extends \Magento\Framework\View\Element\Html\Select {

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $_regionFactory;

    /**
     * Activation constructor.
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Config\Model\Config\Source\Enabledisable $enableDisable $enableDisable
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_regionFactory = $regionFactory;
    }

    /**
     * @param string $value
     * @return Magently\Tutorial\Block\Adminhtml\Form\Field\Activation
     */
    public function setInputName($value) {
        return $this->setName($value);
    }

    /**
     * Parse to html.
     *
     * @return mixed
     */
    public function _toHtml() {
        if (!$this->getOptions()) {
            $regions = $this->_regionFactory->create()->getCollection()
                ->addFieldToSelect(['region_id', 'default_name'])
                ->addFieldToFilter('country_id', ['eq' => 'SA']);

                
            if ($regions->count()) {
                foreach ($regions as $val) {
                    $this->addOption($val->getRegionId(), $val->getDefaultName());
                }
            }
        }
        return parent::_toHtml();
    }

}
