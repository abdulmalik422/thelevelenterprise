<?php

namespace Ktpl\OrderSuccess\Observer\Order;

use Magento\Checkout\Model\Session as CheckoutSession;
use Ktpl\SocialLogin\Helper\SmsApi;

class OrderSuccess implements \Magento\Framework\Event\ObserverInterface {

    /**
     * @var \Ktpl\Core\Helper\SmsApi
     */
    protected $smsApi;

    /**
     * @var CheckoutSession
     */
    private $_checkoutSession;
    protected $orderCollectionFactory;

    /**
     * OrderSuccess constructor.
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Ktpl\Core\Helper\SmsApi $smsApi
     */
    public function __construct(
    SmsApi $smsApi, CheckoutSession $checkoutSession, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    ) {
        $this->smsApi = $smsApi;
        $this->_checkoutSession = $checkoutSession;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $lastOrder = $this->_checkoutSession->getLastRealOrder();
        try {
            $mobileNo = $lastOrder->getShippingAddress()->getTelephone();            
            if ($mobileNo && $lastOrder->getCustomerIsGuest()) {
                //$incrementId = $lastOrder->getIncrementId();
                //$orderTotal = $lastOrder->getGrandTotal();
                $otp = $this->generateOtp();
                $this->smsApi->saveMobileOtp(['mobile' => $mobileNo, 'otp' => $otp]);
                $message = $this->smsApi->getOrderOtpMessage($otp);
                $this->smsApi->sendMessageUsingPrimaryService($mobileNo, $message, '');
                $lastOrder->save();
            }
        } catch (\Exception $e) {
            echo $e->getTraceAsString();die;
        }
    }

    protected function generateOtp() {
        return rand(pow(10, 4 - 1), pow(10, 4) - 1);
    }

}
