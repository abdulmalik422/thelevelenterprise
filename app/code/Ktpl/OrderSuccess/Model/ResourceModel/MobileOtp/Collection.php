<?php

namespace Ktpl\OrderSuccess\Model\ResourceModel\MobileLogin;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = 'id';

    /**
     * Resource initialization
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Ktpl\OrderSuccess\Model\MobileOtp',
            'Ktpl\OrderSuccess\Model\ResourceModel\MobileOtp'
        );
    }
}
