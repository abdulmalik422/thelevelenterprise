<?php

namespace Ktpl\OrderSuccess\Model\ResourceModel;

/**
 * Productattach Resource Model
 */
class MobileOtp extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('ktpl_mobileotp', 'id');
    }
}
