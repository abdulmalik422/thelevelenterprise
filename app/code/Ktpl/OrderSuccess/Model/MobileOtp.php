<?php

namespace Ktpl\OrderSuccess\Model;

use Magento\Framework\Model\AbstractModel;

class MobileOtp extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Ktpl\OrderSuccess\Model\ResourceModel\MobileOtp');
    }
}