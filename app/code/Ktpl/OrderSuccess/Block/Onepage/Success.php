<?php

namespace Ktpl\OrderSuccess\Block\Onepage;

class Success extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    protected $orderCollectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_checkoutSession = $checkoutSession;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    public function isOtpBlockEnable()
    {
        $order = $this->_checkoutSession->getLastRealOrder();
        if($order && $order->getCustomerIsGuest())
        {
            return true;
        }
        return false;
    }

    public function getLastOrderTelephone()
    {
        $order = $this->_checkoutSession->getLastRealOrder();
        return $order->getShippingAddress()->getTelephone();
    }
}
