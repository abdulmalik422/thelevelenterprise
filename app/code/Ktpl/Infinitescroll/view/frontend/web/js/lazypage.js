/*
**	Anderson Ferminiano
**	contato@andersonferminiano.com -- feel free to contact me for bugs or new implementations.
**	jQuery ScrollPagination
**	28th/March/2011
**	http://andersonferminiano.com/jqueryscrollpagination/
**	You may use this script for free, but keep my credits.
**	Thank you.
*/
define(["jquery"], function($){
(function( $ ){


 $.fn.scrollPagination = function(options) {

		var opts = $.extend($.fn.scrollPagination.defaults, options);
		var target = opts.scrollTarget;
		if (target == null){
			target = obj;
	 	}
		opts.scrollTarget = target;
		return this.each(function() {
               // alert('hello');
		  $.fn.scrollPagination.init($(this), opts);
		});

  };

  $.fn.stopScrollPagination = function(){
	  return this.each(function() {
	 	$(this).attr('scrollPagination', 'disabled');

	  });

  };

  $.fn.scrollPagination.loadContent = function(obj, opts){
	 var target = opts.scrollTarget;
	 var mayLoadContent = $(target).scrollTop()+opts.heightOffset >= $(document).height() - $(target).height();
	 var status=jQuery('#productdata li').last().attr("status");

	 var item_per_page= parseInt($('#item_per_page').val());
	 var page=($('#productdata li').last().attr("rel") / item_per_page) + 1;

	if(page % 1 === 0)
    {

		 if(status!='loading'){
		 if (mayLoadContent){

			 if (opts.beforeLoad != null){
				opts.beforeLoad();
			 }
			// $(obj).children().attr('rel', 'loaded');
			var nextpage=jQuery('#productdata li').last().attr("page");

			 $.ajax({
				  type: 'POST',
				  url: opts.contentPage + nextpage,
				  data: opts.contentData,
				  success: function(data){
					$(obj).append(data);
					var objectsRendered = $(obj).children('[rel!=loaded]');

					if (opts.afterLoad != null){
						opts.afterLoad(objectsRendered);
					}
				  },
				  dataType: 'html'
			 });
		 }
		 }
	}
  };

  $.fn.scrollPagination.init = function(obj, opts){
	 var target = opts.scrollTarget;
	 $(obj).attr('scrollPagination', 'enabled');

	 $(target).scroll(function(event){

		if ($(obj).attr('scrollPagination') == 'enabled'){

	 		$.fn.scrollPagination.loadContent(obj, opts);
		}
		else {
			event.stopPropagation();
		}
	 });

	 $.fn.scrollPagination.loadContent(obj, opts);

 };

 $.fn.scrollPagination.defaults = {
      	 'contentPage' : null,
     	 'contentData' : {},
		 'beforeLoad': null,
		 'afterLoad': null	,
		 'scrollTarget': null,
		 'heightOffset': 0
 };
})( jQuery );
});