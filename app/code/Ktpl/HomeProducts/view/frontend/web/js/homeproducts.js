require(['jquery', 'slick'], function ($) {
    $(document).ready(function () {
        $('.featured-slider, .topsellers-slider, .trending-slider, .shopbylooks-slider').slick({
            infinite: true,
            dots: false,
            arrows: true,
            slidesToShow: 4,
            autoplay: false,
            autoplaySpeed: 2000,
            centerMode: true,
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3  
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,  
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 568,
                    settings: {
                        slidesToShow: 2,  
                        slidesToScroll: 2
                    }
                },
            ]
        });
    });
});