<?php

namespace Ktpl\HomeProducts\Block;

class Products extends \Magento\Framework\View\Element\Template {
    
    private $currentStoreId;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    
    /**
     * @var \Ktpl\General\Helper\Data
     */
    protected $_generalHelper;
    
    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $_productStatus;
    
    /**
     * @var \Magento\Catalog\Model\Product\Visibility 
     */
    protected $_productVisibility;
    
    /**
     * @var \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory 
     */
    protected $_collectionFactory;

    /**
     * @var \Magedelight\Looknbuy\Model\Looknbuy\ResourceModel\Looknbuy\CollectionFactory
     */
    protected $_looknbuyCollection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $_storeManager;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory $collectionFactory
     * @param \Ktpl\General\Helper\Data $generalHelper
     * @param \Magedelight\Looknbuy\Model\Looknbuy\ResourceModel\Looknbuy\CollectionFactory $looknbuyCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory $collectionFactory,
        \Ktpl\General\Helper\Data $generalHelper,
        \Magedelight\Looknbuy\Model\ResourceModel\Looknbuy\CollectionFactory $looknbuyCollection,
        array $data = array()
    ) {
        $this->_productCollectionFactory    = $productCollectionFactory;
        $this->_generalHelper               = $generalHelper;
        $this->_productStatus               = $productStatus;
        $this->_productVisibility           = $productVisibility;
        $this->_collectionFactory           = $collectionFactory;
        $this->_looknbuyCollection          = $looknbuyCollection;
        $this->_storeManager                = $storeManager;
        $this->currentStoreId               = $this->_storeManager->getStore()->getId();
        
        parent::__construct($context, $data);
    }
    
    public function getHomeProducts($type) {
        if ($type == "featured") {
            $pageSize = $this->_generalHelper->getScopeConfigValue('homeproducts/homesettings/featuredproducts');
            $attribute = 'is_featured';
        }
        else if ($type == "trending") {
            $pageSize = $this->_generalHelper->getScopeConfigValue('homeproducts/homesettings/trendingnowproducts');
            $attribute = 'is_trending';
        }
        
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect(['name', 'small_image', 'price', 'special_price', 'tier_price', 'unit', 'brand'])
                ->addAttributeToFilter($attribute, 1)
                ->addStoreFilter($this->currentStoreId)
                ->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()])
                ->setVisibility($this->_productVisibility->getVisibleInSiteIds())
                ->setPageSize($pageSize);
        
        return $collection;
    }
    
    public function getTopSellingProducts() {
        $pageSize = $this->_generalHelper->getScopeConfigValue('homeproducts/homesettings/topsellingproducts');
        $collection = $this->_collectionFactory->create()
            ->setModel('Magento\Catalog\Model\Product')
            ->setPeriod('month')
            ->setPageSize($pageSize);
        
        return $collection;
    }

    public function getShopByLooksCollection() {
        $look_id = $this->getRequest()->getParam('look_id');
        $pageSize = $this->_generalHelper->getScopeConfigValue('homeproducts/homesettings/looksweloveproducts');
        $collection = $this->_looknbuyCollection->create();
        $collection->addFieldToSelect('*')
                ->addFieldToFilter('status', array('eq' => 1));
        if ($look_id) {
            $collection->addFieldToFilter('look_id', array('neq' => $look_id));
        }
        $collection->setPageSize($pageSize);
        return $collection;
    }
}