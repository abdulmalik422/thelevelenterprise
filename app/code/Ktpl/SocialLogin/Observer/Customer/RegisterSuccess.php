<?php

namespace Ktpl\SocialLogin\Observer\Customer;

use Ktpl\SocialLogin\Helper\Data;
use Ktpl\SocialLogin\Helper\SmsApi;
use Magento\Framework\Event\Observer;

class RegisterSuccess implements \Magento\Framework\Event\ObserverInterface
{
    protected $ktplHelper;

    protected $smsApi;

    /**
     * RegisterSuccess constructor.
     * @param Data $ktplHelper
     * @param SmsApi $smsApi
     */
    public function __construct(
        Data $ktplHelper,
        SmsApi $smsApi
    ) {
        $this->ktplHelper = $ktplHelper;
        $this->smsApi = $smsApi;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        if($customer->getId()) {
            $mobile = $this->ktplHelper->getCustomerMobile($customer->getId());
            if($mobile) {
                $message = $this->smsApi->getCustomerRegistrationMessage($mobile);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'customer_registration');
            }
        }
    }
}