<?php

namespace Ktpl\SocialLogin\Observer\Order;

use Ktpl\SocialLogin\Helper\SmsApi;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class OrderSuccess implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var SmsApi
     */
    protected $smsApi;
    /**
     * @var CollectionFactory
     */
    protected $orderCollectionFactory;
    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;
    /**
     * @var CheckoutSession
     */
    private $_checkoutSession;

    /**
     * OrderSuccess constructor.
     * @param SmsApi $smsApi
     * @param CheckoutSession $checkoutSession
     * @param CollectionFactory $orderCollectionFactory
     */
    public function __construct(
        SmsApi $smsApi,
        CheckoutSession $checkoutSession,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CollectionFactory $orderCollectionFactory
    )
    {
        $this->smsApi = $smsApi;
        $this->_checkoutSession = $checkoutSession;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $lastOrder = $this->_checkoutSession->getLastRealOrder();

        try {
            //$mobile = $lastOrder->getShippingAddress()->getTelephone();
            $mobile = $this->_customerRepositoryInterface->getById($lastOrder->getCustomerId())->getCustomAttribute('mobile_no')->getValue();
            if ($mobile) {
                $incrementId = $lastOrder->getIncrementId();
                $orderTotal = $lastOrder->getGrandTotal();
                $message = $this->smsApi->getOrderSuccessMessage($incrementId, $orderTotal);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'order_success');
            }
        } catch (\Exception $e) {
        }
    }
}
