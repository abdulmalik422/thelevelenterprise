<?php

namespace Ktpl\SocialLogin\Observer\Order;

use Ktpl\SocialLogin\Helper\SmsApi;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Api\OrderRepositoryInterface;

class OrderShipment implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Ktpl\Core\Helper\SmsApi
     */
    protected $smsApi;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * OrderShipment constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param SmsApi $smsApi
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        CustomerRepositoryInterface $customerRepositoryInterface,
        SmsApi $smsApi
    )
    {
        $this->orderRepository = $orderRepository;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->smsApi = $smsApi;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();

        try {
            $mobile = $this->_customerRepositoryInterface->getById($order->getCustomerId())->getCustomAttribute('mobile_no')->getValue();
            //$mobile = $order->getShippingAddress()->getTelephone();
            if ($mobile) {
                $orderId = $order->getIncrementId();
                $shipmentId = $shipment->getIncrementId();
                $trakingId = '';
                $link = '';
                $message = $this->smsApi->getOrderShipmentMessage($orderId, $shipmentId, $trakingId, $link);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'order_shipment');
            }
        } catch (\Exception $e) {
        }

    }
}