<?php

namespace Ktpl\SocialLogin\Observer\Order;

use Ktpl\SocialLogin\Helper\SmsApi;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Api\OrderRepositoryInterface;

class SaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var SmsApi
     */
    protected $smsApi;

    /**
     * @var RequestInterface
     */
    protected $requestInterface;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;


    /**
     * SaveAfter constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param SmsApi $smsApi
     * @param RequestInterface $requestInterface
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        SmsApi $smsApi,
        CustomerRepositoryInterface $customerRepositoryInterface,
        RequestInterface $requestInterface
    )
    {
        $this->orderRepository = $orderRepository;
        $this->smsApi = $smsApi;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->requestInterface = $requestInterface;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $orderId = $order->getIncrementId();
        //$mobile = $order->getShippingAddress()->getTelephone();
        $mobile = $this->_customerRepositoryInterface->getById($order->getCustomerId())->getCustomAttribute('mobile_no')->getValue();
        if ($order->getId() && $order->getStatus() && !empty($mobile)) {
            if ($order->getStatus() == "processing") {
                $message = $this->smsApi->getProcessingOrderMessage($orderId);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'order_processing');
            }
            if ($order->getStatus() == "complete") {
                $message = $this->smsApi->getCompletedOrderMessage($orderId);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'order_complete');
            }
            if ($order->getStatus() == "canceled") {
                $message = $this->smsApi->getCanceledOrderMessage($orderId);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'order_canceled');
            }
            if ($order->getStatus() == "closed") {
                $message = $this->smsApi->getClosedOrderMessage($orderId);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'order_closed');
            }
            if ($order->getStatus() == "holded") {
                $message = $this->smsApi->getOnHoldOrderMessage($orderId);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'order_holded');
            }
            if ($order->getStatus() == "pending") {
                $message = $this->smsApi->getPendingOrderMessage($orderId);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message,'order_pending');
            }
        }
    }
}