<?php

namespace Ktpl\SocialLogin\Observer\Order;

use Ktpl\SocialLogin\Helper\SmsApi;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Api\OrderRepositoryInterface;

class OrderInvoice implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var SmsApi
     */
    protected $smsApi;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * OrderInvoice constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param SmsApi $smsApi
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        CustomerRepositoryInterface $customerRepositoryInterface,
        SmsApi $smsApi
    )
    {
        $this->orderRepository = $orderRepository;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->smsApi = $smsApi;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();

        try {

            $mobile = $this->_customerRepositoryInterface->getById($order->getCustomerId())->getCustomAttribute('mobile_no')->getValue();
            //$mobile = $order->getShippingAddress()->getTelephone();
            if ($mobile) {
                $orderId = $order->getIncrementId();
                $invoiceId = $invoice->getIncrementId();
                $message = $this->smsApi->getOrderInvoiceMessage($orderId, $invoiceId);
                $this->smsApi->sendMessageUsingPrimaryService($mobile, $message, 'order_invoice');
            }
        } catch (\Exception $e) {
        }
    }
}
