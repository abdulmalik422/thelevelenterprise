<?php

namespace Ktpl\SocialLogin\Helper;

use Ktpl\OrderSuccess\Model\MobileOtpFactory;
use Ktpl\SocialLogin\Model\Config\Source\Values as ConfigValues;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class SmsApi
 * @package Ktpl\SocialLogin\Helper
 */
class SmsApi extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var CurlFactory
     */
    private $curlFactory;

    /**
     * @var Data
     */
    private $jsonHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    protected $_mobileOtp;

    /**
     * SmsApi constructor.
     * @param Context $context
     * @param CurlFactory $curlFactory
     * @param Data $jsonHelper
     * @param DateTime $dateTime
     * @param StoreManagerInterface $storeManager
     * @param MobileOtpFactory $mobileOtp
     */
    public function __construct(
        Context $context,
        CurlFactory $curlFactory,
        Data $jsonHelper,
        DateTime $dateTime,
        StoreManagerInterface $storeManager,
        MobileOtpFactory $mobileOtp
    ) {
        $this->curlFactory   = $curlFactory;
        $this->jsonHelper    = $jsonHelper;
        $this->dateTime      = $dateTime;
        $this->_storeManager = $storeManager;
        $this->_mobileOtp    = $mobileOtp;
        parent::__construct($context);
    }

    /**
     * Get Store name
     *
     * @return string
     */
    public function getStoreName()
    {
        return $this->_storeManager->getStore()->getName();
    }

    /**
     * @return mixed
     */
    public function getIsSmsEnable()
    {
        return $this->getConfig(ConfigValues::SMS_ENABLE_PATH);
    }

    /**
     * @param $path
     * @return mixed
     */
    public function getConfig($path)
    {
        return $this->scopeConfig
            ->getValue(
                $path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }

    /**
     * @return mixed
     */
    public function getCodSmsEnable()
    {
        return $this->getConfig(ConfigValues::SMS_COD_ENABLE_PATH);
    }

    /**
     * @return mixed
     */
    public function getCodOrderAbove()
    {
        return $this->getConfig(ConfigValues::SMS_COD_ORDER_ABOVE);
    }

    /**
     * @param $message
     */
    public function sendMessageToAdmin($message)
    {
        $mobileNumber = $this->getAdminMobile();
        if ($mobileNumber) {
            $this->sendMessageUsingPrimaryService($mobileNumber, $message,'admin_notification');
        }
    }

    /**
     * @return mixed
     */
    public function getAdminMobile()
    {
        return $this->getConfig(ConfigValues::SMS_ADMIN_MOBILE_NUMBER);
    }

    /**
     * @return mixed
     */
    private function getPrimaryUsername()
    {
        return $this->getConfig(ConfigValues::PRIMARY_SMS_USERNAME_PATH);
    }

    /**
     * @return mixed
     */
    private function getPrimaryPassword()
    {
        return $this->getConfig(ConfigValues::PRIMARY_SMS_PASSWORD_PATH);
    }

    /**
     * @return mixed
     */
    private function getPrimarySenderId()
    {
        return $this->getConfig(ConfigValues::PRIMARY_SMS_SENDER_PATH);
    }

    /**
     * @return string
     */
    private function getPrimaryServiceGatewayUrl()
    {
        return $this->getConfig(ConfigValues::PRIMARY_SMS_GATEWAY_URL_PATH) . "?";
    }

    /**
     * Using mobili.ws as primary SMS service
     *
     * @param $mobileNumber
     * @param $message
     * @return bool
     */
    public function sendMessageUsingPrimaryService($mobileNumber, $message, $formtype)
    {
        $params = [
            'mobile'          => $this->getPrimaryUsername(),
            'password'        => $this->getPrimaryPassword(),
            'numbers'         => $mobileNumber,
            'sender'          => $this->getPrimarySenderId(),
            'msg'             => $this->convertToUnicode($message),
            'applicationType' => ConfigValues::PRIMARY_SMS_APPLICATIONTPE,
        ];

        $url      = $this->getPrimaryServiceGatewayUrl() . http_build_query($params);
        $response = $this->get($url);

        /**
         * log SMS sent
         */
        $logMessage = __('SMS sent to %1 and message was %2. Server responce: %3', $mobileNumber, $message, $response);
        $this->saveLog($logMessage, 'smslog_' . date('d_m_Y') . '.log');
        /**
         * $response is restricted with fix responses
         * 1 = SMS sent successfully
         * 15 = invalid mobile number
         * anything else = error from SMS service
         */
        if ($response === 1) {
            $result = __('We have sent you message on %1,', $mobileNumber);
        } elseif ($response === 15) {
            $result = __('Invalid mobile number %1,', $mobileNumber);
        } else {
            $result = $this->sendMessageUsingSecondaryService($mobileNumber, $message, $formtype);
        }

        return $result;
    }

    /**
     * Logging Sms request and response messages
     */
    private function saveLog($message, $filename = 'smslog.log')
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $filename);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

    /**
     * @param $message
     * @return string
     */
    public function convertToUnicode($message)
    {
        $chrArray[0]       = "،";
        $unicodeArray[0]   = "060C";
        $chrArray[1]       = "؛";
        $unicodeArray[1]   = "061B";
        $chrArray[2]       = "؟";
        $unicodeArray[2]   = "061F";
        $chrArray[3]       = "ء";
        $unicodeArray[3]   = "0621";
        $chrArray[4]       = "آ";
        $unicodeArray[4]   = "0622";
        $chrArray[5]       = "أ";
        $unicodeArray[5]   = "0623";
        $chrArray[6]       = "ؤ";
        $unicodeArray[6]   = "0624";
        $chrArray[7]       = "إ";
        $unicodeArray[7]   = "0625";
        $chrArray[8]       = "ئ";
        $unicodeArray[8]   = "0626";
        $chrArray[9]       = "ا";
        $unicodeArray[9]   = "0627";
        $chrArray[10]      = "ب";
        $unicodeArray[10]  = "0628";
        $chrArray[11]      = "ة";
        $unicodeArray[11]  = "0629";
        $chrArray[12]      = "ت";
        $unicodeArray[12]  = "062A";
        $chrArray[13]      = "ث";
        $unicodeArray[13]  = "062B";
        $chrArray[14]      = "ج";
        $unicodeArray[14]  = "062C";
        $chrArray[15]      = "ح";
        $unicodeArray[15]  = "062D";
        $chrArray[16]      = "خ";
        $unicodeArray[16]  = "062E";
        $chrArray[17]      = "د";
        $unicodeArray[17]  = "062F";
        $chrArray[18]      = "ذ";
        $unicodeArray[18]  = "0630";
        $chrArray[19]      = "ر";
        $unicodeArray[19]  = "0631";
        $chrArray[20]      = "ز";
        $unicodeArray[20]  = "0632";
        $chrArray[21]      = "س";
        $unicodeArray[21]  = "0633";
        $chrArray[22]      = "ش";
        $unicodeArray[22]  = "0634";
        $chrArray[23]      = "ص";
        $unicodeArray[23]  = "0635";
        $chrArray[24]      = "ض";
        $unicodeArray[24]  = "0636";
        $chrArray[25]      = "ط";
        $unicodeArray[25]  = "0637";
        $chrArray[26]      = "ظ";
        $unicodeArray[26]  = "0638";
        $chrArray[27]      = "ع";
        $unicodeArray[27]  = "0639";
        $chrArray[28]      = "غ";
        $unicodeArray[28]  = "063A";
        $chrArray[29]      = "ف";
        $unicodeArray[29]  = "0641";
        $chrArray[30]      = "ق";
        $unicodeArray[30]  = "0642";
        $chrArray[31]      = "ك";
        $unicodeArray[31]  = "0643";
        $chrArray[32]      = "ل";
        $unicodeArray[32]  = "0644";
        $chrArray[33]      = "م";
        $unicodeArray[33]  = "0645";
        $chrArray[34]      = "ن";
        $unicodeArray[34]  = "0646";
        $chrArray[35]      = "ه";
        $unicodeArray[35]  = "0647";
        $chrArray[36]      = "و";
        $unicodeArray[36]  = "0648";
        $chrArray[37]      = "ى";
        $unicodeArray[37]  = "0649";
        $chrArray[38]      = "ي";
        $unicodeArray[38]  = "064A";
        $chrArray[39]      = "ـ";
        $unicodeArray[39]  = "0640";
        $chrArray[40]      = "ً";
        $unicodeArray[40]  = "064B";
        $chrArray[41]      = "ٌ";
        $unicodeArray[41]  = "064C";
        $chrArray[42]      = "ٍ";
        $unicodeArray[42]  = "064D";
        $chrArray[43]      = "َ";
        $unicodeArray[43]  = "064E";
        $chrArray[44]      = "ُ";
        $unicodeArray[44]  = "064F";
        $chrArray[45]      = "ِ";
        $unicodeArray[45]  = "0650";
        $chrArray[46]      = "ّ";
        $unicodeArray[46]  = "0651";
        $chrArray[47]      = "ْ";
        $unicodeArray[47]  = "0652";
        $chrArray[48]      = "!";
        $unicodeArray[48]  = "0021";
        $chrArray[49]      = '"';
        $unicodeArray[49]  = "0022";
        $chrArray[50]      = "#";
        $unicodeArray[50]  = "0023";
        $chrArray[51]      = "$";
        $unicodeArray[51]  = "0024";
        $chrArray[52]      = "%";
        $unicodeArray[52]  = "0025";
        $chrArray[53]      = "&";
        $unicodeArray[53]  = "0026";
        $chrArray[54]      = "'";
        $unicodeArray[54]  = "0027";
        $chrArray[55]      = "(";
        $unicodeArray[55]  = "0028";
        $chrArray[56]      = ")";
        $unicodeArray[56]  = "0029";
        $chrArray[57]      = "*";
        $unicodeArray[57]  = "002A";
        $chrArray[58]      = "+";
        $unicodeArray[58]  = "002B";
        $chrArray[59]      = ",";
        $unicodeArray[59]  = "002C";
        $chrArray[60]      = "-";
        $unicodeArray[60]  = "002D";
        $chrArray[61]      = ".";
        $unicodeArray[61]  = "002E";
        $chrArray[62]      = "/";
        $unicodeArray[62]  = "002F";
        $chrArray[63]      = "0";
        $unicodeArray[63]  = "0030";
        $chrArray[64]      = "1";
        $unicodeArray[64]  = "0031";
        $chrArray[65]      = "2";
        $unicodeArray[65]  = "0032";
        $chrArray[66]      = "3";
        $unicodeArray[66]  = "0033";
        $chrArray[67]      = "4";
        $unicodeArray[67]  = "0034";
        $chrArray[68]      = "5";
        $unicodeArray[68]  = "0035";
        $chrArray[69]      = "6";
        $unicodeArray[69]  = "0036";
        $chrArray[70]      = "7";
        $unicodeArray[70]  = "0037";
        $chrArray[71]      = "8";
        $unicodeArray[71]  = "0038";
        $chrArray[72]      = "9";
        $unicodeArray[72]  = "0039";
        $chrArray[73]      = ":";
        $unicodeArray[73]  = "003A";
        $chrArray[74]      = ";";
        $unicodeArray[74]  = "003B";
        $chrArray[75]      = "<";
        $unicodeArray[75]  = "003C";
        $chrArray[76]      = "=";
        $unicodeArray[76]  = "003D";
        $chrArray[77]      = ">";
        $unicodeArray[77]  = "003E";
        $chrArray[78]      = "?";
        $unicodeArray[78]  = "003F";
        $chrArray[79]      = "@";
        $unicodeArray[79]  = "0040";
        $chrArray[80]      = "A";
        $unicodeArray[80]  = "0041";
        $chrArray[81]      = "B";
        $unicodeArray[81]  = "0042";
        $chrArray[82]      = "C";
        $unicodeArray[82]  = "0043";
        $chrArray[83]      = "D";
        $unicodeArray[83]  = "0044";
        $chrArray[84]      = "E";
        $unicodeArray[84]  = "0045";
        $chrArray[85]      = "F";
        $unicodeArray[85]  = "0046";
        $chrArray[86]      = "G";
        $unicodeArray[86]  = "0047";
        $chrArray[87]      = "H";
        $unicodeArray[87]  = "0048";
        $chrArray[88]      = "I";
        $unicodeArray[88]  = "0049";
        $chrArray[89]      = "J";
        $unicodeArray[89]  = "004A";
        $chrArray[90]      = "K";
        $unicodeArray[90]  = "004B";
        $chrArray[91]      = "L";
        $unicodeArray[91]  = "004C";
        $chrArray[92]      = "M";
        $unicodeArray[92]  = "004D";
        $chrArray[93]      = "N";
        $unicodeArray[93]  = "004E";
        $chrArray[94]      = "O";
        $unicodeArray[94]  = "004F";
        $chrArray[95]      = "P";
        $unicodeArray[95]  = "0050";
        $chrArray[96]      = "Q";
        $unicodeArray[96]  = "0051";
        $chrArray[97]      = "R";
        $unicodeArray[97]  = "0052";
        $chrArray[98]      = "S";
        $unicodeArray[98]  = "0053";
        $chrArray[99]      = "T";
        $unicodeArray[99]  = "0054";
        $chrArray[100]     = "U";
        $unicodeArray[100] = "0055";
        $chrArray[101]     = "V";
        $unicodeArray[101] = "0056";
        $chrArray[102]     = "W";
        $unicodeArray[102] = "0057";
        $chrArray[103]     = "X";
        $unicodeArray[103] = "0058";
        $chrArray[104]     = "Y";
        $unicodeArray[104] = "0059";
        $chrArray[105]     = "Z";
        $unicodeArray[105] = "005A";
        $chrArray[106]     = "[";
        $unicodeArray[106] = "005B";
        $char              = "\ ";
        $chrArray[107]     = trim($char);
        $unicodeArray[107] = "005C";
        $chrArray[108]     = "]";
        $unicodeArray[108] = "005D";
        $chrArray[109]     = "^";
        $unicodeArray[109] = "005E";
        $chrArray[110]     = "_";
        $unicodeArray[110] = "005F";
        $chrArray[111]     = "`";
        $unicodeArray[111] = "0060";
        $chrArray[112]     = "a";
        $unicodeArray[112] = "0061";
        $chrArray[113]     = "b";
        $unicodeArray[113] = "0062";
        $chrArray[114]     = "c";
        $unicodeArray[114] = "0063";
        $chrArray[115]     = "d";
        $unicodeArray[115] = "0064";
        $chrArray[116]     = "e";
        $unicodeArray[116] = "0065";
        $chrArray[117]     = "f";
        $unicodeArray[117] = "0066";
        $chrArray[118]     = "g";
        $unicodeArray[118] = "0067";
        $chrArray[119]     = "h";
        $unicodeArray[119] = "0068";
        $chrArray[120]     = "i";
        $unicodeArray[120] = "0069";
        $chrArray[121]     = "j";
        $unicodeArray[121] = "006A";
        $chrArray[122]     = "k";
        $unicodeArray[122] = "006B";
        $chrArray[123]     = "l";
        $unicodeArray[123] = "006C";
        $chrArray[124]     = "m";
        $unicodeArray[124] = "006D";
        $chrArray[125]     = "n";
        $unicodeArray[125] = "006E";
        $chrArray[126]     = "o";
        $unicodeArray[126] = "006F";
        $chrArray[127]     = "p";
        $unicodeArray[127] = "0070";
        $chrArray[128]     = "q";
        $unicodeArray[128] = "0071";
        $chrArray[129]     = "r";
        $unicodeArray[129] = "0072";
        $chrArray[130]     = "s";
        $unicodeArray[130] = "0073";
        $chrArray[131]     = "t";
        $unicodeArray[131] = "0074";
        $chrArray[132]     = "u";
        $unicodeArray[132] = "0075";
        $chrArray[133]     = "v";
        $unicodeArray[133] = "0076";
        $chrArray[134]     = "w";
        $unicodeArray[134] = "0077";
        $chrArray[135]     = "x";
        $unicodeArray[135] = "0078";
        $chrArray[136]     = "y";
        $unicodeArray[136] = "0079";
        $chrArray[137]     = "z";
        $unicodeArray[137] = "007A";
        $chrArray[138]     = "{";
        $unicodeArray[138] = "007B";
        $chrArray[139]     = "|";
        $unicodeArray[139] = "007C";
        $chrArray[140]     = "}";
        $unicodeArray[140] = "007D";
        $chrArray[141]     = "~";
        $unicodeArray[141] = "007E";
        $chrArray[142]     = "©";
        $unicodeArray[142] = "00A9";
        $chrArray[143]     = "®";
        $unicodeArray[143] = "00AE";
        $chrArray[144]     = "÷";
        $unicodeArray[144] = "00F7";
        $chrArray[145]     = "×";
        $unicodeArray[145] = "00F7";
        $chrArray[146]     = "§";
        $unicodeArray[146] = "00A7";
        $chrArray[147]     = " ";
        $unicodeArray[147] = "0020";
        $chrArray[148]     = "\n";
        $unicodeArray[148] = "000D";
        $chrArray[149]     = "\r";
        $unicodeArray[149] = "000A";
        $strResult         = "";
        for ($i = 0; $i < strlen($message); $i++) {
            if (in_array(substr($message, $i, 1), $chrArray)) {
                $strResult .= $unicodeArray[array_search(substr($message, $i, 1), $chrArray)];
            }
        }
        return $strResult;
    }

    /**
     * @return mixed
     */
    private function getSecondaryUsername()
    {
        return $this->getConfig(ConfigValues::SECONDARY_SMS_USERNAME_PATH);
    }

    /**
     * @return mixed
     */
    private function getSecondaryPassword()
    {
        return $this->getConfig(ConfigValues::SECONDARY_SMS_PASSWORD_PATH);
    }

    /**
     * @return mixed
     */
    private function getSecondarySenderId()
    {
        return $this->getConfig(ConfigValues::SECONDARY_SMS_SENDER_PATH);
    }

    /**
     * @return string
     */
    private function getSecondaryServiceSendOtpGatewayUrl()
    {
        return $this->getConfig(ConfigValues::SECONDARY_SMS_GATEWAY_URL_OTP_SEND_PATH) . "?";
    }

    /**
     * @return string
     */
    private function getSecondaryServiceVerifyOtpGatewayUrl()
    {
        return $this->getConfig(ConfigValues::SECONDARY_SMS_GATEWAY_URL_OTP_VERIFY_PATH) . "?";
    }

    /**
     * @return string
     */
    private function getSecondaryServiceTransectionalGatewayUrl()
    {
        return $this->getConfig(ConfigValues::SECONDARY_SMS_GATEWAY_URL_TRANSECTIONAL_PATH) . "?";
    }

    /**
     * Using RouteMobile as secondary SMS service
     *
     * NOTE: We are sending all sms as transectional message(TM)
     * if required to send OTP and TM saparetly kindly uncomment code and remove direct function call.
     *
     * @param $mobileNumber
     * @param $message
     * @return bool
     */
    private function sendMessageUsingSecondaryService($mobileNumber, $message, $formtype = null)
    {
        $this->sendTransactionalMessage($mobileNumber, $message, $this->getSecondaryServiceTransectionalGatewayUrl());

        /*$otpSms = ['login','forgotpassword','account_edit','checkout'];

    if (in_array($formtype,$otpSms)) {
    $this->sendOtpMessage($mobileNumber, $message, $this->getSecondaryServiceSendOtpGatewayUrl());
    } else {
    $this->sendTransactionalMessage($mobileNumber, $message, $this->getSecondaryServiceTransectionalGatewayUrl());
    }*/
    }

    /**
     * @param $mobileNumber
     * @param $otp
     * @return mixed
     */
    public function getOtpVerifiedUsingSecondaryService($mobileNumber, $otp)
    {
        $params = [
            'username' => $this->getSecondaryUsername(),
            'password' => $this->getSecondaryPassword(),
            'msisdn'   => $mobileNumber,
            'otp'      => $otp,
        ];

        $url      = $this->urlEncoder($this->getSecondaryServiceVerifyOtpGatewayUrl() . http_build_query($params));
        $response = $this->get($url);
        $result   = $this->jsonDecode($response);

        if (in_array('1701', explode('|', $response))) {
            $response['success'] = true;
            $response['message'] = __('We have verified your OTP.');
        } else {
            $response['success'] = false;
            $response['message'] = __('Wrong OTP. Please enter correct OTP.');
        }

        return $response;
    }

    /**
     * @param $mobileNumber
     * @param $message
     * @return bool
     */
    public function sendTransactionalMessage($mobileNumber, $message, $gatewayUrl)
    {
        return $this->sendMessage($mobileNumber, $message, $gatewayUrl);
    }

    /**
     * @param $mobileNumber
     * @param $message
     * @return bool
     */
    public function sendOtpMessage($mobileNumber, $message, $gatewayUrl)
    {
        $message = '%m' . __(' is your OTP for %1', $this->getStoreName());
        return $this->sendMessage($mobileNumber, $message, $gatewayUrl);
    }

    /**
     * @param $mobileNumber
     * @param $message
     * @param $apiKey
     * @param $senderId
     * @return bool
     */
    private function sendMessage($mobileNumber, $message, $gatewayUrl)
    {
        $params = [
            'username' => $this->getSecondaryUsername(),
            'password' => $this->getSecondaryPassword(),
            'msisdn'   => $mobileNumber,
            'source'   => $this->getSecondarySenderId(),
            'msg'      => urlencode($message),
            'otplen'   => 4,
            'exptime'  => 300,
        ];

        $url      = $gatewayUrl . http_build_query($params);
        $response = $this->get($url);

        $logMessage = __('SMS sent from secondary service to %1 and message was %2. Server responce: %3', $mobileNumber, $message, $response);
        $this->saveLog($logMessage, 'smslog_' . date('d_m_Y') . '.log');

        if (in_array('1701', explode('|', $response))) {
            $result = __('We have sent you message on %1,', $mobileNumber);
        } else {
            $result = __('Something went wrong, please try again.');
        }
    }

    /**
     * @param $url
     * @param null $params
     * @return mixed
     */
    public function get($url, $params = null)
    {
        $curlGet = $this->curlFactory->create();
        $curlGet->write(\Zend_Http_Client::GET, $url, '1.1', ['Connection: close']);
        $response = \Zend_Http_Response::extractBody($curlGet->read());
        return $response;
    }

    /**
     * @param $response
     * @return mixed
     */
    private function jsonDecode($response)
    {
        return $this->jsonHelper->jsonDecode($response);
    }

    /**
     * @return string
     */
    public function getCurrentDate()
    {
        return $this->dateTime->date();
    }

    /**
     * @param $updatedDate
     * @return bool
     */
    public function checkOtpSent($updatedDate)
    {
        $currentTimeStamp = $this->dateTime->timestamp();
        $updatedTimeStamp = $this->dateTime->timestamp($updatedDate);
        $timeBetween      = $currentTimeStamp - $updatedTimeStamp;
        if ($timeBetween < ConfigValues::TIME_BETWEEN_OTP) {
            return true;
        }
    }

    /**
     * @param $updatedDate
     * @return bool
     */
    public function isOtpExpired($updatedDate)
    {
        $currentTimeStamp = $this->dateTime->timestamp();
        $updatedTimeStamp = $this->dateTime->timestamp($updatedDate);
        $timeBetween      = $currentTimeStamp - $updatedTimeStamp;
        if ($timeBetween > ConfigValues::OTP_EXPIRE_TIME) {
            return true;
        }
    }

    /**
     * @param $otp
     * @return mixed|string
     */
    public function getLoginMessage($otp)
    {
        $message = $this->getConfig(ConfigValues::SMS_LOGIN_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{otp}}", $otp, $message);
        } else {
            $message = "{$otp}" . __('is the OTP to login to your account.');
        }

        return $message;
    }

    /**
     * @param $otp
     * @return mixed|string
     */
    public function getRegistrationMessage($otp)
    {
        $message = $this->getConfig(ConfigValues::SMS_REGISTRATION_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{otp}}", $otp, $message);
        } else {
            $message = "{$otp} is the OTP to register to your " . $this->getStoreName() . " account.";
        }

        return $message;
    }

    /**
     * @param $otp
     * @return mixed|string
     */
    public function getRegistrationOtpMessage($otp)
    {
        $message = $this->getConfig(ConfigValues::SMS_REGISTRATION_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{otp}}", $otp, $message);
        } else {
            $message = "{$otp} is the OTP to to reset your password.";
        }

        return $message;
    }

    /**
     * @param $resetPasswordLink
     * @return mixed|string
     */
    public function getResetPasswordMessage($resetPasswordLink)
    {
        $resetPasswordLink = $this->createShortLink($resetPasswordLink);
        $message           = $this->getConfig(ConfigValues::SMS_RESET_PASSWORD_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{link}}", $resetPasswordLink, $message);
        } else {
            $message = "Looks like you forgot your password! Click here {$resetPasswordLink} to reset your password.";
        }

        return $message;
    }

    /**
     * @param $link
     * @return bool
     */
    public function createShortLink($link)
    {
        $apiKey = $this->getTransactionalApiKey();

        $params = array(
            'api_key' => $apiKey,
            'method'  => ConfigValues::SMS_SHORTEN_LINK_METHOD_TYPE,
            'url'     => $link,
        );

        $url      = $this->getGatewayUrl() . http_build_query($params);
        $response = $this->get($url);
        $result   = $this->jsonDecode($response);

        if ($result["status"] == ConfigValues::OTP_SUCCESS_STATUS) {
            return $result['txtly'];
        } else {
            //TODO::Print error status and message
            //$result['status'].$result['message'];
            return false;
        }
    }

    /**
     * @param $orderId
     * @return mixed|string
     */
    public function getOrderSuccessMessage($orderId, $orderTotal)
    {
        $orderId    = $orderId;
        $orderTotal = $orderTotal;
        $message    = $this->getConfig(ConfigValues::SMS_ORDER_SUCCESS_TEMPLATE_PATH);
        if ($message) {
            $orderVarChangeFrom = ["{{order}}", "{{total}}"];
            $orderVarChangeTo   = [$orderId, $orderTotal];
            $message            = str_replace($orderVarChangeFrom, $orderVarChangeTo, $message);
        } else {
            $message = "Your order no. {$orderId} has been placed successfully. We will confirm your order soon. Thank you for choosing " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $orderId
     * @param $invoiceId
     * @return mixed|string
     */
    public function getOrderInvoiceMessage($orderId, $invoiceId)
    {
        $message = $this->getConfig(ConfigValues::SMS_ORDER_INVOICE_TEMPLATE_PATH);
        if ($message) {
            $orderVarChangeFrom = ["{{order_id}}", "{{invoice_id}}"];
            $orderVarChangeTo   = [$orderId, $invoiceId];
            $message            = str_replace($orderVarChangeFrom, $orderVarChangeTo, $message);
        } else {
            $message = "The invoice with no." . $invoiceId . " succesfully generated for your order no." . $orderId . " on " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $orderId
     * @param $shipmentId
     * @return mixed|string
     */
    public function getOrderShipmentMessage($orderId, $shipmentId, $trakingId, $link)
    {
        $message = $this->getConfig(ConfigValues::SMS_ORDER_SHIPMENT_TEMPLATE_PATH);
        if ($message) {
            $orderVarChangeFrom = ["{{order}}", "{{shipment}}", "{{traking}}", "{{link}}"];
            $orderVarChangeTo   = [$orderId, $shipmentId, $trakingId, $link];
            $message            = str_replace($orderVarChangeFrom, $orderVarChangeTo, $message);
        } else {
            $message = "The shipment with no.{$shipmentId} succesfully generated for your order no.{$orderId} on " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $mobile
     * @return mixed|string
     */
    public function getCustomerRegistrationMessage($mobile)
    {
        $message = $this->getConfig(ConfigValues::SMS_CUSTOMER_REGISTRATION);
        if ($message) {
            $message = str_replace("{{mobile}}", $mobile, $message);
        } else {
            $message = "Your mobile number " . $mobile . " is successfully register on " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $link
     * @return mixed|string
     */
    public function getRegistrationOnOrderMessage($link)
    {
        $message = $this->getConfig(ConfigValues::SMS_CUSTOMER_REGISTRATION_ORDER);
        if ($message) {
            $message = str_replace("{{link}}", $link, $message);
        } else {
            $message = "Click here " . $link . " to sign in to " . $this->getStoreName() . " and set a password";
        }

        return $message;
    }

    /**
     * @param $otp
     * @return mixed|string
     */
    public function getCodOrderOtpMessage($otp)
    {
        $message = $this->getConfig(ConfigValues::SMS_COD_ORDER_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{otp}}", $otp, $message);
        } else {
            $message = "Dear Customer, The OTP to confirm your COD order on " . $this->getStoreName() . " is {$otp}. Thanks for choosing " . $this->getStoreName();
        }

        return $message;
    }

    public function getOrderOtpMessage($otp)
    {
        $message = $this->getConfig(ConfigValues::SMS_COD_ORDER_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{otp}}", $otp, $message);
        } else {
            $message = "Dear Customer, The OTP to confirm your order on " . $this->getStoreName() . " is {$otp}. Thanks for choosing " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $orderId
     * @return mixed|string
     */
    public function getProcessingOrderMessage($orderId)
    {
        $message = $this->getConfig(ConfigValues::SMS_ORDER_INVOICE_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{order_id}}", $orderId, $message);
        } else {
            $message = "Your order no. " . $orderId . " status has been updated to processing on " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $orderId
     * @return mixed|string
     */
    public function getCompletedOrderMessage($orderId)
    {
        $message = $this->getConfig(ConfigValues::SMS_ORDER_INVOICE_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{order_id}}", $orderId, $message);
        } else {
            $message = "Your order no. " . $orderId . " status has been updated to completed on " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $orderId
     * @return mixed|string
     */
    public function getCanceledOrderMessage($orderId)
    {
        $message = $this->getConfig(ConfigValues::SMS_ORDER_INVOICE_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{order_id}}", $orderId, $message);
        } else {
            $message = "Your order no. " . $orderId . " status has been updated to canceled on " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $orderId
     * @return mixed|string
     */
    public function getClosedOrderMessage($orderId)
    {
        $message = $this->getConfig(ConfigValues::SMS_ORDER_INVOICE_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{order_id}}", $orderId, $message);
        } else {
            $message = "Your order no. " . $orderId . " status has been updated to closed on " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $orderId
     * @return mixed|string
     */
    public function getOnHoldOrderMessage($orderId)
    {
        $message = $this->getConfig(ConfigValues::SMS_ORDER_INVOICE_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{order_id}}", $orderId, $message);
        } else {
            $message = "Your order no. " . $orderId . " status has been updated to on hold on " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $orderId
     * @return mixed|string
     */
    public function getPendingOrderMessage($orderId)
    {
        $message = $this->getConfig(ConfigValues::SMS_ORDER_INVOICE_TEMPLATE_PATH);
        if ($message) {
            $message = str_replace("{{order_id}}", $orderId, $message);
        } else {
            $message = "Your order no. " . $orderId . " status has been updated to pending on  " . $this->getStoreName();
        }

        return $message;
    }

    /**
     * @param $message
     * @return mixed
     */
    private function cleanMessage($message)
    {
        $format  = str_replace(' ', '+', $message);
        $message = preg_replace('/[^A-Za-z0-9\-\(\)\+]/', '', $format);
        return $message;
    }

    /**
     * @return mixed
     */
    private function getPrimaryApiKey()
    {
        return $this->getConfig(ConfigValues::PRIMARY_SMS_APIKEY_PATH);
    }

    public function saveMobileOtp($data = array())
    {
        $modelFactory = $this->_mobileOtp->create();
        $modelFactory->load($data['mobile'], 'mobile');
        if ($modelFactory->getMobile()) {
            $modelFactory->setOtp($data['otp']);
            $modelFactory->setUpdatedAt(date('Y-m-d H:i:s'));
            $modelFactory->save();
        } else {
            $modelFactory->setMobile($data['mobile']);
            $modelFactory->setOtp($data['otp']);
            $modelFactory->save();
        }
    }

    public function getMobileOtp($data = array())
    {
        $response['success'] = false;
        $response['message'] = __('Wrong OTP. Please enter correct OTP.');
        $modelFactory        = $this->_mobileOtp->create();
        $modelFactory->load($data['mobile'], 'mobile');
        if ($modelFactory->getMobile() && $data['otp'] == $modelFactory->getOtp()) {
            $updatedTime = $modelFactory->getUpdatedAt();
            $otpExpired  = $this->isOtpExpired($updatedTime);
            if ($otpExpired) {
                $response['success'] = false;
                $response['message'] = __('OTP has been expired. Please resend the OTP.');
            } else {
                $response['success'] = true;
                $response['message'] = __('We have verified your OTP, you can login now.');
            }
        }
        return $response;
    }

}