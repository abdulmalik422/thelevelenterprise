<?php

namespace Ktpl\SocialLogin\Helper;

use Magento\Customer\Model\SessionFactory as CustomerSession;
use Magento\Framework\Session\SessionManagerInterface as CoreSession;
use Magento\Catalog\Block\Product\ReviewRendererInterface;

/**
 * Contact module base controller
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $priceHelper;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    protected $_coreSession;

    /**
     * @var \Magento\Catalog\Block\Product\ImageBuilder
     */
    protected $imageBuilder;

    /**
     * @var ReviewRendererInterface
     */
    protected $reviewRenderer;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     * @param CustomerSession $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        CustomerSession $customerSession,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        CoreSession $coreSession,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        ReviewRendererInterface $reviewRenderer
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $context->getScopeConfig();
        $this->_objectManager = $objectmanager;
        $this->_customerSession = $customerSession;
        $this->priceHelper = $priceHelper;
        $this->customerRepository = $customerRepository;
        $this->_coreSession = $coreSession;
        $this->imageBuilder = $imageBuilder;
        $this->reviewRenderer = $reviewRenderer;
        parent::__construct($context);
    }

    /**
     * @return string Store Email Address
     */
    public function getStoreGeneralEmail()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_general/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Check customer is logged in or not
     *
     * @return bool
     */
    public function checkIsLoggedIn()
    {
        $context = $this->_objectManager->get('Magento\Framework\App\Http\Context');
        $isLoggedIn = $context->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);

        if($isLoggedIn)
        {
            return true;
        }
    }

    /**
     * Get customer name
     *
     * @return string
     */
    public function getCustomerName()
    {
        $customerName = $this->_customerSession->create()->getCustomer()->getName();
        return $customerName;
    }

    public function getCurrentCustomer()
    {
        return $this->_customerSession->create();
    }

    public function getCustomerMobile($customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        $mobileAttribute = $customer->getCustomAttribute('mobile');
        if($mobileAttribute) {
            return $mobileAttribute->getValue();
        }

        return false;
    }

    /**
     * Format price with currency symbol
     *
     * @param $price
     * @return mixed
     */
    public function formatPrice($price)
    {
        return $this->priceHelper->currency($price, true, false);
    }

    /**
     * Get store code
     *
     * @return mixed
     */
    public function getStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }

    public function getMediaUrl()
    {
        return $this->storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getNewCustomerMobile()
    {
        return $this->_coreSession->getNewCustomerMobile();
    }

    /**
     * Retrieve product image
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $imageId
     * @param array $attributes
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }

    /**
     * Get product reviews summary
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param bool $templateType
     * @param bool $displayIfNoReviews
     * @return string
     */
    public function getReviewsSummaryHtml(
        \Magento\Catalog\Model\Product $product,
        $templateType = false,
        $displayIfNoReviews = true
    ) {
        return $this->reviewRenderer->getReviewsSummaryHtml($product, $templateType, $displayIfNoReviews);
    }
}
