<?php

namespace Ktpl\SocialLogin\Helper;

use Ktpl\SocialLogin\Model\Config\Source\Values as ConfigValues;

class ApiRequest extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\HTTP\Adapter\CurlFactory
     */
    private $curlFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    /**
     * ApiRequest constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->curlFactory = $curlFactory;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    /**
     * @param $url
     * @param null $params
     * @return mixed
     */
    public function get($url, $params = null)
    {
        $curlGet = $this->curlFactory->create();
        $curlGet->write(\Zend_Http_Client::GET, $url, '1.1', ['Connection: close']);
        $response = \Zend_Http_Response::extractBody($curlGet->read());
        return $response;
    }

    public function jsonDecode($response)
    {
        return $this->jsonHelper->jsonDecode($response);
    }

    public function sendMessage($mobileNumber, $message, $secureMessage = true)
    {
        if($secureMessage) {
            $message = $this->cleanMessage($message);
        }

        $params = array(
            'api_key' => $this->getApiKey(),
            'method'  => ConfigValues::SMS_METHOD_TYPE,
            'message' => $message,
            'to'      => $mobileNumber,
            'sender'  => $this->getSenderId(),
        );

        $url = $this->getGatewayUrl().http_build_query($params);
        $response = $this->get($url);
        $result = $this->jsonDecode($response);

        if($result["status"] == ConfigValues::OTP_SUCCESS_STATUS) {
            return true;
        } else {
            //Print error status and message
            //$result['status'].$result['message'];
        }

        return false;
    }

    public function sendMessageToAdmin($message)
    {
        $mobileNumber = $this->getAdminMobile();
        if($mobileNumber) {
            $this->sendMessage($mobileNumber, $message);
        }
    }

    private function cleanMessage($message)
    {
        $format = str_replace(' ', '+', $message);
        $message = preg_replace('/[^A-Za-z0-9\-\(\)\+]/', '', $format);
        return $message;
    }

    public function getConfig($path)
    {
        return $this->scopeConfig
            ->getValue(
                $path,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }

    /**
     * @return string
     */
    private function getGatewayUrl()
    {
        return $this->getConfig(ConfigValues::SMS_GATEWAY_URL_PATH)."?";
    }

    /**
     * @return mixed
     */
    private function getSenderId()
    {
        return $this->getConfig(ConfigValues::SMS_SENDER_PATH);
    }

    /**
     * @return mixed
     */
    private function getApiKey()
    {
        return $this->getConfig(ConfigValues::SMS_APIKEY_PATH);
    }

    public function getAdminMobile()
    {
        return $this->getConfig(ConfigValues::SMS_ADMIN_MOBILE_NUMBER);
    }
}