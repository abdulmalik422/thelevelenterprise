<?php

namespace Ktpl\SocialLogin\Model\Config\Source;

class Values
{
    const CUSTOMER_NOT_FOUND = '404';

    const OTP_SUCCESS_STATUS = "OK";

    const SMS_METHOD_TYPE = "sms";

    const SMS_SHORTEN_LINK_METHOD_TYPE = "txtly.create";

    /**
     * OTP responses
     */
    const OTP_SENT_SUCCESS = 'otp_success';

    /**
     * Resend otp time in seconds
     */
    const TIME_BETWEEN_OTP = "60";

    const OTP_ALREADY_SENT = "otp_sent";

    const OTP_EXPIRED = "otp_expired";

    /**
     * Otp expired time in seconds
     */
    const OTP_EXPIRE_TIME = "300";

    const OTP_VERIFIED = "otp_verified";

    const OTP_NOT_VERIFIED = "otp_notverified";

    const CUSTOMER_ALREADY_REGISTER = "already_register";

    const NEW_CUSTOMER = "new_customer";

    /**
     * SMS general settings configuration path
     */

    /*const SMS_GATEWAY_URL_PATH = "sms_service/general/geteway_url";*/

    const SMS_ADMIN_MOBILE_NUMBER = "sms_service/general/admin_mobile";

    const SMS_ENABLE_PATH = "sms_service/general/enable";

    /**
     *  COD SMS COnfiguration path
     */

    /*const SMS_COD_ENABLE_PATH = "sms_service/cod_order/cod_enable";

    const SMS_COD_ORDER_ABOVE = "sms_service/cod_order/order_above";*/

    /**
     * Primary SMS settings configuration path
     */

    const PRIMARY_SMS_GATEWAY_URL_PATH = "sms_service/primary_messages/geteway_url";

    const PRIMARY_SMS_SENDER_PATH = "sms_service/primary_messages/sender";

    const PRIMARY_SMS_USERNAME_PATH = "sms_service/primary_messages/username";

    const PRIMARY_SMS_PASSWORD_PATH = "sms_service/primary_messages/password";

    const PRIMARY_SMS_APIKEY_PATH = "sms_service/primary_messages/apikey";

    const PRIMARY_SMS_APPLICATIONTPE = 68;

    /**
     * COD order OTP configuration path
     */

    const SMS_COD_ORDER_ENABLE_PATH = "sms_service/cod_order/order_above/cod_enable";

    const SMS_COD_ORDER_ABOVE_PATH = "sms_service/cod_order/order_above/order_above";

    const SMS_COD_ORDER_TEMPLATE_PATH = "sms_service/cod_order/order_above/cod_order_otp_message";


    /**
     * Secondary SMS settings configuration path
     */

    const SECONDARY_SMS_GATEWAY_URL_OTP_SEND_PATH = "sms_service/secondary_messages/geteway_url_sendotp";

    const SECONDARY_SMS_GATEWAY_URL_OTP_VERIFY_PATH = "sms_service/secondary_messages/geteway_url_verifyotp";

    const SECONDARY_SMS_GATEWAY_URL_TRANSECTIONAL_PATH = "sms_service/secondary_messages/geteway_url_transectional";

    const SECONDARY_SMS_SENDER_PATH = "sms_service/secondary_messages/sender";

    const SECONDARY_SMS_USERNAME_PATH = "sms_service/secondary_messages/username";

    const SECONDARY_SMS_PASSWORD_PATH = "sms_service/secondary_messages/password";

    /**
     * Customer Login, register and reset password message template config path
     */

    const SMS_LOGIN_TEMPLATE_PATH = "sms_service/general/templates/login_otp_message";

    const SMS_REGISTRATION_TEMPLATE_PATH = "sms_service/general/templates/registration_otp_message";

    const SMS_RESET_PASSWORD_TEMPLATE_PATH = "sms_service/general/templates/password_reset_otp_message";

    const SMS_CUSTOMER_REGISTRATION = "sms_service/general/templates/customer_registration_message";

    const SMS_CUSTOMER_REGISTRATION_ORDER = "sms_service/general/templates/customer_registration_order_message";

    /**
     * Order success, invoice and shipment message template config path
     */

    const SMS_ORDER_SUCCESS_TEMPLATE_PATH = "sms_service/general/order_templates/order_success_message";

    const SMS_ORDER_INVOICE_TEMPLATE_PATH = "sms_service/general/order_templates/order_invoice_message";

    const SMS_ORDER_SHIPMENT_TEMPLATE_PATH = "sms_service/general/order_templates/order_shipment_message";

    /**
     * Order status messages
     */
    const SMS_ORDER_PROCESSING_STATUS_TEMPLATE_PATH = "sms_service/general/order_status_templates/order_processing_status_message";

    const SMS_ORDER_COMPLETED_STATUS_TEMPLATE_PATH = "sms_service/general/order_status_templates/order_completed_status_message";

    const SMS_ORDER_CANCELED_STATUS_TEMPLATE_PATH = "sms_service/general/order_status_templates/order_canceled_status_message";

    const SMS_ORDER_CLOSED_STATUS_TEMPLATE_PATH = "sms_service/general/order_status_templates/order_closed_status_message";

    const SMS_ORDER_HOLD_STATUS_TEMPLATE_PATH = "sms_service/general/order_status_templates/order_hold_status_message";

    const SMS_ORDER_PENDING_STATUS_TEMPLATE_PATH = "sms_service/general/order_status_templates/order_pending_status_message";

}