<?php
namespace Ktpl\SocialLogin\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Ktpl\SocialLogin\Model\Config\Source\Values as ConfigValue;

class Verifyotpsuccess extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var \Ktpl\MobileLogin\Model\MobileLoginFactory
     */
    protected $mobileLoginFactory;

    /**
     * @var \Ktpl\Core\Helper\SmsApi
     */
    protected $smsApi;

    /**
     * @var \Magento\Checkout\Model\SessionFactory
     */
    private $_checkoutSession;

    /**
     * Verifyotp constructor.
     * @param PageFactory $pageFactory
     * @param \Ktpl\MobileLogin\Model\MobileLoginFactory $mobileLoginFactory
     * @param \Ktpl\Core\Helper\SmsApi $smsApi
     * @param Context $context
     */
    public function __construct(
        PageFactory $pageFactory,
        \Ktpl\OrderSuccess\Model\MobileOtpFactory $mobileLoginFactory,
        \Ktpl\SocialLogin\Helper\SmsApi $smsApi,
        \Magento\Checkout\Model\SessionFactory $checkoutSession,
        Context $context
    ) {
        $this->pageFactory = $pageFactory;
        $this->mobileLoginFactory = $mobileLoginFactory;
        $this->smsApi = $smsApi;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * return bool based on Verify OTP
     * @return bool
     */
    public function execute() {
        $mobileNo = $this->getRequest()->getParam('otpLogin');
        $otp = $this->getRequest()->getParam('OTP');
        $sendResponse['isVerified'] = "";
        if ($mobileNo && $otp) {
            $modelFactory = $this->mobileLoginFactory->create();
            $modelFactory->load($mobileNo, 'mobile');
            if($modelFactory->getMobile() && $modelFactory->getOtp()) {
                if($otp == $modelFactory->getOtp()) {
                    $updatedTime = $modelFactory->getUpdatedAt();
                    $otpExpired = $this->smsApi->isOtpExpired($updatedTime);
                    if($otpExpired) {
                        $sendResponse['isVerified'] = ConfigValue::OTP_EXPIRED;
                    } else {
                        $lastOrder = $this->_checkoutSession->create()->getLastRealOrder();
                        // $lastOrder->setStatus('otp_verified');
                     //   $lastOrder->setOtpVerified('1');
                        $lastOrder->setStatus('processing');
                        $lastOrder->addStatusHistoryComment('Mobile number has been verified.');
                        $lastOrder->save();
                        $sendResponse['isVerified'] = ConfigValue::OTP_VERIFIED;
                    }
                } else {
                    $sendResponse['isVerified'] = ConfigValue::OTP_NOT_VERIFIED;
                }
            } else {
                $sendResponse['isVerified'] = ConfigValue::OTP_NOT_VERIFIED;
            }
        } else {
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('customer/account/login');
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($sendResponse);
        return $resultJson;
    }
}
