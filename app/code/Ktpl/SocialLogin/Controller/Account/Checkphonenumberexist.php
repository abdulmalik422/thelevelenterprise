<?php

namespace Ktpl\SocialLogin\Controller\Account;

use Magento\Customer\Model\Customer;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Cart;

/**
 * Class Checkphonenumberexist
 * @package Ktpl\SocialLogin\Controller\Account
 */
class Checkphonenumberexist extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Customer
     */
    protected $_customerFactory;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * Checkphonenumberexist constructor.
     * @param Customer $customerModel
     * @param Context $context
     */
    public function __construct(
        Customer $customerModel,
        Cart $cart,
        Context $context
    )
    {
        $this->_customerFactory = $customerModel;
        $this->cart = $cart;
        parent::__construct($context);
    }

    /**
     *
     * @return Array
     */
    public function execute()
    {
        $sendResponse = '';
        $post = $this->getRequest()->getParams();

        $customerCollection = $this->_customerFactory->getCollection();

        if (array_key_exists('mobile_no', $post)) {
            $customerCollection->addFieldToFilter('mobile_no', $post['mobile_no'])->load();
        } else {
            $sendResponse['exist'] = false;
        }

        if (count($customerCollection) > 0) {
            $sendResponse['exist'] = true;
            $message = __('Customer with phone number: %1 is already exist.', $post['mobile_no']);
            $sendResponse['message'] = $message;
        } else {
            $sendResponse['exist'] = false;
            $message = __('There is no customer registered with %1 mobile number.', $post['mobile_no']);
            $sendResponse['message'] = $message;
        }

        if(array_key_exists('savefororder',$post)) {

        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($sendResponse);
        return $resultJson;
    }

    protected function getCurrentQuote($mobileNumber)
    {
        $currentQuote = $this->cart->setData($mobileNumber,'mobile_number');
        $this->cart->save();
    }

}