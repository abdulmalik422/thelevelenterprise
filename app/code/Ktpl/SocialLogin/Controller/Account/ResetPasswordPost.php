<?php

namespace Ktpl\SocialLogin\Controller\Account;

use Ktpl\SocialLogin\Cookie\Mobilecookie;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Math\Random;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class ResetPasswordPost
 * @package Ktpl\SocialLogin\Controller\Account
 */
class ResetPasswordPost extends AbstractAccount
{

    /**
     * @var AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var Session
     */
    protected $session;

    /**
     * ResetPasswordPost constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $accountManagement
     * @param CustomerRepositoryInterface $customerRepository
     * @param Customer $customerModel
     * @param Mobilecookie $customCookie
     * @param StoreManagerInterface $storeManager
     * @param Random $mathRandom
     * @param JsonFactory $resultJsonFactory
     * @param RawFactory $resultRawFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $accountManagement,
        CustomerRepositoryInterface $customerRepository,
        Customer $customerModel,
        Mobilecookie $customCookie,
        StoreManagerInterface $storeManager,
        Random $mathRandom,
        JsonFactory $resultJsonFactory,
        RawFactory $resultRawFactory,
        Data $helper
    )
    {
        $this->session = $customerSession;
        $this->accountManagement = $accountManagement;
        $this->customerRepository = $customerRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->helper = $helper;
        $this->_customerFactory = $customerModel;
        $this->_customCookie = $customCookie;
        $this->storeManager = $storeManager;
        $this->mathRandom = $mathRandom;
        parent::__construct($context);
    }

    /**
     * Reset forgotten password
     *
     * Used to handle data received from reset forgotten password form
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getparams();
        $resultRedirect = $this->resultRedirectFactory->create();
        $mobileNo = $this->getRequest()->getParam('mobile_no');
        $password = (string)$this->getRequest()->getPost('password');
        $passwordConfirmation = (string)$this->getRequest()->getPost('conf_password');

        if (iconv_strlen($password) <= 0) {
            $response = [
                'errors' => true,
                'message' => "Please enter a new password."
            ];
        }
        if ($password !== $passwordConfirmation) {
            $response = [
                'errors' => true,
                'message' => "New Password and Confirm New Password values didn't match."
            ];
        }

        $customerModel = $this->_customerFactory->getCollection();
        $customerModel->addFieldToFilter('mobile_no', $this->getRequest()->getParam('mobile_no'))->load();
        $customerId = '';
        $resetPasswordToken = '';

        if (count($customerModel) < 0) {
            $response = [
                'errors' => true,
                'message' => "Mobile number does not exist."
            ];
        }

        if (count($customerModel) > 0) {
            $resetPasswordToken = (string)$customerModel->getFirstItem()->getRpToken();
            $customerId = (int)$customerModel->getFirstItem()->getId();
        }

        try {
            $customer = $this->customerRepository->getById($customerId);
            $customerEmail = $customer->getEmail();
            $websiteId = null;
            if ($websiteId === null) {
                $websiteId = $this->storeManager->getStore()->getWebsiteId();
            }
            $customer = $this->customerRepository->get($customerEmail, $websiteId);
            $newPasswordToken = $this->mathRandom->getUniqueHash();
            $this->accountManagement->changeResetPasswordLinkToken($customer, $newPasswordToken);
            $this->accountManagement->resetPassword($customerEmail, $newPasswordToken, $password);

            $this->session->unsRpToken();
            $this->session->unsRpCustomerId();
            $response = [
                'errors' => false,
                'message' => __('You have updated your password')
            ];
        } catch (\Exception $exception) {
            $response = [
                'errors' => true,
                'message' => $exception->getMessage()
            ];
        }
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }

}
