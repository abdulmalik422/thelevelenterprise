<?php

namespace Ktpl\SocialLogin\Controller\Account;

use Ktpl\SocialLogin\Cookie\Mobilecookie;
use Ktpl\SocialLogin\Helper\SmsApi;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Verifyotp
 * @package Ktpl\SocialLogin\Controller\Account
 */
class Verifyotp extends Action {

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var SmsApi
     */
    protected $_smsApi;

    /**
     * Verifyotp constructor.
     * @param SmsApi $smsApi
     * @param Mobilecookie $customCookie
     * @param PageFactory $pageFactory
     * @param Context $context
     */
    public function __construct(
    SmsApi $smsApi, PageFactory $pageFactory, Context $context
    ) {
        $this->_smsApi = $smsApi;
        $this->pageFactory = $pageFactory;        
        parent::__construct($context);
    }

    /**
     *
     *
     * @return Array
     */
    public function execute() {
        $mobileNo = $this->getRequest()->getParam('mobile_no');
        $otp = $this->getRequest()->getParam('otp');
        $verified = [];
        if ($mobileNo && $otp) {
            $verified = $this->_smsApi->getMobileOtp(['mobile' => $mobileNo, 'otp' => $otp]);
        }

        /*if(!$verified['success']) {
            $verified = $this->_smsApi->getOtpVerifiedUsingSecondaryService($mobileNo,$otp);
        }*/

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($verified);
        return $resultJson;
    }

}
