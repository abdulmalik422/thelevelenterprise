<?php

namespace Ktpl\SocialLogin\Controller\Account;

use Magedelight\SocialLogin\Controller\Account\LoginPost as MDLoginPost;
use Magedelight\SocialLogin\Helper\Data;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session\Proxy as CustomerSession;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\EmailNotConfirmedException;

/**
 * Class LoginPost
 * @package Ktpl\SocialLogin\Controller\Account
 */
class LoginPost extends MDLoginPost
{
    /**
     * LoginPost constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param CustomerUrl $customerHelperData
     * @param Validator $formKeyValidator
     * @param AccountRedirect $accountRedirect
     * @param Data $socialhelper
     * @param Customer $customer
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        AccountManagementInterface $customerAccountManagement,
        CustomerUrl $customerHelperData,
        Validator $formKeyValidator,
        AccountRedirect $accountRedirect,
        Data $socialhelper,
        Customer $customer
    )
    {
        parent::__construct(
            $context,
            $customerSession,
            $customerAccountManagement,
            $customerHelperData,
            $formKeyValidator,
            $accountRedirect,
            $socialhelper
        );
        $this->_customer = $customer;
    }

    /**
     * Login post action.
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $this->layoutFactory = $this->_helper->getlayoutFactory();
        $this->resultJsonFactory = $this->_helper->getjsonFactory();
        $response = $this->_helper->getresponseObject();
        $response->setError(false);
        if ($this->session->isLoggedIn() || !$this->formKeyValidator->validate($this->getRequest())) {
            $backUrl = $this->_helper->getBaseUrl() . 'customer/account';
            $response->setError(false);
            $response->setUrl($backUrl);

            return $this->resultJsonFactory->create()->setJsonData($response->toJson());
        }

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');

            if ($login['login_type'] === 'email') {
                if (!empty($login['username']) && !empty($login['email_password'])) {
                    try {
                        $customer = $this->customerAccountManagement->authenticate($login['username'], $login['email_password']);
                        $this->session->setCustomerDataAsLoggedIn($customer);
                        $this->session->regenerateId();
                    } catch (EmailNotConfirmedException $e) {
                        $value = $this->customerUrl->getEmailConfirmationUrl($login['username']);
                        $message = __(
                            'This account is not confirmed.' .
                            ' <a href="%1">Click here</a> to resend confirmation email.',
                            $value
                        );
                        $this->messageManager->addError($message);
                        $this->session->setUsername($login['username']);
                        $response->setError(true);
                    } catch (AuthenticationException $e) {
                        $message = __('Invalid login or password.');
                        $this->messageManager->addError($message);
                        $this->session->setUsername($login['username']);
                        $response->setError(true);
                    } catch (\Exception $e) {
                        $this->messageManager->addError(__('Invalid login or password.'));
                        $response->setError(true);
                    }
                } else {
                    $this->messageManager->addError(__('A login and a password are required.'));
                    $response->setError(true);
                }
            } elseif ($login['login_type'] === 'mobile') {
                if (!empty($login['mobile_number']) && !empty($login['mobile_password'])) {
                    $customerModel = $this->_customer->getCollection();
                    $customerLoad = $customerModel->addFieldToFilter('mobile_no', $login['mobile_number'])->load();
                    if (count($customerModel) > 0) {
                        try {
                            $customer = $this->customerAccountManagement->authenticate($customerLoad->getFirstItem()->getEmail(), $login['mobile_password']);
                            $this->session->setCustomerDataAsLoggedIn($customer);
                            $this->session->regenerateId();
                        } catch (EmailNotConfirmedException $e) {
                            $value = $this->customerUrl->getEmailConfirmationUrl($login['username']);
                            $message = __(
                                'This account is not confirmed.' .
                                ' <a href="%1">Click here</a> to resend confirmation email.',
                                $value
                            );
                            $this->messageManager->addError($message);
                            $this->session->setUsername($login['username']);
                            $response->setError(true);
                        } catch (AuthenticationException $e) {
                            $message = __('Invalid mobile number or password.');
                            $this->messageManager->addError($message);
                            $this->session->setUsername($login['username']);
                            $response->setError(true);
                        } catch (\Exception $e) {
                            $this->messageManager->addError(__('Invalid mobile number or password.'));
                            $response->setError(true);
                        }
                    } else {
                        $this->messageManager->addError(__('There is no customer registered with mobile number %1', $login['mobile_number']));
                        $response->setError(true);
                    }
                } else {
                    $this->messageManager->addError(__('Invalid mobile number or password'));
                    $response->setError(true);
                }
            } else {
                $this->messageManager->addError(__('Something went wrong. Please try again.'));
                $response->setError(true);
            }
        }
        if ($response->getError() == true) {
            $layout = $this->layoutFactory->create();
            $layout->initMessages();
            $response->setHtmlMessage($layout->getMessagesBlock()->getGroupedHtml());

            return $this->resultJsonFactory->create()->setJsonData($response->toJson());
        } else {
            $backUrl = $this->_helper->getRedirection();
            $response->setUrl($backUrl);

            return $this->resultJsonFactory->create()->setJsonData($response->toJson());
        }
    }
}