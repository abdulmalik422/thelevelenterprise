<?php

namespace Ktpl\SocialLogin\Controller\Account;

use Ktpl\SocialLogin\Cookie\Mobilecookie;
use Ktpl\SocialLogin\Helper\SmsApi;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Sendotp
 * @package Ktpl\SocialLogin\Controller\Account
 */
class Sendotp extends Action {

    /**
     * @var Customer
     */
    protected $_customerFactory;

    /**
     * @var SmsApi
     */
    protected $smsApi;

    /**
     * @var Mobilecookie
     */

    /**
     * Sendotp constructor.
     * @param Customer $customerModel
     * @param StoreManagerInterface $storeManager
     * @param Session $customersession
     * @param SmsApi $smsApi
     * @param Context $context
     * @param Mobilecookie $mobileCookie
     */
    public function __construct(
    Customer $customerModel, StoreManagerInterface $storeManager, Session $customersession, SmsApi $smsApi, Context $context, Mobilecookie $mobileCookie
    ) {
        $this->_customerFactory = $customerModel;
        $this->_storeManager = $storeManager;
        $this->customersession = $customersession;
        $this->smsApi = $smsApi;
        parent::__construct($context);
    }

    /**
     * OTP send to specific mobile number
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $response = [];
        $mobileNo = $this->getRequest()->getParam('mobile_no');
        $formType = $this->getRequest()->getParam('formtype');
        $otp = $this->generateOtp();
        /**
         * Send SMS on various forms
         * login, registration, forgot password
         */
        if ($mobileNo) {
            if ($formType === 'login') {
                if ($this->checkCustomerExistWithMobileNumber($mobileNo)) {
                    //$this->_customCookie->set('otp_' . $mobileNo, $otp, 600);
                    $this->smsApi->saveMobileOtp(['mobile' => $mobileNo, 'otp' => $otp]);
                    $message = $this->smsApi->getLoginMessage($otp);
                    $response['success'] = true;
                    $response['message'] = __('We have sent OTP on %1', $mobileNo);
                    $this->smsApi->sendMessageUsingPrimaryService($mobileNo, $message, $formType);
                } else {
                    $response['error'] = true;
                    $response['message'] = __('Customer with mobile number %1 does not exist. Please register a new account.', $mobileNo);
                }
            } elseif ($formType === 'account_edit') {
                if (!$this->checkCustomerExistWithMobileNumber($mobileNo)) {
                    //$this->_customCookie->set('otp_' . $mobileNo, $otp, 600);
                    $this->smsApi->saveMobileOtp(['mobile' => $mobileNo, 'otp' => $otp]);
                    $message = $this->smsApi->getRegistrationOtpMessage($otp);
                    $response['success'] = true;
                    $response['message'] = __('We have sent OTP on %1', $mobileNo);
                    $this->smsApi->sendMessageUsingPrimaryService($mobileNo, $message, $formType);
                } else {
                    $response['error'] = true;
                    $response['message'] = __('Customer with mobile number %1 already exist.', $mobileNo);
                }
            } elseif ($formType === 'forgotpassword') {
                if ($this->checkCustomerExistWithMobileNumber($mobileNo)) {
                    //$this->_customCookie->set('otp_' . $mobileNo, $otp, 600);
                    $this->smsApi->saveMobileOtp(['mobile' => $mobileNo, 'otp' => $otp]);
                    $message = $this->smsApi->getRegistrationOtpMessage($otp);
                    $response['success'] = true;
                    $response['message'] = __('We have sent OTP on %1', $mobileNo);
                    $this->smsApi->sendMessageUsingPrimaryService($mobileNo, $message, $formType);
                } else {
                    $response['error'] = true;
                    $response['message'] = __('Customer with mobile number does not exist. Please register a new account.');
                }
            } elseif ($formType === 'sendotpfororder') {
                $this->smsApi->saveMobileOtp(['mobile' => $mobileNo, 'otp' => $otp]);
                $message = $this->smsApi->getOrderOtpMessage($otp);
                $response['success'] = true;
                $response['message'] = __('We have sent OTP on %1', $mobileNo);
                $this->smsApi->sendMessageUsingPrimaryService($mobileNo, $message, $formType);
            } else {
                $response['error'] = true;
                $response['message'] = __('Something went wrong. Please try again.');
            }
        } else {
            $response['error'] = true;
            $response['message'] = __('Please enter valid mobile number.');
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;
    }

    /**
     * @return int
     */
    protected function generateOtp() {
        return rand(pow(10, 4 - 1), pow(10, 4) - 1);
    }

    /**
     * @param $mobileNumber
     * @return int|void
     */
    protected function checkCustomerExistWithMobileNumber($mobileNumber) {
        $customerCollection = $this->_customerFactory->getCollection();
        $customerCollection->addFieldToFilter('mobile_no', $mobileNumber)->load();
        return count($customerCollection);
    }

}
