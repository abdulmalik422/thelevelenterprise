<?php

namespace Ktpl\SocialLogin\Controller\Account;

use Magento\Customer\Model\Customer;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Checkemailexist
 * @package Ktpl\SocialLogin\Controller\Account
 */
class Checkemailexist extends \Magento\Framework\App\Action\Action
{
    protected $_customerFactory;

    /**
     * Checkemailexist constructor.
     * @param Customer $customerModel
     * @param Context $context
     */
    public function __construct(
        Customer $customerModel,
        Context $context
    )
    {
        $this->_customerFactory = $customerModel;
        parent::__construct($context);
    }

    /**
     *
     * @return Array
     */
    public function execute()
    {
        $sendResponse = '';
        $post = $this->getRequest()->getParams();

        $customerCollection = $this->_customerFactory->getCollection();

        if (array_key_exists('email', $post)) {
            $customerCollection->addFieldToFilter('email', $post['email'])->load();
        } else {
            $sendResponse['exist'] = false;
        }

        if (count($customerCollection) > 0) {
            $sendResponse['exist'] = true;
            $message = __('Customer with email id: %1 is already exist. Please login with same.', $post['email']);
            $sendResponse['message'] = $message;
        } else {
            $sendResponse['exist'] = false;
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($sendResponse);
        return $resultJson;
    }

}