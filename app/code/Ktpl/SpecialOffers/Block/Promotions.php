<?php

namespace Ktpl\SpecialOffers\Block;

class Promotions extends \Magento\Framework\View\Element\Template {

	protected $_currentProduct;
	protected $_customerSession;
	protected $_dataHelper;
	protected $_dateTimeObj;
	protected $_ruleCollection;

	/**
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Magento\Framework\Registry $registry
	 * @param \Magento\Customer\Model\Session $customerSession
	 * @param \Ktpl\General\Helper\Data $dataHelper
	 * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTimeObj
	 * @param \Magento\CatalogRule\Model\ResourceModel\Rule\CollectionFactory $ruleCollection
	 * @param array $data = []
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Customer\Model\Session $customerSession,
		\Ktpl\General\Helper\Data $dataHelper,
		\Magento\Framework\Stdlib\DateTime\DateTime $dateTimeObj,
		\Magento\CatalogRule\Model\ResourceModel\Rule\CollectionFactory $ruleCollection,
		array $data = []
	) {
		$this->_currentProduct 	= $registry->registry('current_product');
		$this->_customerSession	= $customerSession;
		$this->_dataHelper		= $dataHelper;
		$this->_dateTimeObj		= $dateTimeObj;
		$this->_ruleCollection = $ruleCollection;
		parent::__construct($context, $data);
	}

	/**
	 * Get affected rules list for current product
	 */
	public function getCatalogPriceRules() {
		$productId 			= $this->_currentProduct->getId();
		$customerGroupId 	= $this->getCustomerGroupId();
		$websiteId 			= $this->_dataHelper->getCurrentWebsiteId();
		$dateTs 			= $this->_dateTimeObj->gmtDate();
		
		if (is_string($dateTs)) {
            $dateTs = strtotime($dateTs);
        }

		$rules = $this->_ruleCollection->create()
            ->addFieldToSelect(['name', 'description'])
			->addFieldToFilter('main_table.is_active', 1);

		$rules->getSelect()
            ->joinLeft(['product' => 'catalogrule_product'], 'product.rule_id = main_table.rule_id', 'product.product_id')
            ->where('product.website_id = ?', $websiteId)
            ->where('product.customer_group_id = ?', $customerGroupId)
            ->where('product.product_id = ?', $productId)
            ->where('product.from_time = 0 OR product.from_time < ?', $dateTs)
            ->where('product.to_time = 0 OR product.to_time > ?', $dateTs);
		return $rules;
	}

	/**
	 * Get current customer's (guest/registered) group id
	 */
	public function getCustomerGroupId(){
	 	$groupId = 0;
	 	if($this->_customerSession->isLoggedIn())
	        $groupId = $this->_customerSession->getCustomer()->getGroupId();
	    return $groupId;
	}
}