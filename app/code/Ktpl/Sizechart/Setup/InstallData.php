<?php
namespace Ktpl\Sizechart\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
	private $eavSetupFactory;

	public function __construct(EavSetupFactory $eavSetupFactory)
	{
		$this->eavSetupFactory = $eavSetupFactory;
	}
	
	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'size_chart',
			[
				'type' => 'text',
				'backend' => '',
				'frontend' => '',
				'label' => 'Size Chart',
				'input' => 'select',
				'class' => '',
				'source' => 'Ktpl\Sizechart\Model\Config\Source\Cmsblockoptions',
				'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '',
				'searchable' => true,
				'filterable' => true,
				'comparable' => true,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false
			]
		);
	}
}