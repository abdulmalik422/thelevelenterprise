<?php

namespace Ktpl\Sizechart\Block\Product;

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Api\Data\BlockInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class Sizechart extends \Magento\Framework\View\Element\Template
{
	protected $_coreRegistry;
	private $blockRepository;
	protected $_storeManager;
    protected $categoryRepository;
	protected $staticBlockRepository;
    protected $filterProvider;

	public function __construct(
		\Magento\Framework\Registry $registry,
		BlockRepositoryInterface $blockRepository,
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Cms\Model\BlockRepository $staticBlockRepository,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        array $data = []
	){
		$this->_coreRegistry         = $registry;
		$this->blockRepository       = $blockRepository;
		$this->_storeManager         = $storeManager;
		$this->categoryRepository    = $categoryRepository;
		$this->staticBlockRepository = $staticBlockRepository;
		$this->filterProvider        = $filterProvider;
		parent::__construct($context, $data);
	}

	public function getProduct()
	{
		return $this->_coreRegistry->registry('current_product');
	}

	public function getContent($identifier)
    {
        try {
            $content = $this->getLayout()->createBlock('\Magento\Cms\Block\Block')->setBlockId($identifier)->toHtml();
        	return $content;
        } catch (LocalizedException $e) {
            $content = false;
        }

        return $content;
    }

    public function getCategorySizeChart($id)
    {
		$category = $this->categoryRepository->get($id, $this->_storeManager->getStore()->getId());

		if (!empty($category->getLandingPage())) {
			try {
				$staticBlock = $this->staticBlockRepository->getById($category->getLandingPage());

				if ($staticBlock && $staticBlock->isActive()) {
		            return $this->filterProvider->getBlockFilter()->setStoreId($this->_storeManager->getStore()->getId())->filter($staticBlock->getContent());
		        }
			}
			catch(\Exception $e) {
				return false;
			}
		}

		return false;
    }
}