<?php

namespace Ktpl\Sizechart\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

class Cmsblockoptions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
	protected $optionFactory;

	public function __construct(
		\Magento\Cms\Model\BlockFactory $blockFactory
	) {
		$this->_blockFactory = $blockFactory;
	}

	public function getAllOptions()
    {
        $cmsBlockCollection = $this->_blockFactory->create()->getCollection();
        $this->_options     = array(array('value' => '','label' => '',));
        $this->_options     = [['label'=>'Select CMS Block', 'value'=>'']];

        foreach($cmsBlockCollection as $cmsBlock) {
            $this->_options[] = array('value' => $cmsBlock['identifier'], 'label' => $cmsBlock['title']);
        }

        return $this->_options;
    }

    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}