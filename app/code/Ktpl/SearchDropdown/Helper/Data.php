<?php

namespace Ktpl\SearchDropdown\Helper;

use Magento\Search\Model\QueryFactory;

/**
 * Search helper
 */
class Data extends \Magento\Search\Helper\Data {

    /**
     * Retrieve result page url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     * 
     * @param string $query
     * @param string $search_category
     * @return string
     */
    public function getResultUrl($query = null, $search_category = null) {
        return $this->_getUrl('catalogsearch/result', [
            '_query' => [
                QueryFactory::QUERY_VAR_NAME => $query, 
                'search_category' => $search_category
            ], 
            '_secure' => $this->_request->isSecure()
        ]);
    }

}
