<?php

namespace Ktpl\SearchDropdown\Helper;

/**
 * Search helper
 */
class SearchData extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * @var CategoryFactory
     */
    protected $_categoryFactory;
    protected $_category;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context, 
        \Magento\Framework\App\RequestInterface $request, 
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ) {
        $this->request = $request;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    /**
     * ktpl created function
     * Get category object
     * Using $_categoryFactory
     *
     * @return \Magento\Catalog\Model\Category
     */
    public function getCategory($categoryId) {
        $this->_category = $this->_categoryFactory->create();
        $this->_category->load($categoryId);
        return $this->_category;
    }

    /**
     * ktpl created function
     * Get all children categories IDs
     *
     * @param boolean $asArray return result as array instead of comma-separated list of IDs
     * @return array|string
     */
    public function getAllChildren($asArray = false, $categoryId = false) {
        if ($this->_category && ($this->_category->getId() == $categoryId)) {
            return $this->_category->getAllChildren($asArray);
        } 
        else {
            return $this->getCategory($categoryId)->getAllChildren($asArray);
        }
    }
}
