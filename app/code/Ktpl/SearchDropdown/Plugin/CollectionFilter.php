<?php

namespace Ktpl\SearchDropdown\Plugin;

use Magento\Catalog\Model\Category;
use Magento\Search\Model\QueryFactory;

class CollectionFilter {

    /**
     * @var \Magento\Search\Model\QueryFactory
     */
    protected $queryFactory;

    /**
     * @param QueryFactory $queryFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Ktpl\SearchDropdown\Helper\SearchData $searchData
     */
    public function __construct(
        QueryFactory $queryFactory, 
        \Magento\Framework\App\RequestInterface $request, //ktpl addition
        \Ktpl\SearchDropdown\Helper\SearchData $searchData //ktpl addition
    ) {
        $this->queryFactory = $queryFactory;
        $this->request = $request; //ktpl addition
        $this->searchData = $searchData; //ktpl addition
    }

    /**
     * Add search filter criteria to search collection
     *
     * @param \Magento\Catalog\Model\Layer\Search\CollectionFilter $subject
     * @param null $result
     * @param \Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection $collection
     * @param Category $category
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterFilter(
        \Magento\Catalog\Model\Layer\Search\CollectionFilter $subject, 
        $result, 
        $collection, 
        Category $category
    ) {
        /** @var \Magento\Search\Model\Query $query */
        $query = $this->queryFactory->get();
        if (!$query->isQueryTextShort()) {
            $collection->addSearchFilter($query->getQueryText());
        }

        /* ktpl addition starts */
        $search_category_id = intval($this->request->getParam('search_category'));
        if ($search_category_id) {
            $category_ids = $this->searchData->getAllChildren(true, $search_category_id);
            $collection->addCategoriesFilter(array('in' => $category_ids));
        }
        /* ktpl addition ends */
    }

}
