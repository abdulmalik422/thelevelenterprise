<?php

namespace Ktpl\SearchDropdown\Block;

class CategoryDropdown extends \Magento\Framework\View\Element\Template {

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Helper\Category $category
     * @param \Magento\Catalog\Model\CategoryRepository $categoryRepository
     * @param \Ktpl\General\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Helper\Category $category,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Ktpl\General\Helper\Data $helper,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->category = $category;
        $this->categoryRepository = $categoryRepository;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    public function getCategoryList() {
        $categories = $this->category->getStoreCategories();
        return $categories;
    }

    public function getCurrentCategory() {
        $currentCategory = $this->helper->getRegistry('current_category');
        return $currentCategory;
    }

    public function getCurrentCategoryArray() {
        $currentCategory = $this->getCurrentCategory();
        $currentCategoryArray = array();
        foreach ($currentCategory->getParentCategories() as $parent) {
            if ($parent->getId() != $currentCategory->getId()) {
                $currentCategoryArray[] = $parent->getId();
            }
        }
        return $currentCategoryArray;
    }

    public function loadCategoryById($categoryId) {
        $catCollection = $this->categoryRepository->get($categoryId, $this->_storeManager->getStore()->getId());
        return $catCollection;
    }

}
