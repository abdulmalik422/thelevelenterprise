define([
    'jquery',
    'underscore',
    'mage/template',
    'matchMedia',
    'jquery/ui',
    'mage/translate'
], function ($, _, mageTemplate, mediaCheck) {
    'use strict';

    return function (widget) {

        $.widget('mage.quickSearch', widget, {
            /** @inheritdoc */
            _create: function () {
                this.responseList = {
                    indexList: null,
                    selected: null
                };
                this.autoComplete = $(this.options.destinationSelector);
                this.searchForm = $(this.options.formSelector);
                this.submitBtn = this.searchForm.find(this.options.submitBtn)[0];
                this.searchLabel = $(this.options.searchLabel);
                this.isExpandable = this.options.isExpandable;

                _.bindAll(this, '_onKeyDown', '_onPropertyChange', '_onSubmit');

                //this.submitBtn.disabled = true; //ktpl commented code

                this.element.attr('autocomplete', this.options.autocomplete);

                mediaCheck({
                    media: '(max-width: 768px)',
                    entry: function () {
                        this.isExpandable = true;
                    }.bind(this),
                    exit: function () {
                        this.isExpandable = false;
                        this.element.removeAttr('aria-expanded');
                    }.bind(this)
                });

                this.searchLabel.on('click', function (e) {
                    // allow input to lose its' focus when clicking on label
                    if (this.isExpandable && this.isActive()) {
                        e.preventDefault();
                    }
                }.bind(this));

                //this.element.on('blur', $.proxy(function () { //ktpl commented code
                $('body').on('click', $.proxy(function (e) {
                    if (!e.target.matches('#search_mini_form *') || event.target.matches('#search_mini_form .field.search label.active')) {
                        if (!this.searchLabel.hasClass('active')) {
                            return;
                        }

                        setTimeout($.proxy(function () {
                            if (this.autoComplete.is(':hidden')) {
                                this.setActiveState(false);
                            } else {
                                this.element.trigger('focus');
                            }
                            this.autoComplete.hide();
                            this._updateAriaHasPopup(false);
                        }, this), 250);
                    }
                }, this));

                if (this.element.get(0) === document.activeElement) {
                    this.setActiveState(true);
                }

                this.element.on('focus', this.setActiveState.bind(this, true));
                this.element.on('keydown', this._onKeyDown);
                this.element.on('input propertychange', this._onPropertyChange);

                this.searchForm.on('submit', $.proxy(function (e) {
                    this._onSubmit(e);
                    this._updateAriaHasPopup(false);
                }, this));
            },
        });

        return $.mage.quickSearch;
    }
});
