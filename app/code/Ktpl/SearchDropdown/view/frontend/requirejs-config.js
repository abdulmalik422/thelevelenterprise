/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            chosen: 'Ktpl_SearchDropdown/js/chosen.jquery',
            mCustomScrollbar: 'Ktpl_SearchDropdown/js/jquery.mCustomScrollbar.min',
            Mousewheel: 'Ktpl_SearchDropdown/js/jquery.mousewheel.min'
        }
    },
    "shim": {
        "chosen": ["jquery"],
        "mCustomScrollbar": ["jquery"],
        "Mousewheel": ["jquery"]

    },
    config: {
        mixins: {
	        'Magento_Search/form-mini': {
	            'Ktpl_SearchDropdown/js/form-mini': true
	        }
	    }
    }
};
