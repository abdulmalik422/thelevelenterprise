<?php

namespace Ktpl\SupplierForm\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {
	
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
    	$setup->startSetup();

    	$table = $setup->getConnection()->newTable($setup->getTable('suppliers'))
            ->addColumn('suppliers_id', Table::TYPE_INTEGER, null, 
				['identity' => true, 'nullable' => false, 'primary' => true], 'Supplier ID')
            ->addColumn('name', Table::TYPE_TEXT, 255, ['nullable' => false], 'Supplier Name')
            ->addColumn('email', Table::TYPE_TEXT, 255, ['nullable' => false], 'Supplier Email')
            ->addColumn('phone', Table::TYPE_TEXT, 255, ['nullable' => false], 'Supplier Phone Number')
            ->addColumn('brand_name', Table::TYPE_TEXT, 255, ['nullable' => false], 'Brand Name')
            ->addColumn('categories', Table::TYPE_TEXT, 255, ['nullable' => false], 'Product Categories')
            ->addColumn('quantity', Table::TYPE_NUMERIC, null, ['nullable' => false], 'Product Quantity')
            ->addColumn('insta_account', Table::TYPE_TEXT, 255, ['nullable' => false], 'Instagram Account')
            ->addColumn('is_commercial_registration', Table::TYPE_SMALLINT, 1, 
            	['nullable' => false, 'default' => 1], 'Is Commercial Registration')
            ->addColumn('insta_account', Table::TYPE_TEXT, 255, ['nullable' => false], 'Instagram Account')
            ->addColumn('comments', Table::TYPE_TEXT, null, ['nullable' => false], 'Comments')
            ->addColumn('status', Table::TYPE_SMALLINT, 1, ['nullable' => false, 'default' => 0], 'Status')
            ->addColumn('created_datetime', Table::TYPE_TIMESTAMP, null, 
            	['nullable' => false, 'default' => Table::TIMESTAMP_INIT], 'Supplier Created Datetime')
            ->addColumn('modified_datetime', Table::TYPE_TIMESTAMP, null, 
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE], 'Supplier Modified Datetime')
            ->addIndex(
                $setup->getIdxName(
                    'suppliers', 
                    ['name'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ), 
                ['name'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT])
            ->setComment('Suppliers');

        $setup->getConnection()->createTable($table);

    	$setup->endSetup();
    }
}