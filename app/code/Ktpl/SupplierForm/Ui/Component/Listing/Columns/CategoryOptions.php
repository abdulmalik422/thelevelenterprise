<?php

namespace Ktpl\SupplierForm\Ui\Component\Listing\Columns;

use Magento\Framework\Data\OptionSourceInterface;

class CategoryOptions implements OptionSourceInterface {

    /**
     * @var \Ktpl\SearchDropdown\Block\CategoryDropdown
     */
    protected $_categoryDropdown;

    /**
     * Constructor
     *
     * @param \Ktpl\SearchDropdown\Block\CategoryDropdown $supplier
     */
    public function __construct(
        \Ktpl\SearchDropdown\Block\CategoryDropdown $categoryDropdown
    ) {
        $this->_categoryDropdown = $categoryDropdown;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray() {
        $availableOptions = $this->_categoryDropdown->getCategoryList();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = ['label' => $value->getName(), 'value' => $value->getId()];
        }
        return $options;
    }

}
