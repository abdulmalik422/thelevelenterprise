<?php

namespace Ktpl\SupplierForm\Model\Source\Supplier;

class Status implements \Magento\Framework\Data\OptionSourceInterface {
    
    /**
     * @var \Ktpl\SupplierForm\Model\Supplier
     */
    protected $_supplier;
 
    /**
     * Constructor
     *
     * @param \Ktpl\SupplierForm\Model\Supplier $supplier
     */
    public function __construct(
        \Ktpl\SupplierForm\Model\Supplier $supplier
    ) {
        $this->_supplier = $supplier;
    }
 
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_supplier->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}