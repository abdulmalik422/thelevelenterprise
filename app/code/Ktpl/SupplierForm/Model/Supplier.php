<?php

namespace Ktpl\SupplierForm\Model;

use Ktpl\SupplierForm\Api\Data\SupplierInterface;
use Ktpl\SupplierForm\Model\ResourceModel\Supplier as ResourceSupplier;
use Magento\Framework\Model\AbstractModel;

/**
 * Supplier model
 *
 * @method ResourceSupplier _getResource()
 * @method ResourceSupplier getResource()
 */
class Supplier extends AbstractModel implements SupplierInterface {

    /**
     * Supplier cache tag
     */
    const CACHE_TAG = 'supplier';

    /**
     * Commercial Registration Status
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Supplier Status
     */
    const STATUS_PENDING = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_COMPLETED = 2;

    /**
     * @var string
     */
    protected $_cacheTag = 'supplier';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'supplier';

    /**
     * @return void
     */
    protected function _construct() {
        $this->_init('Ktpl\SupplierForm\Model\ResourceModel\Supplier');
    }

    /**
     * Get ID
     * @return int|null
     */
    public function getId() {
        return $this->getData(self::SUPPLIER_ID);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName() {
        return $this->getData(self::NAME);
    }

    /**
     * Get email
     * @return string|null
     */
    public function getEmail() {
        return $this->getData(self::EMAIL);
    }

    /**
     * Get phone
     * @return string|null
     */
    public function getPhone() {
        return $this->getData(self::PHONE);
    }

    /**
     * Get brand name
     * @return string|null
     */
    public function getBrandName() {
        return $this->getData(self::BRAND_NAME);
    }

    /**
     * Get categories
     * @return string|null
     */
    public function getCategories() {
        return $this->getData(self::CATEGORIES);
    }

    /**
     * Get quanity
     * @return int|null
     */
    public function getQuantity() {
        return $this->getData(self::QUANTITY);
    }

    /**
     * Get is commercial registration
     * @return int|null
     */
    public function getIsCommercialRegistration() {
        return $this->getData(self::IS_COMMERCIAL_REGISTRATION);
    }

    /**
     * Get instagram account
     * @return string|null
     */
    public function getInstaAccount() {
        return $this->getData(self::INSTA_ACCOUNT);
    }

    /**
     * Get comments
     * @return string|null
     */
    public function getComments() {
        return $this->getData(self::COMMENTS);
    }

    /**
     * Get created_datetime
     * @return string|null
     */
    public function getCreatedDatetime() {
        return $this->getData(self::CREATED_DATETIME);
    }

    /**
     * Get modified_datetime
     * @return string|null
     */
    public function getModifiedDatetime() {
        return $this->getData(self::MODIFIED_DATETIME);
    }

    /**
     * Set ID
     * @param int $id
     * @return SupplierInterface
     */
    public function setId($id) {
        return $this->setData(self::SUPPLIER_ID, $id);
    }

    /**
     * Set name
     * @param string $name
     * @return SupplierInterface
     */
    public function setName($name) {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Set email
     * @param string $email
     * @return SupplierInterface
     */
    public function setEmail($email) {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * Set phone
     * @param string $phone
     * @return SupplierInterface
     */
    public function setPhone($phone) {
        return $this->setData(self::PHONE, $phone);
    }
    /**
     * Set brand name
     * @param string $brand_name
     * @return SupplierInterface
     */
    public function setBrandName($brand_name) {
        return $this->setData(self::BRAND_NAME, $brand_name);
    }

    /**
     * Set categories
     * @param string $categories
     * @return SupplierInterface
     */
    public function setCategories($categories) {
        return $this->setData(self::CATEGORIES, $categories);
    }

    /**
     * Set quantity
     * @param int $quantity
     * @return SupplierInterface
     */
    public function setQuantity($quantity) {
        return $this->setData(self::QUANTITY, $quantity);
    }

    /**
     * Set is commercial registration
     * @param int $is_commercial_registration
     * @return SupplierInterface
     */
    public function setIsCommercialRegistration($is_commercial_registration) {
        return $this->setData(self::IS_COMMERCIAL_REGISTRATION, $is_commercial_registration);
    }

    /**
     * Set instagram account
     * @param string $insta_account
     * @return SupplierInterface
     */
    public function setInstaAccount($insta_account) {
        return $this->setData(self::INSTA_ACCOUNT, $insta_account);
    }

    /**
     * Set comments
     * @param string $comments
     * @return SupplierInterface
     */
    public function setComments($comments) {
        return $this->setData(self::COMMENTS, $comments);
    }

    /**
     * Set created_datetime
     * @param string $createdDatetime
     * @return SupplierInterface
     */
    public function setCreatedDatetime($createdDatetime) {
        return $this->setData(self::CREATED_DATETIME, $createdDatetime);
    }

    /**
     * Set modified_datetime
     * @param string $modifiedDatetime
     * @return SupplierInterface
     */
    public function setModifiedDatetime($modifiedDatetime) {
        return $this->setData(self::MODIFIED_DATETIME, $modifiedDatetime);
    }

    /**
     * Prepare commercial registration statuses.
     * @return array
     */
    public function commercialRegistrationStatus() {
        return [self::STATUS_ENABLED => __('Yes'), self::STATUS_DISABLED => __('No')];
    }

    /**
     * Prepare supplier statuses.
     * @return array
     */
    public function getAvailableStatuses() {
        return [
            self::STATUS_PENDING => __('Pending'), 
            self::STATUS_PROCESSING => __('Processing'),
            self::STATUS_COMPLETED => __('Completed')
        ];
    }

}
