<?php

namespace Ktpl\SupplierForm\Model;

use Ktpl\SupplierForm\Model\ResourceModel\Supplier\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

    /**
     * @var \Ktpl\SupplierForm\Model\ResourceModel\Supplier\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $supplierCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name, 
        $primaryFieldName, 
        $requestFieldName, 
        CollectionFactory $supplierCollectionFactory, 
        DataPersistorInterface $dataPersistor, 
        array $meta = [], 
        array $data = []
    ) {
        $this->collection = $supplierCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData() {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        /** @var \Ktpl\SupplierForm\Model\Supplier $supplier */
        foreach ($items as $supplier) {
            $supplierData = $supplier->getData();
            // --- start: To display checkboxes checked on edit supplier admin form
            $supplierData['categories'] = explode(",", $supplierData['categories']);
            // --- end:
            $this->loadedData[$supplier->getId()] = $supplierData;
        }

        $data = $this->dataPersistor->get('supplier');
        if (!empty($data)) {
            $supplier = $this->collection->getNewEmptyItem();
            $supplier->setData($data);
            $this->loadedData[$supplier->getId()] = $supplier->getData();
            $this->dataPersistor->clear('supplier');
        }

        return $this->loadedData;
    }

}
