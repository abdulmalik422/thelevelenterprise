<?php

namespace Ktpl\SupplierForm\Model\ResourceModel\Supplier;

use Ktpl\SupplierForm\Api\Data\SupplierInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Supplier Collection
 */
class Collection extends AbstractCollection {

    /**
     * @var string
     */
    protected $_idFieldName = 'suppliers_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Ktpl\SupplierForm\Model\Supplier', 'Ktpl\SupplierForm\Model\ResourceModel\Supplier');
    }

}
