<?php

namespace Ktpl\SupplierForm\Model\Status;

use Magento\Framework\Data\OptionSourceInterface;

class IsCommercialRegistration implements OptionSourceInterface {

    /**
     * @var \Ktpl\SupplierForm\Model\Supplier
     */
    protected $_supplier;

    /**
     * Constructor
     *
     * @param \Ktpl\SupplierForm\Model\Supplier $supplier
     */
    public function __construct(
        \Ktpl\SupplierForm\Model\Supplier $supplier
    ) {
        $this->_supplier = $supplier;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray() {
        $availableOptions = $this->_supplier->commercialRegistrationStatus();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = ['label' => $value, 'value' => $key];
        }
        return $options;
    }

}
