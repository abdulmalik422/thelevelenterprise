<?php

namespace Ktpl\SupplierForm\Controller\Adminhtml\Index;

class Delete extends \Ktpl\SupplierForm\Controller\Adminhtml\Index {
    
    /**
     * @var \Ktpl\SupplierForm\Model\Supplier 
     */
    protected $_supplier;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Ktpl\SupplierForm\Model\Supplier $supplier
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context, 
        \Magento\Framework\Registry $coreRegistry,
        \Ktpl\SupplierForm\Model\Supplier $supplier
    ) {
        $this->_supplier = $supplier;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        
        if ($id) {
            try {
                // init model and delete
                $model = $this->_supplier;
                $model->load($id)->delete();
                
                // display success message
                $this->messageManager->addSuccess(__('You deleted the supplier.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } 
            catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a supplier to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

}
