<?php

namespace Ktpl\SupplierForm\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;

class MassDelete extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $_filter;

    /**
     * @var \Ktpl\SupplierForm\Model\ResourceModel\Supplier\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Ktpl\SupplierForm\Model\ResourceModel\Supplier\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context, 
        \Magento\Ui\Component\MassAction\Filter $filter, 
        \Ktpl\SupplierForm\Model\ResourceModel\Supplier\CollectionFactory $collectionFactory
    ) {
        $this->_filter              = $filter;
        $this->_collectionFactory   = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());

        $collectionSize = 0;
        foreach ($collection as $supplier) {            
            $supplier->delete();
            $collectionSize++;
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

}
