<?php

namespace Ktpl\SupplierForm\Controller\Adminhtml\Index;

class Edit extends \Ktpl\SupplierForm\Controller\Adminhtml\Index {

	/**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Ktpl\SupplierForm\Model\Supplier
     */
    protected $_supplier;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context, 
        \Magento\Framework\Registry $coreRegistry, 
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Ktpl\SupplierForm\Model\Supplier $supplier
    ) {
        $this->_resultPageFactory	= $resultPageFactory;
        $this->_supplier 			= $supplier;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit Supplier Request
     */
    public function execute() {
    	$id = $this->getRequest()->getParam('id');

    	$model = $this->_supplier;
    	if ($id) {
    		$model->load($id);
    		if (!$model->getId()) {
                $this->messageManager->addError(__('This supplier no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
    	}
    	$this->_coreRegistry->register('supplier_item_form_data_source', $model);
    	
    	$resultPage = $this->_resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb( __('Edit Supplier'), __('Edit Supplier'));
        $resultPage->getConfig()->getTitle()->prepend(__('Suppliers'));
        $resultPage->getConfig()->getTitle()->prepend(__('Update Supplier'));
        return $resultPage;
    }
}