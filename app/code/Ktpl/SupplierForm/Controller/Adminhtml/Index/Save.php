<?php

namespace Ktpl\SupplierForm\Controller\Adminhtml\Index;

use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;

class Save extends \Ktpl\SupplierForm\Controller\Adminhtml\Index {

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $_dataPersistor;
    
    /**
     * @var \Ktpl\SupplierForm\Model\Supplier 
     */
    protected $_supplier;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param \Ktpl\SupplierForm\Model\Supplier $supplier
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context, 
        \Magento\Framework\Registry $coreRegistry, 
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Ktpl\SupplierForm\Model\Supplier $supplier
    ) {
        $this->_dataPersistor   = $dataPersistor;
        $this->_supplier        = $supplier;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        
        if ($data) {
            $id = $data['suppliers_id'];

            if (empty($data['suppliers_id'])) {
                $data['suppliers_id'] = null;
            }
            
            if (!empty($data['categories'])){
                $data['categories'] = implode(',', $data['categories']);
            }

            /** @var \Ktpl\SupplierForm\Model\Supplier $model */
            $model = $this->_supplier->load($id);
            
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This supplier no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            
            $model->setData($data);
            $model->setModifiedDatetime(date('Y-m-d H:i:s'));
            
            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved the supplier.'));
                $this->_dataPersistor->clear('supplier');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } 
            catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } 
            catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the supplier.'));
            }

            $this->_dataPersistor->set('supplier', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
