<?php

namespace Ktpl\SupplierForm\Controller\Index;

use Magento\Store\Model\StoreManagerInterface;
use Ktpl\SocialLogin\Helper\SmsApi;

class Save extends \Magento\Framework\App\Action\Action {

	private $transportBuilder;
	private $inlineTranslation;
	protected $_supplier;
    protected $_categoryCollectionFactory;
    protected $_generalHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var SmsApi
     */
    protected $smsApi;

	/**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Ktpl\SupplierForm\Model\Supplier $supplier
     * @param \Ktpl\General\Helper\Data $generalHelper
     * @param StoreManagerInterface $storeManager
     */
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
		\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
		\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
		\Ktpl\SupplierForm\Model\Supplier $supplier,
		\Ktpl\General\Helper\Data $generalHelper,
        StoreManagerInterface $storeManager,
        SmsApi $smsApi
	) {
		$this->_categoryCollectionFactory 	= $categoryCollectionFactory;
		$this->transportBuilder 			= $transportBuilder;
		$this->inlineTranslation 			= $inlineTranslation;
		$this->_supplier 					= $supplier;
		$this->_generalHelper 				= $generalHelper;
        $this->_storeManager                = $storeManager;
        $this->smsApi                       = $smsApi;
		parent::__construct($context);
	}

    public function execute() {
		$post = (array) $this->getRequest()->getPost();

		if (!empty($post)) {
			$data = $post;

			$data['categories'] = implode(",", $data['categories']);
			$data['created_datetime'] = date('Y-m-d H:i:s');
			$this->_supplier->setData($data);

			$resultRedirect = $this->resultRedirectFactory->create();
			if ($this->_supplier->save()) {
				$this->sendNoficationEmail($data);
				if(isset($data['phone']) && !empty($data['phone'])) {
                    $this->sendSmsToSupplier($data['phone'],$data,'supplier_form');
                }
				$this->messageManager->addSuccess(__('A supplier request has been sent successfully.'));
			}
			else {
				$this->messageManager->addError(__('Something went wrong. Please try again!'));	
			}
		}
		else {
			$this->messageManager->addError(__('Something went wrong. Please try again!'));	
		}
		return $resultRedirect->setPath('*/*/requestform');
	}

    public function sendNoficationEmail($data) {
        if (!empty($data['categories'])) {
            $collection = $this->_categoryCollectionFactory->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', ['in' => $data['categories']]);
            $arrCat = [];
            foreach ($collection as $key => $value) {
            	array_push($arrCat, $value->getName());
            }
            $data['categories'] = implode(", ", $arrCat);
        }
        $data['is_commercial_registration'] = ($data['is_commercial_registration']) ? "Yes" : "No";

        unset($data['captcha']);
        unset($data['save_form']);
        unset($data['created_datetime']);

        $this->inlineTranslation->suspend();

        try {
        	$fromEmail = $this->_generalHelper->getStoreGeneralSenderEmail();
        	$fromName = $this->_generalHelper->getStoreGeneralSenderName();

	        $transport = $this->transportBuilder
	        	->setTemplateIdentifier('supplier_email_email_template')
	        	->setTemplateOptions([
		        		'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		        		'store' => $this->_generalHelper->getStoreId()
	        		])
	        	->setTemplateVars($data)
	        	->setFrom(['email' => $fromEmail, 'name' => $fromName])
	            ->addTo($data['email'])
	            ->setReplyTo($fromEmail, $fromName)
	            ->getTransport();
	        $transport->sendMessage();
	    }
	    finally {
            $this->inlineTranslation->resume();
        }
    }

    /**
     * @param $data
     * @return \Magento\Framework\Phrase
     */
    protected function getSupplierMessage($data)
    {
	    if(!is_null($data)) {
	        $supplierName = $data['name'];
	        $productCategories = $this->getCategoriesNameFromIds($data);
	        $brand = $data['brand_name'];
            return __('Hello %1, we have received your application for being supplier for %2 of %3. We will contact you soon. Thanks %4.',$supplierName,$productCategories,$brand,$this->getStoreName());
        } else {
	        return __('Thank you for applying for being a supplier on %1',$this->getStoreName());
        }
    }

    /**
     * @param $mobilenumber
     * @param $data
     * @param null $formType
     * @return \Magento\Framework\Phrase
     */
    protected function sendSmsToSupplier($mobilenumber, $data, $formType = null)
    {
        if(!empty($mobilenumber)) {
            $message = $this->getSupplierMessage($data);
            $this->smsApi->sendMessageUsingPrimaryService($mobilenumber,$message,$formType);
        } else {
            return __('Please enter valid mobile number.');
        }
    }

    /**
     * Get Store name
     *
     * @return string
     */
    public function getStoreName()
    {
        return $this->_storeManager->getStore()->getName();
    }

    /**
     * @param $data
     * @return string
     */
    protected function getCategoriesNameFromIds($data)
    {
        if (!empty($data['categories'])) {
            $collection = $this->_categoryCollectionFactory->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', ['in' => $data['categories']]);
            $arrCat = [];
            foreach ($collection as $key => $value) {
                array_push($arrCat, $value->getName());
            }
            $categories = implode(", ", $arrCat);
            return $categories;
        } else {
            return '';
        }
    }
}