<?php

namespace Ktpl\SupplierForm\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Supplier CRUD interface.
 * @api
 */
interface SupplierRepositoryInterface {

    /**
     * Save supplier.
     *
     * @param \Ktpl\SupplierForm\Api\Data\SupplierInterface $supplier
     * @return \Ktpl\SupplierForm\Api\Data\SupplierInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\SupplierInterface $supplier);

    /**
     * Retrieve supplier.
     *
     * @param int $supplierId
     * @return \Ktpl\SupplierForm\Api\Data\SupplierInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($supplierId);

    /**
     * Retrieve suppliers matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Ktpl\SupplierForm\Api\Data\SupplierSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete supplier.
     *
     * @param \Ktpl\SupplierForm\Api\Data\SupplierInterface $supplier
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\SupplierInterface $supplier);

    /**
     * Delete supplier by ID.
     *
     * @param int $supplierId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($supplierId);
}
