<?php

namespace Ktpl\SupplierForm\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for supplier search results.
 * @api
 */
interface SupplierSearchResultsInterface extends SearchResultsInterface {

    /**
     * Get supplier list.
     * @return \Ktpl\SupplierForm\Api\Data\SupplierInterface[]
     */
    public function getItems();

    /**
     * Set supplier list.
     * @param \Ktpl\SupplierForm\Api\Data\SupplierInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
