<?php

namespace Ktpl\SupplierForm\Api\Data;

/**
 * Supplier interface.
 * @api
 */
interface SupplierInterface {

	/**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
	const SUPPLIER_ID          = 'suppliers_id';
	const NAME                 = 'name';
	const EMAIL                = 'email';
	const PHONE                = 'phone';
	const BRAND_NAME           = 'brand_name';
	const CATEGORIES           = 'categories';
	const QUANTITY             = 'quantity';
	const IS_COMMERCIAL_REGISTRATION = 'is_commercial_registration';
	const INSTA_ACCOUNT        = 'insta_account';
	const COMMENTS             = 'comments';
	const CREATED_DATETIME     = 'created_datetime';
	const MODIFIED_DATETIME    = 'modified_datetime';

	/**
     * Get ID
     * @return int|null
     */
    public function getId();

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Get email
     * @return string|null
     */
    public function getEmail();

    /**
     * Get phone
     * @return string|null
     */
    public function getPhone();

    /**
     * Get brand name
     * @return string|null
     */
    public function getBrandName();

    /**
     * Get categories
     * @return string|null
     */
    public function getCategories();

    /**
     * Get quanity
     * @return int|null
     */
    public function getQuantity();

    /**
     * Get is commercial registration
     * @return int|null
     */
    public function getIsCommercialRegistration();

    /**
     * Get instagram account
     * @return string|null
     */
    public function getInstaAccount();

    /**
     * Get comments
     * @return string|null
     */
    public function getComments();

    /**
     * Get created_datetime
     * @return string|null
     */
    public function getCreatedDatetime();

    /**
     * Get modified_datetime
     * @return string|null
     */
    public function getModifiedDatetime();

    /**
     * Set ID
     * @param int $id
     * @return SupplierInterface
     */
    public function setId($id);

    /**
     * Set name
     * @param string $name
     * @return SupplierInterface
     */
    public function setName($name);

    /**
     * Set email
     * @param string $email
     * @return SupplierInterface
     */
    public function setEmail($email);

    /**
     * Set phone
     * @param string $phone
     * @return SupplierInterface
     */
    public function setPhone($phone);

    /**
     * Set brand name
     * @param string $brand_name
     * @return SupplierInterface
     */
    public function setBrandName($brand_name);

    /**
     * Set categories
     * @param string $categories
     * @return SupplierInterface
     */
    public function setCategories($categories);

    /**
     * Set quantity
     * @param int $quantity
     * @return SupplierInterface
     */
    public function setQuantity($quantity);

    /**
     * Set is commercial registration
     * @param int $is_commercial_registration
     * @return SupplierInterface
     */
    public function setIsCommercialRegistration($is_commercial_registration);

    /**
     * Set instagram account
     * @param string $insta_account
     * @return SupplierInterface
     */
    public function setInstaAccount($insta_account);

    /**
     * Set comments
     * @param string $comments
     * @return SupplierInterface
     */
    public function setComments($comments);

    /**
     * Set created_datetime
     * @param string $createdDatetime
     * @return SupplierInterface
     */
    public function setCreatedDatetime($createdDatetime);

    /**
     * Set modified_datetime
     * @param string $modifiedDatetime
     * @return SupplierInterface
     */
    public function setModifiedDatetime($modifiedDatetime);
}