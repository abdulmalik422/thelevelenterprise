<?php

namespace Ktpl\SupplierForm\Block;

class Supplier extends \Magento\Framework\View\Element\Template {

	/**
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param array $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		array $data = []
	) {
		parent::__construct($context, $data);
	}

	/**
     * @return string
     */
    public function getFormAction() {
        return $this->getUrl('supplier/index/save', ['_secure' => true]);
    }
}