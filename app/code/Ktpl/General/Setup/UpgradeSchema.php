<?php

namespace Ktpl\General\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

	/**
	 * @var \Magento\Directory\Model\RegionFactory
	 */
	protected $_regionFactory;

	/**
	 * @param \Magento\Directory\Model\RegionFactory $regionFactory
	 */
	public function __construct(
		\Magento\Directory\Model\RegionFactory $regionFactory
	) {
		$this->_regionFactory = $regionFactory;
	}

	/**
	 * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
	 * @param \Magento\Framework\Setup\ModuleContextInterface $context
	 */
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
		$setup->startSetup();

		if (version_compare($context->getVersion(), '1.0.1', '<')) {
			$arrData = [
				'SA-11' => 'Al Bahah',
				'SA-12' => 'Al Jowf',
				'SA-03' => 'Al Madinah',
				'SA-05' => 'Al Qassim',
				'SA-14' => 'Aseer',
				'SA-04' => 'Eastern Province',
				'SA-06' => 'Hail',
				'SA-09' => 'Jazan',
				'SA-02' => 'Makkah',
				'SA-10' => 'Najran',
				'SA-08' => 'Northern Province',
				'SA-01' => 'Riyadh',
				'SA-07' => 'Tabuk'
			];
			foreach ($arrData as $code => $name) {
				$countryId = 'SA';
				$regionObj = $this->_regionFactory->create();
                $region = $regionObj->loadByCode($code, $countryId);
                if (empty($region->getData())) {
                    $data = [
                        'country_id'    => $countryId,
                        'code'          => $code,
                        'default_name'  => $name
                    ];
                    $region->setData($data);
                    $region->save();
                }
			}
		}

		$setup->endSetup();
	}
}