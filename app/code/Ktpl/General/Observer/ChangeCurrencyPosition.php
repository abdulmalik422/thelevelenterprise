<?php

namespace Ktpl\General\Observer;

class ChangeCurrencyPosition implements \Magento\Framework\Event\ObserverInterface {
    
    public function execute(\Magento\Framework\Event\Observer $observer) {    
        $currencyOptions = $observer->getEvent()->getCurrencyOptions();    
        $currencyOptions->setData('position', \Magento\Framework\Currency::RIGHT);  
        return $this;
    } 
}