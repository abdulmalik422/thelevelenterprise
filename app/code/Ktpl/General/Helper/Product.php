<?php

namespace Ktpl\General\Helper;

use Magento\Store\Model\ScopeInterface;

class Product extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * @var \Magento\Catalog\Block\Product\ListProduct
     */
    protected $_listProduct;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productloader;

    /**
     * @var \Amasty\ShopbyBrand\Block\Widget\BrandSlider
     */
    protected $_brandSlider;

    protected $_objectManager;

    private $optionHelper;

    private $storeManager;

    private $filterProvider;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Block\Product\ListProduct $listProduct
     * @param \Magento\Catalog\Model\ProductFactory $productloader
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Block\Product\ListProduct $listProduct,
        \Magento\Catalog\Model\ProductFactory $productloader,
        \Amasty\ShopbyBrand\Block\Widget\BrandSlider $brandSlider,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Amasty\ShopbyBase\Helper\OptionSetting $optionHelper,
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider
    ) {
        $this->_listProduct   = $listProduct;
        $this->_productloader = $productloader;
        $this->_brandSlider   = $brandSlider;
        $this->_objectManager = $objectManager;
        $this->optionHelper   = $optionHelper;
        $this->storeManager   = $storeManager;
        $this->filterProvider = $filterProvider;
        parent::__construct($context);
    }

    public function getAddToCartPostParams($product) {
        return $this->_listProduct->getAddToCartPostParams($product);
    }

    public function getProductPrice($product) {
        return $this->_listProduct->getProductPrice($product);
    }

    public function getProductImage($product) {
        return $this->_listProduct->getImage($product, 'category_page_grid');
    }

    public function loadProduct($id) {
        return $this->_productloader->create()->load($id);
    }

    public function getAddToWishlistParams($product) {
        return $this->_listProduct->getAddToWishlistParams($product);
    }

    public function getBrandUrl($label) {
        $brands = $this->_brandSlider->getItems();
        $url = "";
        foreach ($brands as $brand) {
            if ($brand['label'] == $label) {
                $url = $brand['url'];
            }
        }
        return $url;
    }

    public function getObjectManager($objectmanager) {
        return $this->_objectManager->get($objectmanager);
    }

    public function getBrandData($brandId) {
        $attributeCode = $this->scopeConfig->getValue('amshopby_brand/general/attribute_code', ScopeInterface::SCOPE_STORE);

        return $this->optionHelper->getSettingByValue(
            $brandId,
            \Amasty\ShopbyBase\Helper\FilterSetting::ATTR_PREFIX . $attributeCode,
            $this->storeManager->getStore()->getId()
        );
    }

    public function getPageFilter() {
        return $this->filterProvider;
    }
}
