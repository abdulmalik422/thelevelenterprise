<?php

namespace Ktpl\General\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    protected $_scopeConfig;
    protected $_storeManager;
    protected $redirect;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Response\RedirectInterface $redirect
    ) {
        $this->_scopeConfig  = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->redirect      = $redirect;
        parent::__construct($context);
    }

    public function getScopeConfigValue($value) {
        return $this->_scopeConfig->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getMediaUrl() {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getStoreId() {
        return $this->_storeManager->getStore()->getId();
    }

    public function getStoreViewCode() {
        return $this->_storeManager->getStore()->getCode();
    }

    public function getCurrentWebsiteId() {
        return $this->_storeManager->getStore()->getWebsiteId();
    }

    public function getRefererUrl() {
        return $this->redirect->getRefererUrl();
    }

    public function getStoreGeneralSenderName() {
        return $this->_scopeConfig->getValue('trans_email/ident_general/name');
    }

    public function getStoreGeneralSenderEmail() {
        return $this->_scopeConfig->getValue('trans_email/ident_general/email');
    }

    public function isMobile() {
        $regex_match = "/(nokia|iphone|android|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|" 
            . "htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|" 
            . "blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|" 
            . "symbian|smartphone|mmp|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|" 
            . "jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220" 
            . ")/i";
        if (preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) || 
            ((isset($_SERVER['HTTP_X_WAP_PROFILE']) || 
            isset($_SERVER['HTTP_PROFILE'])))) {
            return true;
        }
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array( 
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-',
            'dang','doco','eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','oper','palm','pana',
            'pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar', 
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v',
            'voda','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda ','xda-');

        if (in_array($mobile_ua, $mobile_agents)) { 
            return true;
        }
        if (isset($_SERVER['ALL_HTTP']) && strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini') > 0) { 
            return true;
        }
        return false;
    }
}
