<?php

namespace Ktpl\OrderTracking\Block;

use Magento\Sales\Api\Data\ShipmentTrackInterface;

class TrackOrder extends \Magento\Framework\View\Element\Template {

	protected $_order;
	protected $_shipmentCollection;
	protected $_customerSession;
	protected $_customer;

	/**
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Magento\Sales\Model\Order $order
	 * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentCollection
	 * @param array $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Sales\Model\Order $order,
		\Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentCollection,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Customer\Model\Customer $customer,
		array $data = []
	) {
		$this->_order 				= $order;
		$this->_shipmentCollection 	= $shipmentCollection;
		$this->_customerSession 	= $customerSession;
		$this->_customer 			= $customer;
		parent::__construct($context, $data);
	}

	public function getCustomerEmail() {
		if ($this->_customerSession->getCustomerId()) {
			$customer = $this->_customer->load($this->_customerSession->getCustomerId());
			return $customer->getEmail();
		}
		return "";
	}

	public function getOrderInfo() {
		$post = (array) $this->getRequest()->getPost();
		$response = [];

		if (isset($post) && !empty($post)) {
			$orderNo 	= isset($post['orderno']) ? $post['orderno'] : "";
			$orderEmail = isset($post['email']) ? $post['email'] : "";

			if (!empty($orderNo) && !empty($orderEmail)) {
				$order = $this->_order->getCollection()
					->addFieldToSelect(['status', 'grand_total', 'customer_email', 'base_currency_code'])
					->addFieldToFilter('main_table.increment_id', $orderNo);

				$order->getSelect()
					->joinLeft(['shipment' => 'sales_shipment'], 'shipment.order_id = main_table.entity_id', ['increment_id', 'created_at'])
					->joinLeft(['ship_track' => 'sales_shipment_track'], 'ship_track.parent_id = shipment.entity_id', ['track_number', 'title']);

				if (!empty($order->getData())) {
					$orderInfo = $order->getData()[0];

					if ($orderInfo['customer_email'] == $orderEmail) {
						$response['order_status'] 	= $orderInfo['status'];
						$response['grand_total'] 	= number_format($orderInfo['grand_total'], 2) . " " . $orderInfo['base_currency_code'];
						$response['order_number'] 	= $orderNo;
						$response['shipment_id'] 	= $orderInfo['increment_id'];
						$response['track_number'] 	= $orderInfo['track_number'];
						$response['title'] 			= $orderInfo['title'];
						$response['created_date'] 	= (!empty($orderInfo['created_at'])) ? date('Y-m-d', strtotime($orderInfo['created_at'])) : "";
					}
					else {
						$response['error'] = __('Wrong e-mail address entered for requested order.');
					}
				}
				else {
					$response['error'] = __('Order number is not available.');
				}
			}
		}

		return $response;
	}
}