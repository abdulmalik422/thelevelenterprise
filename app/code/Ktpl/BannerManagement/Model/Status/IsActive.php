<?php

namespace Ktpl\BannerManagement\Model\Status;

use Magento\Framework\Data\OptionSourceInterface;

class IsActive implements OptionSourceInterface {

    /**
     * @var \Ktpl\BannerManagement\Model\Banner
     */
    protected $_banner;

    /**
     * Constructor
     *
     * @param \Ktpl\BannerManagement\Model\Banner $banner
     */
    public function __construct(\Ktpl\BannerManagement\Model\Banner $banner) {
        $this->_banner = $banner;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray() {
        $availableOptions = $this->_banner->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = ['label' => $value, 'value' => $key];
        }
        return $options;
    }

}
