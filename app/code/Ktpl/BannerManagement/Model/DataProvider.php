<?php

namespace Ktpl\BannerManagement\Model;

use Ktpl\BannerManagement\Model\ResourceModel\Banner\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Ktpl\BannerManagement\Helper\Image;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

    /**
     * @var \Ktpl\BannerManagement\Model\ResourceModel\Banner\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $bannerCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name, 
        $primaryFieldName, 
        $requestFieldName, 
        CollectionFactory $bannerCollectionFactory, 
        DataPersistorInterface $dataPersistor, 
        Image $imageUrl, 
        array $meta = [], 
        array $data = []
    ) {
        $this->collection = $bannerCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->imageUrl = $imageUrl->_getUrl();
        $this->smallImageUrl = $imageUrl->_getUrl(1);
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData() {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Ktpl\BannerManagement\Model\Banner $banner */
        foreach ($items as $banner) {
            $bannerData = $banner->getData();
            if (isset($bannerData['image'])) {
                unset($bannerData['image']);
                $bannerData['image'][0]['name'] = $banner->getData('image');
                $bannerData['image'][0]['url'] = $this->imageUrl . $banner->getData('image');
                unset($bannerData['small_banner_image']);
                $bannerData['small_banner_image'][0]['name'] = $banner->getData('small_banner_image');
                $bannerData['small_banner_image'][0]['url'] = $this->smallImageUrl . $banner->getData('small_banner_image');
            }
            $this->loadedData[$banner->getId()] = $bannerData;
        }

        $data = $this->dataPersistor->get('banners');
        if (!empty($data)) {
            $banner = $this->collection->getNewEmptyItem();
            $banner->setData($data);
            $this->loadedData[$banner->getId()] = $banner->getData();
            $this->dataPersistor->clear('banners');
        }

        return $this->loadedData;
    }

}
