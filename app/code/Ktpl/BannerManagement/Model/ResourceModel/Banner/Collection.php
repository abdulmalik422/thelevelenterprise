<?php

namespace Ktpl\BannerManagement\Model\ResourceModel\Banner;

use Ktpl\BannerManagement\Api\Data\BannerInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Banner Collection
 */
class Collection extends AbstractCollection {

    /**
     * @var string
     */
    protected $_idFieldName = 'banners_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Ktpl\BannerManagement\Model\Banner', 'Ktpl\BannerManagement\Model\ResourceModel\Banner');
    }

}
