<?php

namespace Ktpl\BannerManagement\Model;

use Ktpl\BannerManagement\Api\Data\BannerInterface;
use Ktpl\BannerManagement\Model\ResourceModel\Banner as ResourceBanner;
use Magento\Framework\Model\AbstractModel;

/**
 * Banner model
 *
 * @method ResourceBanner _getResource()
 * @method ResourceBanner getResource()
 * @method Banner setStoreId(array $storeId)
 * @method array getStoreId()
 */
class Banner extends AbstractModel implements BannerInterface {

    /**
     * Banner cache tag
     */
    const CACHE_TAG = 'banners';

    /*     * #@+
     * Banner's statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /*     * #@- */

    /**
     * @var string
     */
    protected $_cacheTag = 'banners';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'banners';

    /**
     * @return void
     */
    protected function _construct() {
        $this->_init('Ktpl\BannerManagement\Model\ResourceModel\Banner');
    }

    /**
     * Retrieve banner id
     *
     * @return int
     */
    public function getId() {
        return $this->getData(self::BANNERS_ID);
    }

    /**
     * Retrieve image
     *
     * @return string
     */
    public function getImage() {
        return (string) $this->getData(self::IMAGE);
    }

    /**
     * Retrieve small_banner_image
     *
     * @return string
     */
    public function getSmallBannerImage() {
        return (string) $this->getData(self::SMALL_BANNER_IMAGE);
    }

    /**
     * Retrieve banner title
     *
     * @return string
     */
    public function getTitle() {
        return $this->getData(self::TITLE);
    }

    /**
     * Retrieve banner link
     *
     * @return string
     */
    public function getLink() {
        return $this->getData(self::LINK);
    }

    /**
     * Is active
     *
     * @return bool
     */
    public function isActive() {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Retrive Sort Order
     *
     * @return int
     */
    public function GetSortOrder() {
        return (bool) $this->getData(self::SORT_ORDER);
    }

    /**
     * Retrieve banner created_datetime
     *
     * @return string
     */
    public function getCreatedDatetime() {
        return $this->getData(self::CREATED_DATETIME);
    }

    /**
     * Retrieve banner modified_datetime
     *
     * @return string
     */
    public function getModifiedDatetime() {
        return $this->getData(self::MODIFIED_DATETIME);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return BannerInterface
     */
    public function setId($id) {
        return $this->setData(self::BANNERS_ID, $id);
    }

    /**
     * Set image
     *
     * @param string $image
     * @return BannerInterface
     */
    public function setImage($image) {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Set small_banner_image
     *
     * @param string $small_banner_image
     * @return BannerInterface
     */
    public function setSmallBannerImage($small_banner_image) {
        return $this->setData(self::SMALL_BANNER_IMAGE, $small_banner_image);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return BannerInterface
     */
    public function setTitle($title) {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set link
     *
     * @param string $link
     * @return BannerInterface
     */
    public function setLink($link) {
        return $this->setData(self::LINK, $link);
    }

    /**
     * Set is active
     *
     * @param bool|int $isActive
     * @return BannerInterface
     */
    public function setIsActive($isActive) {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Set Sort Order
     *
     * @param int $sortOrder
     * @return BannerInterface
     */
    public function setSortOrder($sortOrder) {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Set created_datetime
     *
     * @param string $createdDatetime
     * @return BannerInterface
     */
    public function setCreatedDatetime($createdDatetime) {
        return $this->setData(self::CREATED_DATETIME, $createdDatetime);
    }

    /**
     * Set modified_datetime
     *
     * @param string $modifiedDatetime
     * @return BannerInterface
     */
    public function setModifiedDatetime($modifiedDatetime) {
        return $this->setData(self::MODIFIED_DATETIME, $modifiedDatetime);
    }

    /**
     * Receive page store ids
     *
     * @return int[]
     */
    public function getStores() {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');
    }

    /**
     * Prepare banner's statuses.
     *
     * @return array
     */
    public function getAvailableStatuses() {
        return [self::STATUS_ENABLED => __('Yes'), self::STATUS_DISABLED => __('No')];
    }

}
