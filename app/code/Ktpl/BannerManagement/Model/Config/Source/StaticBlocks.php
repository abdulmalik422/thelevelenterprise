<?php

namespace Ktpl\BannerManagement\Model\Config\Source;

class StaticBlocks implements \Magento\Framework\Option\ArrayInterface {
    
    protected  $_blockFactory;

    public function __construct(
        \Magento\Cms\Model\BlockFactory $blockFactory
    ) {
        $this->_blockFactory = $blockFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        $blocks = $this->_blockFactory->create()->getCollection()
            ->addFieldToSelect(['identifier', 'title'])
            ->addFieldToFilter('is_active', ['eq' => '1'])
            ->setOrder('title', 'asc');
        
        $list = [];
        if ($blocks->count()) {
            foreach ($blocks as $b) {
                $list[] = ['value' => $b->getIdentifier(), 'label' => $b->getTitle()];
            }
        }
        
        return $list;
    }
}
