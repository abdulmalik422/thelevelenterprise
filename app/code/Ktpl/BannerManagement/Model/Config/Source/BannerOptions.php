<?php

namespace Ktpl\BannerManagement\Model\Config\Source;

class BannerOptions implements \Magento\Framework\Option\ArrayInterface {
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => 1, 'label' => __('Full-width Slider')], 
            ['value' => 2, 'label' => __('2-columns Slider')]
        ];
    }
}
