<?php
namespace Ktpl\BannerManagement\Helper;

class Image extends \Magento\Framework\Data\Form\Element\Image {
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * @var \Magento\Framework\Filesystem $fileSystem
     */
    protected $_fileSystem;

    /**
     * @param \Magento\Framework\Data\Form\Element\Factory $factoryElement
     * @param \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Data\Form\Element\Factory $factoryElement,
        \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $fileSystem,
        $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_fileSystem = $fileSystem;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $urlBuilder, $data);
    }

    /**
     * @return bool|string
     */
    public function _getUrl($is_small_image = 0) {
        if ($is_small_image) {
            $url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'ktpl/banners/small/';
        }
        else {
            $url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'ktpl/banners/';
        }
        return $url;
    }
    
    /**
     * @return bool|string
     */
    public function _getBaseUrl($is_small_image = 0) {
        $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $mediaRootDir = $mediaDirectory->getAbsolutePath();
        if ($is_small_image) {
            $url = $mediaRootDir . 'ktpl/banners/small/';
        }
        else {
            $url = $mediaRootDir . 'ktpl/banners/';   
        }
        return $url;
    }
}