<?php

namespace Ktpl\BannerManagement\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Ktpl\BannerManagement\Model\ResourceModel\Banner\CollectionFactory;
use Ktpl\BannerManagement\Helper\Image as ImageHelper;
use Magento\Framework\Filesystem\Driver\File;

class MassDelete extends \Magento\Backend\App\Action {

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    
    /**
     * @var \Ktpl\BannerManagement\Helper\Image $imageHelper
     */
    protected $_imageHelper;
    
    /**
     * @var \Magento\Framework\Filesystem\Driver\File $file
     */
    protected $_file;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ImageHelper $imageHelper
     * @param File $file
     */
    public function __construct(
        Context $context, 
        Filter $filter, 
        CollectionFactory $collectionFactory,
        ImageHelper $imageHelper,
        File $file
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_imageHelper = $imageHelper;
        $this->_file = $file;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $template) {
            $filePath = $this->_imageHelper->_getBaseUrl() . $template->getImage();
            if ($this->_file->isExists($filePath))  {
                $this->_file->deleteFile($filePath);
            }
            $filePath1 = $this->_imageHelper->_getBaseUrl(1) . $template->getSmallBannerImage();
            if ($this->_file->isExists($filePath1))  {
                $this->_file->deleteFile($filePath1);
            }
            $template->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

}
