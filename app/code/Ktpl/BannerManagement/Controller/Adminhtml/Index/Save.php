<?php

namespace Ktpl\BannerManagement\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Ktpl\BannerManagement\Model\Banner;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;

class Save extends \Ktpl\BannerManagement\Controller\Adminhtml\Index {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    
    /**
     * @var \Ktpl\BannerManagement\Model\Banner 
     */
    protected $_banner;    
    
    /**
     * @var \Ktpl\BannerManagement\Helper\Image $imageHelper
     */
    protected $_imageHelper;
    
    /**
     * @var \Magento\Framework\Filesystem\Driver\File $file
     */
    protected $_file;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     * @param \Ktpl\BannerManagement\Model\Banner $banner
     * @param \Ktpl\BannerManagement\Helper\Image $imageHelper
     * @param \Magento\Framework\Filesystem\Driver\File $file
     */
    public function __construct(
        Context $context, 
        \Magento\Framework\Registry $coreRegistry, 
        DataPersistorInterface $dataPersistor,
        \Ktpl\BannerManagement\Model\Banner $banner,
        \Ktpl\BannerManagement\Helper\Image $imageHelper,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->_banner = $banner;
        $this->_imageHelper = $imageHelper;
        $this->_file = $file;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        
        if ($data) {
            $id = $data['banners_id'];

            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = Banner::STATUS_ENABLED;
            }
            if (empty($data['banners_id'])) {
                $data['banners_id'] = null;
            }

            if (isset($data['image']['0'])) {
                $data['image'] = $data['image']['0']['name'];
            } else {
                $data['image'] = NULL;
            }

            if (isset($data['small_banner_image']['0'])) {
                $data['small_banner_image'] = $data['small_banner_image']['0']['name'];
            } else {
                $data['small_banner_image'] = NULL;
            }
            
            if (!empty($data['store_id'])){
                $data['store_id'] = implode(',', $data['store_id']);
            }

            /** @var \Ktpl\BannerManagement\Model\Banner $model */
            $model = $this->_banner->load($id);
            
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This banner no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            
            if ($id) {
                // Unlink/Delete old image
                if ($data['image'] != NULL && $model->getImage() && $data['image'] != $model->getImage()) {
                    $filePath = $this->_imageHelper->_getBaseUrl() . $model->getImage();

                    if ($this->_file->isExists($filePath))  {
                        $this->_file->deleteFile($filePath);
                    }
                }
                if ($data['small_banner_image'] != NULL && $model->getSmallBannerImage() != NULL && $data['small_banner_image'] != $model->getSmallBannerImage()) {
                    $filePath1 = $this->_imageHelper->_getBaseUrl(1) . $model->getSmallBannerImage();

                    if ($this->_file->isExists($filePath1))  {
                        $this->_file->deleteFile($filePath1);
                    }
                }
            }
            
            $model->setData($data);
            $model->setModifiedDatetime(date('Y-m-d H:i:s'));
            
            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved the banner.'));
                $this->dataPersistor->clear('banners');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the banner.'));
            }

            $this->dataPersistor->set('banners', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
