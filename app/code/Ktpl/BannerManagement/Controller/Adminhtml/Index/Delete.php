<?php

namespace Ktpl\BannerManagement\Controller\Adminhtml\Index;

class Delete extends \Ktpl\BannerManagement\Controller\Adminhtml\Index {
    
    /**
     * @var \Ktpl\BannerManagement\Model\Banner 
     */
    protected $_banner;    
    
    /**
     * @var \Ktpl\BannerManagement\Helper\Image $imageHelper
     */
    protected $_imageHelper;
    
    /**
     * @var \Magento\Framework\Filesystem\Driver\File $file
     */
    protected $_file;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Ktpl\BannerManagement\Model\Banner $banner
     * @param \Ktpl\BannerManagement\Helper\Image $imageHelper
     * @param \Magento\Framework\Filesystem\Driver\File $file
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context, 
        \Magento\Framework\Registry $coreRegistry,
        \Ktpl\BannerManagement\Model\Banner $banner,
        \Ktpl\BannerManagement\Helper\Image $imageHelper,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        $this->_banner = $banner;
        $this->_imageHelper = $imageHelper;
        $this->_file = $file;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        
        if ($id) {
            try {
                // init model and delete
                $model = $this->_banner;
                $model->load($id);
                
                $filePath = $this->_imageHelper->_getBaseUrl() . $model->getImage();

                if ($this->_file->isExists($filePath))  {
                    $this->_file->deleteFile($filePath);
                }

                $filePath1 = $this->_imageHelper->_getBaseUrl(1) . $model->getSmallBannerImage();

                if ($this->_file->isExists($filePath1))  {
                    $this->_file->deleteFile($filePath1);
                }
                
                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('You deleted the banner.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } 
            catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a banner to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

}
