<?php

namespace Ktpl\BannerManagement\Block;

class Banner extends \Magento\Framework\View\Element\Template {

    protected $_itemCollectionFactory;
    protected $_storeManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context, 
        \Ktpl\BannerManagement\Model\ResourceModel\Banner\CollectionFactory $itemCollectionFactory, 
        \Ktpl\BannerManagement\Helper\Image $imageHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->_itemCollectionFactory = $itemCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->fullImageUrl = $imageHelper->_getUrl();
        $this->smallImageUrl = $imageHelper->_getUrl(1);
        parent::__construct($context, $data);
    }

    public function getItemCollection() {
        $currentStoreId = $this->_storeManager->getStore()->getId();
        $allowedStoreIds = [
            ["finset" => $currentStoreId],
            ["finset" => 0]
        ];
        $collection = $this->_itemCollectionFactory->create()
            ->addFieldToFilter('is_active', ['eq' => '1'])
            ->addFieldToFilter('store_id', $allowedStoreIds)
            ->setOrder('sort_order', 'ASC');
        return $collection;
    }

}
