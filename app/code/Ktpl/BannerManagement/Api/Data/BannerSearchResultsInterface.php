<?php

namespace Ktpl\BannerManagement\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for banner search results.
 * @api
 */
interface BannerSearchResultsInterface extends SearchResultsInterface {

    /**
     * Get banners list.
     *
     * @return \Ktpl\BannerManagement\Api\Data\BannerInterface[]
     */
    public function getItems();

    /**
     * Set banners list.
     *
     * @param \Ktpl\BannerManagement\Api\Data\BannerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
