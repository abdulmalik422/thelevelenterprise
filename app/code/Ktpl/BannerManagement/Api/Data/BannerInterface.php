<?php

namespace Ktpl\BannerManagement\Api\Data;

/**
 * Banner interface.
 * @api
 */
interface BannerInterface {
    /*     * #@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */

    const BANNERS_ID = 'banners_id';
    const IMAGE = 'image';
    const SMALL_BANNER_IMAGE = 'small_banner_image';
    const TITLE = 'title';
    const LINK = 'link';
    const IS_ACTIVE = 'is_active';
    const SORT_ORDER = 'sort_order';
    const CREATED_DATETIME = 'created_datetime';
    const MODIFIED_DATETIME = 'modified_datetime';

    /*     * #@- */

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get image
     *
     * @return string
     */
    public function getImage();

    /**
     * Get small_banner_image
     *
     * @return string
     */
    public function getSmallBannerImage();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get link
     *
     * @return string|null
     */
    public function getLink();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Get Sort Order
     *
     * @return int|null
     */
    public function getSortOrder();

    /**
     * Get created_datetime
     *
     * @return string|null
     */
    public function getCreatedDatetime();

    /**
     * Get modified_datetime
     *
     * @return string|null
     */
    public function getModifiedDatetime();

    /**
     * Set ID
     *
     * @param int $id
     * @return BannerInterface
     */
    public function setId($id);

    /**
     * Set image
     *
     * @param string $image
     * @return BannerInterface
     */
    public function setImage($image);

    /**
     * Set small_banner_image
     *
     * @param string $small_banner_image
     * @return BannerInterface
     */
    public function setSmallBannerImage($small_banner_image);

    /**
     * Set link
     *
     * @param string $link
     * @return BannerInterface
     */
    public function setLink($link);

    /**
     * Set Sort Order
     *
     * @param string $sortOrder
     * @return BannerInterface
     */
    public function setSortOrder($sortOrder);

    /**
     * Set is active
     *
     * @param bool|int $isActive
     * @return BannerInterface
     */
    public function setIsActive($isActive);

    /**
     * Set created_datetime
     *
     * @param string $createdDatetime
     * @return BannerInterface
     */
    public function setCreatedDatetime($createdDatetime);

    /**
     * Set modified_datetime
     *
     * @param string $modifiedDatetime
     * @return BannerInterface
     */
    public function setModifiedDatetime($modifiedDatetime);
}
