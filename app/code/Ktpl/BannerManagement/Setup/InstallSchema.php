<?php

namespace Ktpl\BannerManagement\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPD, ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'banners'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('banners'))
            ->addColumn('banners_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, 
                    ['identity' => true, 'nullable' => false, 'primary' => true], 'Banner ID')
            ->addColumn('image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, '2M', [], 'Banner Image')
            ->addColumn('title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false], 'Banner Title')
            ->addColumn('link', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, '2M', [], 'Banner Link')
            ->addColumn('sort_order', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, 
                    ['nullable' => false], 'Sort Order')
            ->addColumn('is_active', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, 
                    ['nullable' => false, 'default' => '1'], 'Is Banner Active')
            ->addColumn('created_datetime', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, 
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 
                    'Banner Created Datetime')
            ->addColumn('modified_datetime', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, 
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 
                    'Banner Modified Datetime')
            ->addIndex(
                    $installer->getIdxName(
                        'banners', 
                        ['title'], 
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                    ), 
                    ['title'], 
                    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT])
            ->setComment('Banners');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}
