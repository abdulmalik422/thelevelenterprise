<?php

namespace Ktpl\BannerManagement\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface {
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $tableName = $setup->getTable('banners');
            $setup->getConnection()->addColumn($tableName, 'store_id', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '255',
                'nullable' => false,
                'unsigned' => false,
                'comment' => 'Store Id'
            ]);
        }
        else if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $tableName = $setup->getTable('banners');
            $setup->getConnection()->addColumn($tableName, 'small_banner_image', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '2M',
                'after' => 'image',
                'comment' => 'Small Banner Image'
            ]);
        }

        $setup->endSetup();
    }
}
