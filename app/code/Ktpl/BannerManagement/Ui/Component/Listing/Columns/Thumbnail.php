<?php

namespace Ktpl\BannerManagement\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Thumbnail extends \Magento\Ui\Component\Listing\Columns\Column {
    
    const NAME              = 'image';
    const SMALL_IMAGE_NAME  = 'small_banner_image';
    const ALT_FIELD         = 'title';

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Ktpl\BannerManagement\Helper\Image $imageHelper
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Ktpl\BannerManagement\Helper\Image $imageHelper,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->imageHelper = $imageHelper;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                if(isset($item[self::NAME]) && trim($item[self::NAME]) != '') {
                    $url = $this->imageHelper->_getUrl() . $item[self::NAME];
                    $item[self::NAME . '_src'] = $url;
                    $item[self::NAME . '_alt'] = isset($item[self::ALT_FIELD]) ? $item[self::ALT_FIELD] : '';
                    $item[self::NAME . '_link'] = $this->urlBuilder->getUrl(
                        'banners/index/edit',
                        ['id' => $item['banners_id'], 'store' => $this->context->getRequestParam('store')]
                    );
                    $item[self::NAME . '_orig_src'] = $url;
                }
                if(isset($item[self::SMALL_IMAGE_NAME]) && trim($item[self::SMALL_IMAGE_NAME]) != '') {
                    $url = $this->imageHelper->_getUrl(1) . $item[self::SMALL_IMAGE_NAME];
                    $item[self::SMALL_IMAGE_NAME . '_src'] = $url;
                    $item[self::SMALL_IMAGE_NAME . '_alt'] = isset($item[self::ALT_FIELD]) ? $item[self::ALT_FIELD] : '';
                    $item[self::SMALL_IMAGE_NAME . '_link'] = $this->urlBuilder->getUrl(
                        'banners/index/edit',
                        ['id' => $item['banners_id'], 'store' => $this->context->getRequestParam('store')]
                    );
                    $item[self::SMALL_IMAGE_NAME . '_orig_src'] = $url;
                }
            }
        }
        return $dataSource;
    }
}
