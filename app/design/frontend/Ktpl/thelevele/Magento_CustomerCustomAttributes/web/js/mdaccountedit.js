require(['jquery', 'mage/url'], function ($, mageurl) {
    $('.field-mobile_no').insertAfter('.mobile-no-title');
    $("#change-mobile_no").click(function () {
        if ($(this).is(':checked')) {
            $('.mobile-no-container').show();
            $('.field-mobile_no').show();
            $(".save").attr("disabled", true);
            if ($('.mobile-success').text() == 'verified') {
                $(".save").removeAttr("disabled");
            }
        } else {
            $('.mobile-no-container').hide();
            $('.field-mobile_no').hide();
            $('#verify-otp').hide();
            $(".save").removeAttr("disabled");
            if ($('.mobile-success').text() != 'verified') {
                $('#mobile_no').val($('#old_mobile_no').val());
            }
        }
    });
    $('#old_mobile_no').val($('#mobile_no').val());
    $('#otp-send-button').click(function () {
        sendOtp(this);
    });
    $("#mobile_no").on('keypress', function (e) {
        if (e.which === 13) {
            $("#send_otp").click();
        }
    });
    $('#otp-verify-button').click(function () {
        verifyOtp(this);
    });

    /**
     * send OTP on entered mobile number
     *
     * @param data
     */
    function sendOtp(data) {
        $('#mobile_no-error').remove();
        $('#register_mobile_no-error').remove();
        var formType = $(data).attr('data-formtype');
        var mobileNumber = $('#mobile_no').val();
        var checkPhoneNumberExistUrl = mageurl.build('sociallogin/account/checkphonenumberexist', {});
        if (mobileNumber.length > 3) {
            $.ajax({
                url: checkPhoneNumberExistUrl,
                data: {
                    'mobile_no': mobileNumber
                },
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function (response) {
                if (response.exist) {
                    var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                    $('.field-mobile_no .control').append(message);
                    return false;
                } else {
                    var sendOtpUrl = $(data).attr('data-sendotp-url');
                    $.ajax({
                        url: sendOtpUrl,
                        data: {
                            'mobile_no': mobileNumber,
                            'formtype': 'account_edit'
                        },
                        type: 'post',
                        dataType: 'json',
                        showLoader: true,
                    }).done(function (response) {
                        if (response.success) {
                            showTimer();
                            $('#md-sociallogin-otp').show();
                            $('#verify-otp-button').show();
                            //$('#send-otp-button').hide();
                            //$('#mobile_no').prop('readonly', true);
                        } else if (response.error) {
                            var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                            $('.field-mobile_no .control').append(message);
                            return true;
                        } else {
                            return false;
                        }
                    });
                }
            });
        } else {
            var message = '<div for="mobile_no" generated="true" class="mage-error" id="mobile_no-error" >This is a required field.</div>';
            $('.field-mobile_no .control').append(message);
        }
    }

    function showTimer() {
        $('#otp-send-button').attr('disabled', 'disabled');
        $('#mobile_no').attr('readonly', 'true');
        //$('#resend-time').show();
        //count();
        var resendText = $.mage.__('Resend Otp');
        var sendText = $.mage.__('Send Otp');
        $('#otp-send-button span').text(resendText);
        setTimeout(function() {
            $('#otp-send-button span').text(sendText);
            $('#resend-time').hide();
            $('#resend-time').html('00:01:00');
            $('#otp-send-button').removeAttr('disabled');
            $('#mobile_no').removeAttr('readonly');
        }, 60000);
    }

    function count() {
        var startTime = document.getElementById('resend-time').innerHTML;
        var pieces = startTime.split(":");
        var time = new Date();
        time.setHours(pieces[0]);
        time.setMinutes(pieces[1]);
        time.setSeconds(pieces[2]);
        var timedif = new Date(time.valueOf() - 1000);
        var newtime = timedif.toTimeString().split(" ")[0];
        document.getElementById('resend-time').innerHTML = newtime;
        timeoutHandle = setTimeout(count, 1000);
    }

    /**
     * verify OTP entered by customer
     *
     * @param data
     */
    function verifyOtp(data) {
        var formType = $(data).attr('data-formtype');
        var mobileNumber = $('#mobile_no').val();
        var otp = $('#md-sociallogin-popup-otp').val();
        if(!otp.length) {
            var message = '<div for="mobile_no" generated="true" class="mage-error" id="mobile_otp_no-error" >This is a required field.</div>';
            $('#md-sociallogin-otp .sociallogin-input-box').append(message);
            return false;
        }
        if (mobileNumber && otp.length) {
            var sendOtpUrl = $(data).attr('data-verify-url');
            var loginWithOtpUrl = $(data).attr('data-loginwithotp-url');
            $.ajax({
                url: sendOtpUrl,
                data: {
                    'mobile_no': mobileNumber,
                    'otp': otp,
                },
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function (response) {
                $('#register_mobile_no-error').contents().remove();
                if (response.success) {
                    $(".save").removeAttr("disabled");
                    var message = '<div class="mage-success" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                    $('#md-sociallogin-otp .sociallogin-input-box').append(message);
                    //$('#form-validate .actions-toolbar').find('.action.save.primary').click();
                } else if (!response.success) {
                    var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                    $('#md-sociallogin-otp .sociallogin-input-box').append(message);
                    return true;
                } else {
                    return false;
                }
            });
        } else {
            var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
            $('.field-mobile_no .control').append(message);
        }
    }

    /**
     * check mobile number is valid or not
     *
     * @param mobileNumber
     * @returns {*}
     */
    function checkMobileNumber(mobileNumber) {
        if (mobileNumber.length) {
            return mobileNumber;
        } else {
            return 0;
        }
    }
});