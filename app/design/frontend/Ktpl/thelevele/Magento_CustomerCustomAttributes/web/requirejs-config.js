/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            mdaccountedit: 'Magento_CustomerCustomAttributes/js/mdaccountedit'
        }
    }
};
