require([
    'jquery'
], function ($) {
    'use strict';

    $(document).ready(function () {
        if ($("#showCaseValue").length == 0) {
        	var reqValue = GetURLParameter('showCaseValue'),
        		caseValue = (reqValue !== undefined) ? reqValue : "3";
            $("#amasty-shopby-product-list").before('<input type="hidden" id="showCaseValue" name="showCaseValue" value="' + caseValue + '" />');
            if (caseValue != '5') {
	            $(".categry-prodct").addClass("col-3");
	            $(".3col").addClass("active");
	        }
        }
    });

    function GetURLParameter(sParam) {
    	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	}

});