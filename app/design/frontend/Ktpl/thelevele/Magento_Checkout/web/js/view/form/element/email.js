/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(['jquery', 'uiComponent', 'ko', 'Magento_Customer/js/model/customer', 'Magento_Customer/js/action/check-email-availability', 'Magento_Customer/js/action/login', 'Magento_Checkout/js/model/quote', 'Magento_Checkout/js/checkout-data', 'Magento_Checkout/js/model/full-screen-loader', 'mage/url', 'mage/validation'], function ($, Component, ko, customer, checkEmailAvailability, loginAction, quote, checkoutData, fullScreenLoader, mageurl) {
    'use strict';
    var validatedEmail = checkoutData.getValidatedEmailValue();
    var validateCompleteMobileNumber = checkoutData.getValidatedStartmobileValue()+checkoutData.getValidatedMobileValue();

    if (validatedEmail && !customer.isLoggedIn()) {
        quote.guestEmail = validatedEmail;
        quote.guestMobileNumber = validateCompleteMobileNumber;
    }

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/form/element/email',
            email: checkoutData.getInputFieldEmailValue(),
            emailFocused: false,
            mobilenumber: checkoutData.getInputFieldMobilenumberValue(),
            mobilenumberFocused: false,
            startmobilenumber: checkoutData.getInputFieldStartMobilenumberValue(),
            startmobilenumberFocused: false,
            isLoading: false,
            isPasswordVisible: false,
            isMobilePasswordVisible: false,
            listens: {
                email: 'emailHasChanged',
                emailFocused: 'emailHasChanged',
                mobilenumber: 'mobileHasChanged',
                mobilenumberFocused: 'mobileHasChanged',
                startmobilenumber:'mobileHasChanged',
                startmobilenumberFocused:'mobileHasChanged'
            }
        },
        checkDelay: 2000,
        checkRequest: null,
        isEmailCheckComplete: null,
        isCustomerLoggedIn: customer.isLoggedIn,
        forgotPasswordUrl: window.checkoutConfig.forgotPasswordUrl,
        emailCheckTimeout: 0,
        /**
         * Initializes observable properties of instance
         *
         * @returns {Object} Chainable.
         */
        initObservable: function () {
            this._super().observe([
                'email',
                'emailFocused',
                'startmobilenumber',
                'startmobilenumberFocused',
                'mobilenumber',
                'mobilenumberFocused',
                'isLoading',
                'isPasswordVisible'
            ]);
            return this;
        },
        /** @inheritdoc */
        initConfig: function () {
            this._super();
            this.isPasswordVisible = this.resolveInitialPasswordVisibility();
            return this;
        },
        mobileHasChanged: function () {
            var self = this;

            clearTimeout(this.emailCheckTimeout);

            var startmobilenumber = self.startmobilenumber();
            var mobilenumber = self.mobilenumber();

            setTimeout(function () {
                self.validateMobileNumber(self.startmobilenumber(),self.mobilenumber());

                if (self.checkMobileNumber(self.startmobilenumber(),self.mobilenumber())) {
                    quote.guestMobileNumber = self.startmobilenumber()+self.mobilenumber();
                    checkoutData.setValidatedStartmobileValue(self.startmobilenumber());
                    checkoutData.setValidatedMobileValue(self.mobilenumber());
                }

            },self.checkDelay);

            checkoutData.setInputFieldStartMobilenumberValue(self.startmobilenumber());
            checkoutData.setInputFieldMobilenumberValue(self.mobilenumber());

            console.log(checkoutData.getInputFieldMobilenumberValue());
            console.log(quote.guestMobileNumber);
        },
        /**
         * Callback on changing email property
         */
        emailHasChanged: function () {
            var self = this;

            clearTimeout(this.emailCheckTimeout);

            if (self.validateEmail()) {
                quote.guestEmail = self.email();
                checkoutData.setValidatedEmailValue(self.email());
            }
            this.emailCheckTimeout = setTimeout(function () {
                if (self.validateEmail()) {
                    self.checkEmailAvailability();
                } else {
                    self.isPasswordVisible(false);
                }
            }, self.checkDelay);

            checkoutData.setInputFieldEmailValue(self.email());
            console.log(quote.guestEmail);
        },
        /**
         * Check email existing.
         */
        checkEmailAvailability: function () {
            this.validateRequest();
            this.isEmailCheckComplete = $.Deferred();
            this.isLoading(true);
            this.checkRequest = checkEmailAvailability(this.isEmailCheckComplete, this.email());
            $.when(this.isEmailCheckComplete).done(function () {
                this.isPasswordVisible(false);
            }.bind(this)).fail(function () {
                this.isPasswordVisible(true);
                checkoutData.setCheckedEmailValue(this.email());
            }.bind(this)).always(function () {
                this.isLoading(false);
            }.bind(this));
        },
        /**
         * If request has been sent -> abort it.
         * ReadyStates for request aborting:
         * 1 - The request has been set up
         * 2 - The request has been sent
         * 3 - The request is in process
         */
        validateRequest: function () {
            if (this.checkRequest != null && $.inArray(this.checkRequest.readyState, [1, 2, 3])) {
                this.checkRequest.abort();
                this.checkRequest = null;
            }
        },
        /**
         * Local email validation.
         *
         * @param {Boolean} focused - input focus.
         * @returns {Boolean} - validation result.
         */
        validateEmail: function (focused) {
            var loginFormSelector = 'form[data-role=email-with-possible-login]',
                usernameSelector = loginFormSelector + ' input[name=username]',
                loginForm = $(loginFormSelector),
                validator;

            loginForm.validation();

            if (focused === false && !!this.email()) {
                return !!$(usernameSelector).valid();
            }

            validator = loginForm.validate();

            return validator.check(usernameSelector);
        },
        /**
         * validate entered mobile number
         *
         * @param mobilenumber
         */
        validateMobileNumber: function (startmobilenumber,mobilenumber) {
            var self = this;
            var validNumber = self.checkMobileNumber(startmobilenumber,mobilenumber);
            if (validNumber) {
                var checkPhoneNumberExistUrl = mageurl.build('sociallogin/account/checkphonenumberexist', {});
                $.ajax({
                    url: checkPhoneNumberExistUrl,
                    data: {
                        'mobile_no': startmobilenumber+mobilenumber,
                        'savefororder': true
                    },
                    type: 'post',
                    dataType: 'json',
                    showLoader: true
                }).done(function (response) {
                    var messageBox = $('#error-success-messages');
                    self.clearMessages();
                    if (!response.exist) {
                        messageBox.html(response.message);
                        $('#fieldset-mobile').fadeOut();
                    } else {
                        $('#fieldset-mobile').fadeIn();
                    }
                });
            }
        },
        checkMobileNumber: function (telephone_start,telephone) {
            var telephone_valid = false;
            switch (telephone_start) {
                case '966':
                    var re = /^(05|5)([0-9]{8})$/;
                    if (!re.test(telephone)) {
                        telephone_valid = false;
                    } else {
                        telephone_valid = true;
                    }
                    break;
                case '971':
                    var re = /^(05|5)([0-9]{8})$/;
                    if (!re.test(telephone)) {
                        telephone_valid = false;
                    } else {
                        telephone_valid = true;
                    }
                    break;
                case '968':
                    var re = /^(9)([0-9]{7})$/;
                    if (!re.test(telephone)) {
                        telephone_valid = false;
                    } else {
                        telephone_valid = true;
                    }
                    break;
                case '973':
                    var re = /^(3)([0-9]{7})$/;
                    if (!re.test(telephone)) {
                        telephone_valid = false;
                    } else {
                        telephone_valid = true;
                    }
                    break;
                case '965':
                    var re = /^(9|6|5)([0-9]{7})$/;
                    if (!re.test(telephone)) {
                        telephone_valid = false;
                    } else {
                        telephone_valid = true;
                    }
                    break;
                case '974':
                    var re = /^([0-9]{8})$/;
                    if (!re.test(telephone)) {
                        telephone_valid = false;
                    } else {
                        telephone_valid = true;
                    }
                    break;
                case '91':
                    var re = /^([0-9]{10})$/;
                    if (!re.test(telephone)) {
                        telephone_valid = false;
                    } else {
                        telephone_valid = true;
                    }
                    break;
            }
            return telephone_valid;
        },
        /**
         * show send otp button
         */
        showOtpFields: function () {
            $('#switch-login-with-otp').hide();
            $('#mobile-login-with-password').hide();
            $('#login-with-password').hide();
            $('#send-otp').fadeIn();
            $('#switch-login-with-password').fadeIn();
        },
        /**
         * show password field
         */
        showPasswordField: function () {
            $('#send-otp').hide();
            $('#login-with-otp').hide();
            $('#switch-login-with-password').hide();
            $('#mobile-login-with-password').fadeIn();
            $('#login-with-password').fadeIn();
            $('#switch-login-with-otp').fadeIn();
        },
        /**
         * verify otp entered by customer
         */
        verifyOtp: function () {
            $('#mobile_otp_no-error').contents().remove();
            var self = this;
            var mobileNumber = self.email();
            var otp = $('#customer-mobile-password-submit-otp').val();
            if(otp.length > 0) {
                var verifyOtpUrl = mageurl.build('sociallogin/account/verifyotp', {});
                $.ajax({
                    url: verifyOtpUrl,
                    data: {
                        'mobile_no': mobileNumber,
                        'otp': otp,
                    },
                    type: 'post',
                    dataType: 'json',
                    showLoader: true
                }).done(function (response) {
                    var messageBox = $('#error-success-messages');
                    self.clearMessages();
                    if (response.success) {
                        messageBox.append(response.message);
                        self.loginWithMobileNumberOtp();
                        return true;
                    } else if (!response.success) {
                        messageBox.append(response.message);
                        return true;
                    } else {
                        return false;
                    }
                });
            } else {
                var message = '<div for="mobile_no" generated="true" class="mage-error" id="mobile_otp_no-error" >This is a required field.</div>';
                $('#mobile-login-with-otp-submit-otp .control').append(message);
                return false;
            }

        },
        /**
         * login with mobile number and otp entered
         */
        loginWithMobileNumberOtp: function () {
            var self = this;
            var mobileNumber = self.email();
            var loginWithOtpUrl = mageurl.build('sociallogin/account/loginwithmobilenumber', {});
            $.ajax({
                url: loginWithOtpUrl,
                data: {
                    'mobile_no': mobileNumber
                },
                type: 'post',
                dataType: 'json',
                showLoader: false,
            }).done(function (response) {
                var messageBox = $('#error-success-messages');
                self.clearMessages();
                if (response.error) {
                    messageBox.append(response.message);
                    return false;
                } else {
                    messageBox.append(response.message);
                    window.location.reload();
                }
            });
        },
        /**
         * login with mobile number and password
         */
        loginWithMobileNumberPassword: function () {
            $('#mobile_otp_no-error').contents().remove();
            var self = this;
            var mobileNumber = self.email();
            var password = $('#customer-mobile-password').val();

            if(password.length > 2) {
                var loginWithPasswordUrl = mageurl.build('sociallogin/account/loginpost', {});
                $.ajax({
                    url: loginWithPasswordUrl,
                    data: {
                        'login': {
                            'login_type': 'mobile',
                            'mobile_number': mobileNumber,
                            'mobile_password': password
                        },
                        'form_key': $.cookie('form_key')
                    },
                    type: 'post',
                    dataType: 'json',
                    showLoader: false
                }).done(function (response) {
                    var messageBox = $('#error-success-messages');
                    self.clearMessages();
                    if (response.error) {
                        messageBox.append(response.message);
                        return false;
                    } else {
                        messageBox.append(response.message);
                        window.location.reload();
                    }
                });
            } else {
                var message = '<div for="mobile_no" generated="true" class="mage-error" id="mobile_otp_no-error" >This is a required field.</div>';
                $('#mobile-login-with-password .control').append(message);
                return false;
            }

        },
        /**
         * send otp to entered mobile number
         */
        sendOtp: function () {
            var self = this;
            var mobileNumber = self.email();
            var sendOtpUrl = mageurl.build('sociallogin/account/sendotp', {});
            $.ajax({
                url: sendOtpUrl,
                data: {
                    'mobile_no': mobileNumber,
                    'formtype': 'login'
                },
                type: 'post',
                dataType: 'json',
                showLoader: true
            }).done(function (response) {
                var messageBox = $('#error-success-messages');
                self.clearMessages();
                if (response.success) {
                    messageBox.append(response.message);
                    self.showTimer();
                    $('#login-with-otp').fadeIn();
                    $('#mobile-login-with-otp-submit-otp').fadeIn();
                    return true;
                } else if (response.error) {
                    messageBox.append(response.message);
                    return false;
                } else {
                    return false;
                }
            });
        },
        showTimer: function () {
            var self = this;
            $('#send-otp').attr('disabled', 'disabled');
            $('#customer-email').attr('readonly', 'true');
            $('#switch-login-with-password').hide();
            //$('#resend-time').show();
            //self.count();
            var resendText = $.mage.__('Resend Otp');
            var sendText = $.mage.__('Send Otp');
            $('#send-otp span').text(resendText);
            setTimeout(function() {
                $('#send-otp span').text(sendText);
                $('#resend-time').hide();
                $('#resend-time').html('00:01:00');
                $('#send-otp').removeAttr('disabled');
                $('#customer-email').removeAttr('readonly');
                $('#switch-login-with-password').fadeIn();
            }, 60000);
        },

        count: function () {
            var startTime = document.getElementById('resend-time').innerHTML;
            var pieces = startTime.split(":");
            var time = new Date();
            time.setHours(pieces[0]);
            time.setMinutes(pieces[1]);
            time.setSeconds(pieces[2]);
            var timedif = new Date(time.valueOf() - 1000);
            var newtime = timedif.toTimeString().split(" ")[0];
            document.getElementById('resend-time').innerHTML = newtime;
            timeoutHandle = setTimeout(count, 1000);
        },
        /**
         * Log in form submitting callback.
         *
         * @param {HTMLElement} loginForm - form element.
         */
        login: function (loginForm) {
            var loginData = {},
                formDataArray = $(loginForm).serializeArray();
            formDataArray.forEach(function (entry) {
                loginData[entry.name] = entry.value;
            });
            if (this.isPasswordVisible() && $(loginForm).validation() && $(loginForm).validation('isValid')) {
                fullScreenLoader.startLoader();
                loginAction(loginData).always(function () {
                    fullScreenLoader.stopLoader();
                });
            }
        },
        /**
         * Resolves an initial sate of a login form.
         *
         * @returns {Boolean} - initial visibility state.
         */
        resolveInitialPasswordVisibility: function () {
            if (checkoutData.getInputFieldEmailValue() !== '') {
                return checkoutData.getInputFieldEmailValue() === checkoutData.getCheckedEmailValue();
            }
            return false;
        },

        clearMessages: function () {
            $('#error-success-messages').contents().remove();
        }
    });
});