require(['jquery'], function($) {
    $(document).ready(function() {
        $('#forgotpassword_email').click();
    });
    $('#md-sociallogin-popup-mobile-forgot').blur(function() {
        checkMobileNumberExist(this);
    });
    $('#forgot_mobile_start').change(function() {
        checkMobileNumberExist(this);
    });
    $('#forgotpassword_email').click(function() {
        displayEmailForgotPassword();
    });
    $('#forgotpassword_mobile').click(function() {
        displayMobileForgotPassword();
    });
    $('#forgot-md-mobile-otp-send-button').click(function() {
        sendOtp(this);
    });
    $('#forgot-md-mobile-otp-verify-button').click(function() {
        verifyOtp(this);
    });
    $('#new-password-set').click(function() {
        resetPasswordUsingOtp(this);
    });
    /**
     * dispaly email filed on forgot password popup
     */
    function displayEmailForgotPassword() {
        $('.block-mobile-forgotpassword').hide();
        $('.block-email-forgotpassword').show();
    }
    /**
     * display mobile number field to reset password
     */
    function displayMobileForgotPassword() {
        $('.block-mobile-forgotpassword').show();
        $('.block-email-forgotpassword').hide();
    }

    function checkMobileNumberExist(data) {
        var checkPhoneNumberExistUrl = $(data).attr('data-mobilecheckurl');
        var phoneNumber = $('#register_mobile_start').val() + $('#register_mobile_no').val();
        if (phoneNumber.length > 3) {
            $.ajax({
                url: checkPhoneNumberExistUrl,
                data: {
                    'mobile_no': phoneNumber
                },
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function(response) {
                $('#register_mobile_no-error').contents().remove();
                if (!response.exist) {
                    var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                    $('.field-mobile_no .control').append(message);
                    return false;
                } else {
                    return true;
                }
            });
        }
    }
    /**
     * send OTP on entered mobile number
     *
     * @param data
     */
    function sendOtp(data) {
        var formType = $(data).attr('data-formtype');
        var checkPhoneNumberExistUrl = $(data).attr('data-mobilecheckurl');
        var mobileNumber = $('#forgot_mobile_start').val() + $('#md-sociallogin-popup-mobile-forgot').val();
        var sendOtpUrl = $(data).attr('data-sendotp-url');
        $.ajax({
            url: checkPhoneNumberExistUrl,
            data: {
                'mobile_no': mobileNumber
            },
            type: 'post',
            dataType: 'json',
            showLoader: true,
        }).done(function(response) {
            $('#register_mobile_no-error').contents().remove();
            if (!response.exist) {
                var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                $('#md-sociallogin-otp-forgot .sociallogin-input-box').append(message);
                return false;
            } else {
                $.ajax({
                    url: sendOtpUrl,
                    data: {
                        'mobile_no': mobileNumber,
                        'formtype': formType
                    },
                    type: 'post',
                    dataType: 'json',
                    showLoader: true,
                }).done(function(response) {
                    $('#register_mobile_no-error').contents().remove();
                    if (response.success) {
                        $('#md-sociallogin-otp-forgot').show();
                        $('#forgot-verify-otp-button').show();
                        showTimer();
                    } else if (response.error) {
                        var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                        $('#md-sociallogin-otp-forgot .sociallogin-input-box').append(message);
                        return true;
                    } else {
                        return false;
                    }
                });
            }
        });
    }

    function showTimer() {
        $('#forgot-md-mobile-otp-send-button').attr('disabled', 'disabled');
        $('#md-sociallogin-popup-mobile-forgot').attr('readonly', 'true');
        //$('#resend-time-forgot').show();
        //count();
        var resendText = $.mage.__('Resend Otp');
        var sendText = $.mage.__('Send Otp');
        $('#forgot-md-mobile-otp-send-button span').text(resendText);
        setTimeout(function() {
            $('#forgot-md-mobile-otp-send-button span').text(sendText);
            $('#resend-time-forgot').hide();
            $('#resend-time-forgot').html('00:01:00');
            $('#forgot-md-mobile-otp-send-button').removeAttr('disabled');
            $('#md-sociallogin-popup-mobile-forgot').removeAttr('readonly');
        }, 60000);
    }

    function count() {
        var startTime = document.getElementById('resend-time-forgot').innerHTML;
        var pieces = startTime.split(":");
        var time = new Date();
        time.setHours(pieces[0]);
        time.setMinutes(pieces[1]);
        time.setSeconds(pieces[2]);
        var timedif = new Date(time.valueOf() - 1000);
        var newtime = timedif.toTimeString().split(" ")[0];
        document.getElementById('resend-time-forgot').innerHTML = newtime;
        timeoutHandle = setTimeout(count, 1000);
    }
    /**
     * verify OTP entered by customer
     *
     * @param data
     */
    function verifyOtp(data) {
        var formType = $(data).attr('data-formtype');
        var mobileNumber = checkMobileNumber($('#forgot_mobile_start').val() + $('#md-sociallogin-popup-mobile-forgot').val());
        var otp = $('#md-sociallogin-popup-otp-forgot').val();
        if (mobileNumber) {
            var sendOtpUrl = $(data).attr('data-verify-url');
            var loginWithOtpUrl = $(data).attr('data-loginwithotp-url');
            $.ajax({
                url: sendOtpUrl,
                data: {
                    'mobile_no': mobileNumber,
                    'otp': otp,
                },
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function(response) {
                $('#register_mobile_no-error').contents().remove();
                if (response.success) {
                    openResetPasswordForm();
                } else if (!response.success) {
                    var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                    $('#md-sociallogin-otp-forgot .sociallogin-input-box').append(message);
                    return true;
                } else {
                    return false;
                }
            });
        } else {
            alert('please enter valid mobile number');
        }
    }
    /**
     * check mobile number is valid or not
     *
     * @param mobileNumber
     * @returns {*}
     */
    function checkMobileNumber(mobileNumber) {
        if (mobileNumber.length) {
            return mobileNumber;
        } else {
            return 0;
        }
    }
    /**
     * display reset password form if OTP varified
     * @param mobileNumber
     */
    function openResetPasswordForm() {
        $('#md-forgot-user').fadeOut();
        $('#reset-password-form').fadeIn();
    }
    /**
     * reset password of customer
     */
    function resetPasswordUsingOtp(data) {
        var resetPasswordPostUrl = $(data).attr('data-resetpasswordpost-url');
        var mobileNumber = checkMobileNumber($('#forgot_mobile_start').val() + $('#md-sociallogin-popup-mobile-forgot').val());
        var password = $('#reset-password-form').find('#password').val()
        var confPassword = $('#reset-password-form').find('#password-confirmation').val();
        $.ajax({
            url: resetPasswordPostUrl,
            data: {
                'mobile_no': mobileNumber,
                'password': password,
                'conf_password': confPassword,
            },
            type: 'post',
            dataType: 'json',
            showLoader: true,
        }).done(function(response) {
            var messageBox = $('#md-forgot-password-messages');
            if (response.error) {
                messageBox.append(response.message);
                return false;
            } else {
                messageBox.append(response.message);
            }
        });
    }
});