require(['jquery', 'mage/translate'], function($) {
    $(document).ready(function() {
        $('#login_email').click();
    });
    $('#login_email').click(function() {
        displayEmailLogin();
    });
    $('#login_mobile').click(function() {
        displayMobileLogin();
    });
    $('#md-mobile-otp-send-button').click(function() {
        sendOtp(this);
    });
    $('#md-mobile-otp-verify-button').click(function() {
        verifyOtp(this);
    });
    $('#ask-login-with-otp').click(function() {
        loginWithOTP();
    });
    $('#ask-login-with-password').click(function() {
        loginWithPassword();
    });
    $('#md-mobile-button-sociallogin-login').click(function() {});
    $("#md-sociallogin-popup-mobile").keypress(function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    /* Login form using mobile */
    $('#md-mobile-button-sociallogin-login').click(function() {
        $('#md-sociallogin-form-mobile').submit();
    });
    $('#md-sociallogin-form-mobile').mage('validation', {
        submitHandler: function(form) {
            /**
             * merge country code and mobile field values
             */
            var mobileNumber = $('#md-sociallogin-popup-mobile').val();
            var regMobileNumber = $('#mobile_start').val() + mobileNumber;
            $('#md-sociallogin-popup-mobile').val(regMobileNumber);
            $.ajax({
                url: form.action,
                data: $('#md-sociallogin-form-mobile').serialize(),
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function(data) {
                $('#md-sociallogin-popup-mobile').val(mobileNumber);
                if (data) {
                    var messageBox = $('#md-login-messages');
                    messageBox.html(data.html_message);
                    if (data.url) {
                        window.location.href = data.url;
                    }
                }
            });
        }
    });
    /**
     * display customer login by email and password
     */
    function displayEmailLogin() {
        $('.block-mobile-login').hide();
        $('.block-email-login').show();
    }
    /**
     * dispaly customer login by mobile number and password
     */
    function displayMobileLogin() {
        $('.block-mobile-login').show();
        $('.block-email-login').hide();
    }
    /**
     * display customer login with mobile number and OTP message
     */
    function loginWithOTP() {
        $('#send-otp-button').show();
        $('#ask-login-with-password').show();
        $('#md-sociallogin-mobile-password').hide();
        $('#ask-login-with-otp').hide();
        $('#md-mobile-button-sociallogin-login').hide();
    }
    /**
     * display customer login with mobile number and password
     */
    function loginWithPassword() {
        $('#verify-otp-button').hide();
        $('#send-otp-button').hide();
        $('#md-sociallogin-otp').hide();
        $('#ask-login-with-password').hide();
        $('#md-sociallogin-mobile-password').show();
        $('#ask-login-with-otp').show();
        $('#md-mobile-button-sociallogin-login').show();
    }
    /**
     * check customer with mobile number exist or not
     */
    function checkMobileNumberExist(data) {
        var checkPhoneNumberExistUrl = $(data).attr('data-mobilecheckurl');
        var phoneNumber = $('#mobile_start').val() + $('#md-sociallogin-popup-mobile').val();
        if (phoneNumber.length > 3) {
            $.ajax({
                url: checkPhoneNumberExistUrl,
                data: {
                    'mobile_no': phoneNumber
                },
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function(response) {
                $('#register_mobile_no-error').contents().remove();
                if (!response.exist) {
                    var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                    $('.field-mobile_no .control').append(message);
                    return false;
                } else {
                    return true;
                }
            });
        }
    }
    /**
     * send OTP on entered mobile number
     *
     * @param data
     */
    function sendOtp(data) {
        var formType = $(data).attr('data-formtype');
        var checkPhoneNumberExistUrl = $(data).attr('data-mobilecheckurl');
        var mobileNumber = checkMobileNumber($('#mobile_start').val() + $('#md-sociallogin-popup-mobile').val());
        $.ajax({
            url: checkPhoneNumberExistUrl,
            data: {
                'mobile_no': mobileNumber
            },
            type: 'post',
            dataType: 'json',
            showLoader: true,
        }).done(function(response) {
            $('#register_mobile_no-error').contents().remove();
            if (!response.exist) {
                var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                $('#login-mobile-number .sociallogin-input-box').append(message);
                return false;
            } else {
                var sendOtpUrl = $(data).attr('data-sendotp-url');
                $.ajax({
                    url: sendOtpUrl,
                    data: {
                        'mobile_no': mobileNumber,
                        'formtype': formType
                    },
                    type: 'post',
                    dataType: 'json',
                    showLoader: true,
                }).done(function(response) {
                    $('#register_mobile_no-error').contents().remove();
                    var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                    if (response.success) {
                        showTimer();
                        //$('#login-mobile-number .sociallogin-input-box').append(message);
                        $('#verify-otp-button').show();
                        $('#md-sociallogin-otp').show();
                    } else if (response.error) {
                        $('#login-mobile-number .sociallogin-input-box').append(message);
                        return true;
                    } else {
                        return false;
                    }
                });
            }
        });
    }

    function showTimer() {
        $('#md-mobile-otp-send-button').attr('disabled', 'disabled');
        $('#md-sociallogin-popup-mobile').attr('readonly', 'true');
        //$('#resend-time').show();
        //count();
        var resendText = $.mage.__('Resend Otp');
        var sendText = $.mage.__('Send Otp');
        $('#md-mobile-otp-send-button span').text(resendText);
        setTimeout(function() {
            $('#md-mobile-otp-send-button span').text(sendText);
            $('#resend-time').hide();
            $('#resend-time').html('00:01:00');
            $('#md-mobile-otp-send-button').removeAttr('disabled');
            $('#md-sociallogin-popup-mobile').removeAttr('readonly');
        }, 60000);
    }

    function count() {
        var startTime = document.getElementById('resend-time').innerHTML;
        var pieces = startTime.split(":");
        var time = new Date();
        time.setHours(pieces[0]);
        time.setMinutes(pieces[1]);
        time.setSeconds(pieces[2]);
        var timedif = new Date(time.valueOf() - 1000);
        var newtime = timedif.toTimeString().split(" ")[0];
        document.getElementById('resend-time').innerHTML = newtime;
        timeoutHandle = setTimeout(count, 1000);
    }
    /**
     * verify OTP entered by customer
     *
     * @param data
     */
    function verifyOtp(data) {
        var formType = $(data).attr('data-formtype');
        var mobileNumber = checkMobileNumber($('#mobile_start').val() + $('#md-sociallogin-popup-mobile').val());
        var otp = $('#md-sociallogin-popup-otp').val();
        if (!otp.length) {
            var message = '<div for="mobile_no" generated="true" class="mage-error" id="mobile_otp_no-error" >This is a required field.</div>';
            $('#md-sociallogin-otp .sociallogin-input-box').append(message);
            return false;
        }
        var sendOtpUrl = $(data).attr('data-verify-url');
        var loginWithOtpUrl = $(data).attr('data-loginwithotp-url');
        $.ajax({
            url: sendOtpUrl,
            data: {
                'mobile_no': mobileNumber,
                'otp': otp,
            },
            type: 'post',
            dataType: 'json',
            showLoader: true,
        }).done(function(response) {
            $('#register_mobile_no-error').contents().remove();
            if (response.success) {
                var message = '<div class="mage-success" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                $('#md-sociallogin-otp .sociallogin-input-box').append(message);
                loginWithMobileNumber(mobileNumber, loginWithOtpUrl);
            } else if (!response.success) {
                var message = '<div class="mage-error" generated="true" id="register_mobile_no-error">' + response.message + '</div>';
                $('#md-sociallogin-otp .sociallogin-input-box').append(message);
                return true;
            } else {
                return false;
            }
        });
    }
    /**
     * Login customer with mobile number and entered OTP
     * redirect to current location after customer logged in
     *
     * @param mobileNumber
     * @param loginUrl
     */
    function loginWithMobileNumber(mobileNumber, loginUrl) {
        $.ajax({
            url: loginUrl,
            data: {
                'mobile_no': mobileNumber
            },
            type: 'post',
            dataType: 'json',
            showLoader: false,
        }).done(function(response) {
            var messageBox = $('#md-login-messages');
            if (response.error) {
                messageBox.append(response.message);
                return false;
            } else {
                messageBox.append(response.message);
                window.location.reload();
            }
        });
    }
    /**
     * check mobile number is valid or not
     *
     * @param mobileNumber
     * @returns {*}
     */
    function checkMobileNumber(mobileNumber) {
        if (mobileNumber.length) {
            return mobileNumber;
        } else {
            return 0;
        }
    }
});