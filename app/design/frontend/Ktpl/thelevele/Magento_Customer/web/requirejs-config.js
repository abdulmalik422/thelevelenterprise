/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            mdlogin: 'Magento_Customer/js/mdlogin',
            mdforgot: 'Magento_Customer/js/mdforgot',
            mdregister: 'Magento_Customer/js/mdregister',
        }
    }
};
