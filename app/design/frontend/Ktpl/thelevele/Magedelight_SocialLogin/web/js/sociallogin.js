/**
 * Magedelight
 * Copyright (C) 2018 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 * @package Magedelight_SocialLogin
 * @copyright Copyright (c) 2018 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */
require(['jquery', 'Magento_Ui/js/modal/modal', 'mage/url', 'mage/translate', 'mage/mage'], function(jQuery, modal, mageurl) {
    var options = {
        type: 'popup',
        responsive: true,
        innerScroll: true,
        title: 'Login',
        buttons: false,
        modalClass: 'social-popup'
    };
    var checkPhoneNumberExistUrl = mageurl.build('sociallogin/account/checkphonenumberexist', {});
    var popup = modal(options, jQuery('#md_popup'));
    jQuery('#md_popup').modal();
    jQuery(document).on('click touchstart', 'li.md-custom-toplink a', function() {
        var display_id = jQuery(this).attr("class");
        //alert(display_id);
        if (jQuery(window).width() <= 767) {
            jQuery('html').toggleClass('nav-open nav-close');
        }
        jQuery('.modal-inner-wrap').addClass('md-social-popup');
        if (display_id == 'md-login-form') {
            displaysignin();
        } else if (display_id == 'md-create-user') {
            displayregister();
        }
        jQuery('#md_popup').modal('openModal');
    });
    /* Login form using email */
    jQuery('#md-email-button-sociallogin-login').click(function() {
        jQuery('#md-sociallogin-form').submit();
    });
    jQuery('#md-sociallogin-form').mage('validation', {
        submitHandler: function(form) {
            jQuery.ajax({
                url: form.action,
                data: jQuery('#md-sociallogin-form').serialize(),
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function(data) {
                if (data) {
                    var messageBox = jQuery('#md-login-messages');
                    messageBox.html(data.html_message);
                    if (data.url) {
                        window.location.href = data.url;
                    }
                }
            });
        }
    });
    /* Login form using mobile */
    jQuery('#md-mobile-button-sociallogin-login').click(function() {
        jQuery('#md-sociallogin-form-mobile').submit();
    });
    jQuery('#md-sociallogin-form-mobile').mage('validation', {
        submitHandler: function(form) {
            /**
             * merge country code and mobile field values
             */
            var mobileNumber = jQuery('#md-sociallogin-popup-mobile').val();
            var regMobileNumber = jQuery('#mobile_start').val() + mobileNumber;
            var messageBox = jQuery('#md-login-messages');
            if (regMobileNumber.length) {
                jQuery.ajax({
                    url: checkPhoneNumberExistUrl,
                    data: {
                        'mobile_no': regMobileNumber
                    },
                    type: 'post',
                    dataType: 'json',
                    showLoader: true,
                }).done(function(response) {
                    clearMessages();
                    if (!response.exist) {
                        messageBox.append(response.message);
                        return false;
                    } else {
                        jQuery('#md-sociallogin-popup-mobile').val(mobileNumber);
                        jQuery.ajax({
                            url: form.action,
                            data: jQuery('#md-sociallogin-form-mobile').serialize(),
                            type: 'post',
                            dataType: 'json',
                            showLoader: true,
                        }).done(function(data) {
                            clearMessages();
                            jQuery('#md-sociallogin-popup-mobile').val(mobileNumber);
                            if (data) {
                                messageBox.html(data.html_message);
                                if (data.url) {
                                    window.location.href = data.url;
                                }
                            }
                        });
                    }
                });
            }
        }
    });
    /* Forgot Form */
    jQuery('#md-button-sociallogin-login-forgot').click(function() {
        jQuery('#md-sociallogin-form-forgot').submit();
    });
    jQuery('#md-sociallogin-form-forgot').mage('validation', {
        submitHandler: function(form) {
            jQuery.ajax({
                url: form.action,
                data: jQuery('#md-sociallogin-form-forgot').serialize(),
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function(data) {
                if (data) {
                    var messageBox = jQuery('#md-forgot-email');
                    messageBox.html(data.html_message);
                    if (data.url) {
                        messageBox.html(data.html_message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 3000);
                    }
                }
            });
        }
    });
    /* Register form */
    jQuery('#md-button-social-login-register').click(function() {
        jQuery('#md-sociallogin-form-create').submit();
    });
    jQuery('#md-sociallogin-form-create').mage('validation', {
        submitHandler: function(form) {
            /**
             * merge country code and mobile field values
             */
            var regMobileNumber = jQuery('#register_mobile_start').val() + jQuery('#register_mobile_no').val();
            jQuery("#register_mobile_no").val(regMobileNumber);
            jQuery.ajax({
                url: form.action,
                data: jQuery('#md-sociallogin-form-create').serialize(),
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function(data) {
                var messageBox = jQuery('#md-registration-messages');
                messageBox.html('');
                if (data) {
                    messageBox.html(data.html_message);
                    if (data.url) {
                        window.location.href = data.url;
                    }
                }
            });
        }
    });
    jQuery('#login_email').click(function() {
        displayEmailLogin();
    });
    jQuery('#login_mobile').click(function() {
        displayMobileLogin();
    });
    jQuery('#md-forgot-password').click(function() {
        displayforgot();
    });
    jQuery('#md-sociallogin-create').click(function() {
        displayregister();
    });
    jQuery('#md-mobile-otp-send-button').click(function() {
        sendOtp(this);
    });
    jQuery('#md-mobile-otp-verify-button').click(function() {
        verifyOtp(this);
    });
    jQuery('#ask-login-with-otp').click(function() {
        loginWithOTP();
    });
    jQuery('#ask-login-with-password').click(function() {
        loginWithPassword();
    });
    jQuery('#md-sociallogin-create').click(function() {
        displayregister();
    });
    jQuery('#md-create-back').click(function() {
        displaysignin();
    });
    jQuery('#forgotpassword_email').click(function() {
        displayEmailForgotPassword();
    });
    jQuery('#forgotpassword_mobile').click(function() {
        displayMobileForgotPassword();
    });
    jQuery('#forgot-md-mobile-otp-send-button').click(function() {
        sendOtp(this);
    });
    jQuery('#forgot-md-mobile-otp-verify-button').click(function() {
        verifyOtp(this);
    });
    jQuery('#new-password-set').click(function() {
        resetPasswordUsingOtp(this);
    });
    jQuery("#md-sociallogin-popup-mobile").keypress(function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
});
/**
 * dispaly sign in page
 */
function displaysignin() {
    jQuery('#md-login-social').show();
    jQuery('.md-forgot-user').hide();
    jQuery('.md-register-user').hide();
    jQuery('.md-login-user').show();
    jQuery('#login_email').click();
}
/**
 * display forgot password page
 */
function displayforgot() {
    jQuery('#md-login-social').hide();
    jQuery('.md-forgot-user').show();
    jQuery('.md-register-user').hide();
    jQuery('.md-login-user').hide();
    jQuery('#forgotpassword_email').click();
    jQuery('.md-social-popup .modal-header .modal-title').text('Forgot Password');
}
/**
 * display customer registration page
 */
function displayregister() {
    jQuery('#md-login-social').show();
    jQuery('.md-forgot-user').hide();
    jQuery('.md-register-user').show();
    jQuery('.md-login-user').hide();
    jQuery('.md-social-popup .modal-header .modal-title').text('Register');
}
/**
 * display customer login by email and password
 */
function displayEmailLogin() {
    jQuery('.block-mobile-login').hide();
    jQuery('.block-email-login').show();
    jQuery('.md-social-popup .modal-header .modal-title').text('Login');
}
/**
 * dispaly customer login by mobile number and password
 */
function displayMobileLogin() {
    jQuery('.block-mobile-login').show();
    jQuery('.block-email-login').hide();
    jQuery('.md-social-popup .modal-header .modal-title').text('Login');
}
/**
 * display customer login with mobile number and OTP message
 */
function loginWithOTP() {
    jQuery('#send-otp-button').show();
    jQuery('#ask-login-with-password').show();
    jQuery('#md-sociallogin-mobile-password').hide();
    jQuery('#ask-login-with-otp').hide();
    jQuery('#md-mobile-button-sociallogin-login').hide();
}
/**
 * display customer login with mobile number and password
 */
function loginWithPassword() {
    jQuery('#verify-otp-button').hide();
    jQuery('#send-otp-button').hide();
    jQuery('#md-sociallogin-otp').hide();
    jQuery('#ask-login-with-password').hide();
    jQuery('#md-sociallogin-mobile-password').show();
    jQuery('#ask-login-with-otp').show();
    jQuery('#md-mobile-button-sociallogin-login').show();
}
/**
 * check entered phone number is already exist
 *
 * @param phoneNumber
 * @param checkPhoneNumberExistUrl
 * @param messageBox
 */
function checkPhoneNumberExist(phoneNumber, checkPhoneNumberExistUrl, messageBox) {
    if (phoneNumber.length) {
        jQuery.ajax({
            url: checkPhoneNumberExistUrl,
            data: {
                'mobile_no': phoneNumber
            },
            type: 'post',
            dataType: 'json',
            showLoader: true,
        }).done(function(response) {
            clearMessages();
            if (!response.exist) {
                messageBox.append(response.message);
                return false;
            } else {
                return true;
            }
        });
    }
}
/**
 * check entered email id is already exist
 * @param data
 */
function checkEmailExist(data) {
    var checkEmailExistUrl = jQuery(data).attr('data-emailcheckurl');
    var email = jQuery(data).val();
    if (email.length) {
        jQuery.ajax({
            url: checkEmailExistUrl,
            data: {
                'email': email
            },
            type: 'post',
            dataType: 'json',
            showLoader: true,
        }).done(function(response) {
            var messageBox = jQuery('#md-registration-messages');
            if (!response.exist) {
                messageBox.append(response.message);
                return false;
            } else {
                return true;
            }
        });
    }
}
/**
 * dispaly email filed on forgot password popup
 */
function displayEmailForgotPassword() {
    jQuery('.block-mobile-forgotpassword').hide();
    jQuery('.block-email-forgotpassword').show();
}
/**
 * display mobile number field to reset password
 */
function displayMobileForgotPassword() {
    jQuery('.block-mobile-forgotpassword').show();
    jQuery('.block-email-forgotpassword').hide();
}
/**
 * send OTP on entered mobile number
 *
 * @param data
 */
function sendOtp(data) {
    var formType = jQuery(data).attr('data-formtype');
    /**
     * get mobile number from respective form
     */
    var checkPhoneNumberExistUrl = jQuery(data).attr('data-mobilecheckurl');
    if (formType === 'login') {
        jQuery('#md-sociallogin-popup-mobile-error').remove();
        if (!jQuery('#md-sociallogin-popup-mobile').val().length) {
            var enterMobileNumberMessage = '<div for="md-sociallogin-popup-mobile" generated="true" class="mage-error" id="md-sociallogin-popup-mobile-error">This is a required field.</div>';
            jQuery('.sociallogin-input-box').append(enterMobileNumberMessage);
            return false;
        }
        var mobileNumber = jQuery('#mobile_start').val() + jQuery('#md-sociallogin-popup-mobile').val();
        var messageBox = jQuery('#md-login-messages');
    } else if (formType === 'forgotpassword') {
        var mobileNumber = jQuery('#forgot_mobile_start').val() + jQuery('#md-sociallogin-popup-mobile-forgot').val();
        var messageBox = jQuery('#md-forgot-password-messages');
    } else {
        var mobileNumber = 0;
    }
    jQuery.ajax({
        url: checkPhoneNumberExistUrl,
        data: {
            'mobile_no': mobileNumber
        },
        type: 'post',
        dataType: 'json',
        showLoader: true,
    }).done(function(response) {
        clearMessages();
        if (!response.exist) {
            messageBox.append(response.message);
            return false;
        } else {
            var sendOtpUrl = jQuery(data).attr('data-sendotp-url');
            jQuery.ajax({
                url: sendOtpUrl,
                data: {
                    'mobile_no': mobileNumber,
                    'formtype': formType
                },
                type: 'post',
                dataType: 'json',
                showLoader: true,
            }).done(function(response) {
                clearMessages();
                if (response.success) {
                    messageBox.append(response.message);
                    /**
                     * display verify otp button and field for respective form
                     */
                    if (formType === 'login') {
                        showTimer('md-mobile-otp-send-button', 'resend-time', 'md-sociallogin-popup-mobile');
                        jQuery('#verify-otp-button').show();
                        jQuery('#md-sociallogin-otp').show();
                    } else if (formType === 'forgotpassword') {
                        showTimer('forgot-md-mobile-otp-send-button', 'resend-time', 'md-sociallogin-popup-mobile-forgot');
                        jQuery('#md-sociallogin-otp-forgot').show();
                        jQuery('#forgot-verify-otp-button').show();
                    } else {
                        var mobileNumber = 0;
                    }
                    return true;
                } else if (response.error) {
                    messageBox.append(response.message);
                    return true;
                } else {
                    return false;
                }
            });
        }
    });
}

function showTimer(otp_button, otp_clock, mobile_id) {
    jQuery('#' + otp_button).attr('disabled', 'disabled');
    jQuery('#' + mobile_id).attr('readonly', 'true');
    //jQuery('#' + otp_clock).show();
    //count(otp_clock);
    var resendText = jQuery.mage.__('Resend Otp');
    var sendText = jQuery.mage.__('Send Otp');
    jQuery('#' + otp_button + ' span').text(resendText);
    setTimeout(function() {
        jQuery('#' + otp_button + ' span').text(sendText);
        jQuery('#' + otp_clock).hide();
        jQuery('#' + otp_clock).html('00:01:00');
        jQuery('#' + otp_button).removeAttr('disabled');
        jQuery('#' + mobile_id).removeAttr('readonly');
    }, 60000);
}

function count(otp_clock) {
    var startTime = document.getElementById(otp_clock).innerHTML;
    var pieces = startTime1.split(":");
    var time = new Date();
    time.setHours(pieces[0]);
    time.setMinutes(pieces[1]);
    time.setSeconds(pieces[2]);
    var timedif = new Date(time.valueOf() - 1000);
    var newtime = timedif.toTimeString().split(" ")[0];
    document.getElementById(otp_clock).innerHTML = newtime;
    timeoutHandle = setTimeout(count, 1000);
}
/**
 * verify OTP entered by customer
 *
 * @param data
 */
function verifyOtp(data) {
    var formType = jQuery(data).attr('data-formtype');
    /**
     * get OTP from respective form
     */
    if (formType === 'login') {
        var messageBox = jQuery('#md-login-messages');
        var regMobileNumber = jQuery('#mobile_start').val() + jQuery('#md-sociallogin-popup-mobile').val();
        var mobileNumber = checkMobileNumber(regMobileNumber);
        var otp = jQuery('#md-sociallogin-popup-otp').val();
        if (otp.length < 4) {
            jQuery('#mobile_otp_no-error').remove();
            var message = '<div for="mobile_no" generated="true" class="mage-error" id="mobile_otp_no-error" >This is a required field.</div>';
            jQuery('#md-sociallogin-otp .sociallogin-input-box').append(message);
            return false;
        }
    } else if (formType === 'forgotpassword') {
        var messageBox = jQuery('#md-forgot-password-messages');
        var mobileNumber = checkMobileNumber(jQuery('#forgot_mobile_start').val() + jQuery('#md-sociallogin-popup-mobile-forgot').val());
        var otp = jQuery('#md-sociallogin-popup-otp-forgot').val();
        if (otp.length < 4) {
            var message = '<div for="mobile_no" generated="true" class="mage-error" id="mobile_otp_no-error" >This is a required field.</div>';
            jQuery('#md-sociallogin-otp .sociallogin-input-box').append(message);
            return false;
        }
    } else {
        var mobileNumber = 0;
        var otp = 0;
    }
    if (mobileNumber) {
        var sendOtpUrl = jQuery(data).attr('data-verify-url');
        var loginWithOtpUrl = jQuery(data).attr('data-loginwithotp-url');
        jQuery.ajax({
            url: sendOtpUrl,
            data: {
                'mobile_no': mobileNumber,
                'otp': otp,
            },
            type: 'post',
            dataType: 'json',
            showLoader: true,
        }).done(function(response) {
            clearMessages();
            if (response.success) {
                messageBox.append(response.message);
                if (formType === 'login') {
                    loginWithMobileNumber(mobileNumber, loginWithOtpUrl);
                } else if (formType === 'forgotpassword') {
                    openResetPasswordForm();
                }
                return true;
            } else {
                messageBox.append(response.message);
                return false;
            }
        });
    } else {
        alert('please enter valid mobile number');
    }
}
/**
 * check mobile number is valid or not
 *
 * @param mobileNumber
 * @returns {*}
 */
function checkMobileNumber(mobileNumber) {
    if (mobileNumber.length) {
        return mobileNumber;
    } else {
        return 0;
    }
}
/**
 * Login customer with mobile number and entered OTP
 * redirect to current location after customer logged in
 *
 * @param mobileNumber
 * @param loginUrl
 */
function loginWithMobileNumber(mobileNumber, loginUrl) {
    jQuery.ajax({
        url: loginUrl,
        data: {
            'mobile_no': mobileNumber
        },
        type: 'post',
        dataType: 'json',
        showLoader: false,
    }).done(function(response) {
        clearMessages();
        var messageBox = jQuery('#md-login-messages');
        if (response.error) {
            messageBox.append(response.message);
            return false;
        } else {
            messageBox.append(response.message);
            window.location.reload();
        }
    });
}
/**
 * clear success/error messages on login popup
 */
function clearMessages() {
    jQuery('#md-login-messages').contents().remove();
    jQuery('#md-forgot-password-messages').contents().remove();
    jQuery('#md-registration-messages').contents().remove();
}
/**
 * display reset password form if OTP varified
 * @param mobileNumber
 */
function openResetPasswordForm() {
    jQuery('#md-forgot-user').fadeOut();
    jQuery('#reset-password-form').fadeIn();
}
/**
 * reset password of customer
 */
function resetPasswordUsingOtp(data) {
    var resetPasswordPostUrl = jQuery(data).attr('data-resetpasswordpost-url');
    var mobileNumber = checkMobileNumber(jQuery('#forgot_mobile_start').val() + jQuery('#md-sociallogin-popup-mobile-forgot').val());
    var password = jQuery('#reset-password-form #password').val()
    var confPassword = jQuery('#reset-password-form #password-confirmation').val();
    jQuery.ajax({
        url: resetPasswordPostUrl,
        data: {
            'mobile_no': mobileNumber,
            'password': password,
            'conf_password': confPassword,
        },
        type: 'post',
        dataType: 'json',
        showLoader: true,
    }).done(function(response) {
        clearMessages();
        var messageBox = jQuery('#md-forgot-password-messages');
        if (response.error) {
            messageBox.append(response.message);
            return false;
        } else {
            jQuery("#md-popup").modal("closeModal");
            messageBox.append(response.message);
            setTimeout(function() {
                window.location.reload();
            }, 3000);
        }
    });
}